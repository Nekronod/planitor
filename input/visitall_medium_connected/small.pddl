(define (problem grid-2)
(:domain grid-visit-all)
(:objects 
    loc-x0-y0
    loc-x0-y5
    loc-x5-y0
    loc-x5-y5
- place 
        
)
(:init
    (at-robot loc-x0-y0)
    (visited loc-x0-y0)
    (connected loc-x0-y0 loc-x5-y0)
    (connected loc-x0-y0 loc-x0-y5)
    (connected loc-x0-y5 loc-x5-y5)
    (connected loc-x0-y5 loc-x0-y0)
    (connected loc-x5-y0 loc-x0-y0)
    (connected loc-x5-y0 loc-x5-y5)
    (connected loc-x5-y5 loc-x0-y5)
    (connected loc-x5-y5 loc-x5-y0)
    ;(connected loc-x5-y0 loc-x0-y5)
    ;(connected loc-x0-y5 loc-x5-y0)
    (= (distance loc-x0-y0 loc-x5-y0) 5)
    (= (distance loc-x0-y0 loc-x0-y5) 15)
    (= (distance loc-x0-y5 loc-x5-y5) 9)
    (= (distance loc-x0-y5 loc-x0-y0) 15)
    (= (distance loc-x5-y0 loc-x0-y0) 5)
    (= (distance loc-x5-y0 loc-x5-y5) 5)
    (= (distance loc-x5-y5 loc-x0-y5) 9)
    (= (distance loc-x5-y5 loc-x5-y0) 5)
    (= (distance loc-x0-y5 loc-x5-y0) 10)
    (= (distance loc-x5-y0 loc-x0-y5) 10)
    (= (distance loc-x5-y5 loc-x5-y5) 0)
    (= (distance loc-x5-y0 loc-x5-y0) 0)
    (= (distance loc-x0-y5 loc-x0-y5) 0)
    (= (distance loc-x0-y0 loc-x0-y0) 0)
)
(:goal
(and 
    (visited loc-x5-y0)
    (visited loc-x0-y5)
    (visited loc-x5-y5)
)
)
(:metric minimize (total-cost))
)
