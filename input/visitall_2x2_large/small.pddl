(define (problem grid-2)
(:domain grid-visit-all)
(:objects 
    loc-x1-y1
    loc-x1-y17
    loc-x17-y2
    loc-x16-y18
- place 
        
)
(:init
    (at-robot loc-x1-y1)
    (visited loc-x1-y1)
    (connected loc-x1-y1 loc-x17-y2)
    (connected loc-x1-y1 loc-x1-y17)
    (connected loc-x1-y17 loc-x16-y18)
    (connected loc-x1-y17 loc-x1-y1)
    (connected loc-x17-y2 loc-x1-y1)
    (connected loc-x17-y2 loc-x16-y18)
    (connected loc-x16-y18 loc-x1-y17)
    (connected loc-x16-y18 loc-x17-y2)
    (= (distance loc-x1-y1 loc-x17-y2) 17)
    (= (distance loc-x1-y1 loc-x1-y17) 21)
    (= (distance loc-x1-y17 loc-x16-y18) 18)
    (= (distance loc-x1-y17 loc-x1-y1) 21)
    (= (distance loc-x17-y2 loc-x1-y1) 17)
    (= (distance loc-x17-y2 loc-x16-y18) 30)
    (= (distance loc-x16-y18 loc-x1-y17) 18)
    (= (distance loc-16-y18 loc-x17-y2) 30)
    (= (distance loc-x16-y18 loc-x16-y18) 0)
    (= (distance loc-x17-y2 loc-x17-y2) 0)
    (= (distance loc-x1-y17 loc-x1-y17) 0)
    (= (distance loc-x1-y1 loc-x1-y1) 0)
)
(:goal
(and 
    (visited loc-x1-y1)
    (visited loc-x17-y2)
    (visited loc-x1-y17)
    (visited loc-x16-y18)
)
)
(:metric minimize (total-cost))
)
