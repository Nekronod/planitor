use super::OrdF;

const PRECISION: i32 = 4;

pub fn round(n: OrdF) -> OrdF {
    let tenth_pow = 10.0f64.powi(PRECISION);
    ((n.0 * tenth_pow).trunc() / tenth_pow).into()
}

pub fn round_f(n: f64) -> f64 {
    let tenth_pow = 10.0f64.powi(PRECISION);
    (n * tenth_pow).trunc() / tenth_pow
}

pub fn approx_eq(a: OrdF, b: OrdF) -> bool {
    let comparator = 1.0 / 10.0f64.powi(PRECISION);
    (a - b).abs() < comparator
}

pub fn approx_f_eq(a: f64, b: f64) -> bool {
    let comparator = 1.0 / 10.0f64.powi(PRECISION);
    (a - b).abs() < comparator
}
