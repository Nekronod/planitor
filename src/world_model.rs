use std::{collections::HashMap, fmt::Debug};
pub(crate) mod abstract_planning;
pub(crate) mod concrete_model;
pub(crate) mod ha_sys_planer;
pub(crate) mod wm_parser;
pub(crate) mod wm_simulator;

use colored::{ColoredString, Colorize};

use crate::{
    hybridautomaton::{AnnotatedHA, LocationId},
    system::ha_system::HaPlan,
};

use self::{
    abstract_planning::{
        domain_handler::DomainHandler, AbstractWorldPlanner, PlanningInstruction,
    },
    concrete_model::{Coordinate, Pather},
    ha_sys_planer::HaRoutePlaner,
};

use super::OrdF;

pub(crate) struct Planner<D>
where
    D: DomainHandler,
{
    pub(crate) pather: HaRoutePlaner,
    abstract_planer: AbstractWorldPlanner<D>,
    cur_abstract_plan: Vec<PlanningInstruction>,
    cur_ins: Option<PlanningInstruction>,
}

impl<D: DomainHandler> Planner<D> {
    pub fn new(
        wm: WorldModel,
        goal: Position,
        start: Position,
        loc: LocationId,
        ha_dim_names: HashMap<String, usize>,
        domain_handler: D,
        input_pddl: String,
    ) -> Self {
        //let a_start = start.abstract_state.as_ref().map(|n| n.id).unwrap_or(0);
        let pather = Pather::new(wm, goal, start);

        let mut s = Self {
            pather: HaRoutePlaner::new(loc, pather, ha_dim_names, 0f64.into(), 0f64.into()),
            abstract_planer: AbstractWorldPlanner::new(domain_handler, input_pddl),
            cur_abstract_plan: vec![],
            cur_ins: None,
        };
        s.cur_abstract_plan = s.abstract_planer.compute_plan();
        s
    }

    pub(crate) fn is_abstract_empty(&self) -> bool {
        self.cur_abstract_plan.is_empty()
    }

    pub(crate) fn get_wm(&self) -> &WorldModel {
        self.pather.get_wm()
    }

    pub(crate) fn get_wm_mut(&mut self) -> &mut WorldModel {
        self.pather.get_wm_mut()
    }

    pub fn new_abstract_route(&self) -> Vec<PlanningInstruction> {
        self.abstract_planer.compute_plan()
    }

    pub fn get_maneuvers(&mut self, ha: &AnnotatedHA, angle: OrdF) -> HaPlan {
        self.pather.plan(ha, None, angle)
    }

    pub fn update_goal(&mut self, new_goal: Position) -> Result<(), &str> {
        self.pather.pather.update_goal(new_goal)
    }

    pub fn update_position(&mut self, cur_pos: Coordinate) -> Result<(), &str> {
        let pos = self.get_wm().position_from_cords(cur_pos);
        //dbg!(&pos, cur_pos);
        if let Field::Tile(TileType::Wall) = self.get_wm().get_field(&pos) {
            Err("Position update points to wall")
        } else {
            self.pather.pather.pos = pos;
            Ok(())
        }
    }

    pub fn get_distances_and_close_points(&self, cur_pos: Position) -> Vec<Position> {
        let positions = self.abstract_planer.dom_handler.distances().keys().collect::<Vec<_>>();
        let f_pos =positions.iter().map(|(p1,p2)|vec![p1,p2]).flatten().collect::<Vec<_>>();
        if let Some(PlanningInstruction::Move(_,g)) = &self.cur_ins {
            let mut c_pather = self.pather.pather.clone();
            c_pather.pos =g.clone();
            c_pather.update_goal_hard(cur_pos).unwrap();
            let distances = f_pos.into_iter().map(|p|c_pather.l_val.get(p)).collect::<Vec<_>>();
            //assert!(distances.len()==4);
            
        } else {
            if self.cur_ins.is_some() {
                panic!()
            } else {
                unreachable!()
            }
        }
        let cur_pos = &self.pather.pather.pos;
        let pather = self.pather.pather.clone();

        todo!()
    }


    #[allow(mutable_borrow_reservation_conflict)]
    pub(crate) fn plan(&mut self, aha: &AnnotatedHA, angle: OrdF) -> HaPlan {
        //dbg!(&self.cur_abstract_plan);
        let all_routing_done_but_not_finished = !self.cur_abstract_plan.is_empty()
            && self
                .cur_abstract_plan
                .iter()
                .all(|pi| matches!(pi, PlanningInstruction::Other(_)));
        for (ix, step) in self.cur_abstract_plan.iter().enumerate() {
            match step {
                PlanningInstruction::Move(from, to) => {
                    assert_eq!(from, &self.pather.pather.pos);
                    self.cur_ins = Some(step.clone());
                    if to == &self.pather.pather.goal {
                        //dbg!(&self.cur_abstract_plan);
                        self.cur_abstract_plan.drain(0..ix + 1);
                        //dbg!(&self.cur_abstract_plan);
                        break;
                    }
                    if let Err(msg) = self.update_goal(to.clone()) {
                        eprintln!("Planing error: {}, should not be reachable", msg.red());
                    }
                    //dbg!(&self.cur_abstract_plan);
                    self.cur_abstract_plan.drain(0..ix + 1);
                    //dbg!(&self.cur_abstract_plan);
                    break;
                }
                PlanningInstruction::Other(ins_name) => {
                    self.abstract_planer.dom_handler.performed_action(step);
                    println!("Executing non-move instruction: {}", ins_name.red());
                }
            }
        }
        if all_routing_done_but_not_finished {
            return HaPlan::empty(false);
        }
        let cur_pos = &self.pather.pather.pos;
        self.abstract_planer
            .dom_handler
            .set_position(cur_pos.clone())
            .unwrap();
        //dbg!("after abstract loop");
        let hp = self.get_maneuvers(aha, angle);
        dbg!(&hp);
        hp
    }
}

#[derive(Debug, Clone)]
pub struct WorldModel {
    grid: Grid,
    pub pos: Position,
    pub start: Position,
    pub goal: Option<Position>,
    pub size_x: OrdF,
    pub size_y: OrdF,
    wm_updates: Vec<Coordinate>,
}

#[derive(Debug, Clone)]
pub struct Position {
    //abstract_state: Option<Node>,
    x: Vec<isize>,
    y: Vec<isize>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum TileType {
    Walkable,
    Wall,
}

#[derive(Debug, Clone)]
pub enum Field {
    Tile(TileType),
    Grid(Grid),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Direction {
    South,
    East,
    West,
    North,
}

#[derive(Debug, Clone)]
pub struct Grid {
    grid: Vec<Vec<Field>>, //[x][y]
    width: usize,          // grid.len()
    height: usize,         // grid[0].len()
}

impl Position {
    /*
    fn new_abs(x: &[isize], y: &[isize]) -> Self {
        // outdated
        panic!()
    }
    */

    pub(crate) fn default() -> Self {
        Self::new(&[0], &[0])
    }

    pub(crate) fn new(x: &[isize], y: &[isize]) -> Self {
        assert_eq!(x.len(), y.len(), "requirers equal depth in both dimensions");
        Position {
            x: x.to_vec(),
            y: y.to_vec(),
        }
    }

    pub(crate) fn new_single<I: Into<isize>>(x: I, y: I) -> Self {
        let x: isize = x.into();
        let y: isize = y.into();
        Position {
            x: vec![x],
            y: vec![y],
        }
    }

    fn is_top_level(&self) -> bool {
        assert_eq!(self.x.len(), self.y.len(), "corrupted position");
        self.x.len() == 1
    }

    pub(crate) fn extract_top_idx(&self) -> (usize, usize, Self) {
        let x = self.x[0];
        let y = self.y[0];
        let rec_lower_pos = Self::new(&self.x[1..], &self.y[1..]);
        (x as usize, y as usize, rec_lower_pos)
    }

    pub(crate) fn len(&self) -> usize {
        assert_eq!(self.x.len(), self.y.len(), "corrupted position");
        self.x.len()
    }

    /// Only call if not [is_top_level]
    fn upper_pos(&self) -> Position {
        let Position { x, y } = self;
        assert_eq!(x.len(), y.len(), "corrupted position");
        assert!(x.len() > 1, "parent of top-level position");
        let p_x = x.chunks_exact(x.len() - 1).next().unwrap().to_vec();
        let p_y = y.chunks_exact(y.len() - 1).next().unwrap().to_vec();
        Position::new(&p_x, &p_y)
    }
}

impl WorldModel {
    pub(crate) fn get_grid(&self) -> &Grid {
        &self.grid
    }

    pub(crate) fn get_grid_mut(&mut self) -> &mut Grid {
        &mut self.grid
    }

    pub fn get_field(&self, pos: &Position) -> &Field {
        self.try_get_field(pos).expect("expect valid field address")
    }

    pub fn get_field_mut(&mut self, pos: &Position) -> &mut Field {
        self.try_get_field_mut(pos)
            .expect("expect valid field address")
    }

    pub fn try_get_field(&self, pos: &Position) -> Option<&Field> {
        let idx_v: Vec<(isize, isize)> = pos.x.iter().cloned().zip(pos.y.iter().cloned()).collect();
        let (first_x, first_y) = idx_v.first().expect("empty pos access");
        let start_field = self
            .grid
            .grid
            .get(*first_y as usize)
            .and_then(|v| v.get(*first_x as usize));

        if idx_v.len() == 1 {
            return start_field;
        }
        idx_v.iter().skip(1).fold(start_field, |acc, (x, y)| {
            if let &Some(Field::Grid(g)) = &acc {
                let f: Option<&Field> = g.grid.get(*y as usize).and_then(|v| v.get(*x as usize));
                f
            } else {
                None
            }
        })
    }

    pub fn try_get_field_mut(&mut self, pos: &Position) -> Option<&mut Field> {
        let idx_v: Vec<(isize, isize)> = pos.x.iter().cloned().zip(pos.y.iter().cloned()).collect();
        let (first_x, first_y) = idx_v.first().expect("empty pos access");
        let start_field = self
            .grid
            .grid
            .get_mut(*first_y as usize)
            .and_then(|v| v.get_mut(*first_x as usize));
        idx_v.iter().skip(1).fold(start_field, |acc, (x, y)| {
            if let Some(Field::Grid(g)) = acc {
                let f: Option<&mut Field> = g
                    .grid
                    .get_mut(*y as usize)
                    .and_then(|v| v.get_mut(*x as usize));
                f
            } else {
                None
            }
        })
    }

    pub fn get_tile(&self, pos: &Position) -> &TileType {
        if let Field::Tile(tt) = self.get_field(pos) {
            tt
        } else {
            panic!("invalid position access - tile position led to grid")
        }
    }

    pub fn neighbors(&self, pos: &Position) -> Vec<Position> {
        let mut candidates = Vec::new();
        let n = self.get_dir_neighbor(pos, Direction::North);
        let e = self.get_dir_neighbor(pos, Direction::East);
        let s = self.get_dir_neighbor(pos, Direction::South);
        let w = self.get_dir_neighbor(pos, Direction::West);
        //dbg!(&n);
        candidates.extend(n.into_iter());
        candidates.extend(e.into_iter());
        candidates.extend(s.into_iter());
        candidates.extend(w.into_iter());

        //dbg!(&candidates);
        candidates
            .into_iter()
            .filter(|c| {
                //dbg!(c, pos, self.distance(c, pos), c.len() == pos.len());
                if c.len() == pos.len() {
                    //dbg!("eq length");
                    //true

                    (self.distance(c, pos) - OrdF::from(1.0 / (2.0f64.powi(c.len() as i32 - 1))))
                        .abs()
                        < 0.000005f64
                } else {
                    //dbg!("not eq length");
                    true
                }
            })
            .collect()
    }

    fn get_smaller_neighbor(&self, pos: &Position, dir: Direction) -> Vec<Position> {
        let g = if let Field::Grid(g) = self.get_field(pos) {
            g
        } else {
            unreachable!()
        };

        // we are coming from the matched direction
        let (x_r, y_r) = match dir.opposite_dir() {
            Direction::South => (0..g.width, (g.height - 1)..=(g.height - 1)),
            Direction::East => ((g.width - 1)..g.width, 0..=g.height - 1),
            Direction::West => (0..1, 0..=g.height - 1),
            Direction::North => (0..g.width, 0..=0),
        };

        let mut res = Vec::new();
        for x in x_r {
            for y in y_r.clone() {
                let mut cur = pos.clone();
                cur.x.push(x as isize);
                cur.y.push(y as isize);

                let mut v = match self.try_get_field(&cur) {
                    Some(f) => match f {
                        Field::Tile(t) => match t {
                            TileType::Walkable => vec![cur],
                            TileType::Wall => vec![],
                        },
                        Field::Grid(_inner_grid) => self.get_smaller_neighbor(&cur, dir),
                    },
                    None => todo!("empty return or error - check in detail"),
                };
                res.append(&mut v);
            }
        }
        res
    }

    fn get_dir_neighbor(&self, pos: &Position, dir: Direction) -> Vec<Position> {
        //dbg!(pos,dir);
        let mut cur = pos.clone();
        let offset = dir.offset();
        let new_x = (cur.x[pos.x.len() - 1] as i16) + offset.0;
        let new_y = (cur.y[pos.y.len() - 1] as i16) + offset.1;
        //dbg!(new_x,new_y);
        if new_x < 0 {
            /*
            assert_eq!(dir, Direction::West);
            let mut upper_left = cur.upper_pos();
            let l = upper_left.len();
            upper_left.x[l -1] -= 1;
            if upper_left.x[l-1] <0 {
                todo!("recursion");
            } else {
                if let Some(f) = self.try_get_field(&upper_left) {
                    match f {
                        Field::Grid(g) => {
                            todo!()
                        }
                        Field::Tile(_) => {
                            return self.get_dir_neighbor(&pos.upper_pos(), dir);
                        }
                    }
                }
            }
            */
        } else if new_y < 0 {
            //??? todo
            //todo!()
        }
        cur.x[pos.x.len() - 1] = new_x as isize;
        cur.y[pos.y.len() - 1] = new_y as isize;
        if let Some(f) = self.try_get_field(&cur) {
            match f {
                Field::Tile(t) => match t {
                    TileType::Walkable => vec![cur],
                    TileType::Wall => vec![],
                },
                Field::Grid(_inner_grid) => self.get_smaller_neighbor(&cur, dir),
            }
        } else if !cur.is_top_level() {
            //get bigger neighbors
            self.get_dir_neighbor(&cur.upper_pos(), dir)
        } else {
            //no neighbor in this direction
            Vec::new()
        }
    }
}

impl Direction {
    fn offset(&self) -> (i16, i16) {
        match self {
            Direction::South => (0, 1),
            Direction::East => (1, 0),
            Direction::West => (-1, 0),
            Direction::North => (0, -1),
        }
    }

    fn opposite_dir(&self) -> Self {
        use Direction::*;
        let v = vec![North, East, South, West];
        let self_idx = v.iter().position(|x| x == self).unwrap();
        v[(self_idx + 2) % 4].to_owned()
    }
}

impl Grid {

    fn g_refine_grid(self) -> Self {
        let w = self.width;
        let h = self.height;
        let gvv = self.grid;

        let new_grid_grid = gvv.into_iter().map(|gv| {
            gv.into_iter().map(|f| {
                match f {
                    Field::Tile(t) => {
                        match t {
                            TileType::Wall => {
                                let g =  Grid::new_blocked(2, 2);
                                Field::Grid(g)
                            }
                            TileType::Walkable =>  Field::Grid(Grid::new_clear(2, 2))
                        }
                       
                    },
                    Field::Grid(g) => Field::Grid(Grid::g_refine_grid(g)),
                }
            }).collect()

        }).collect();

        Grid { grid: new_grid_grid, width:w, height: h }
    }

    fn new_clear(w: usize, h:usize) -> Self {
        let mut outer = Vec::new();
        for _ in 0..h {
            let mut inner = Vec::new();
            for _ in 0..w {
                inner.push(Field::Tile(TileType::Walkable))
            }
            outer.push(inner);
        }
        let grid= outer;
        Grid { grid, width: w, height: h }
    }

    fn new_blocked(w: usize, h:usize) -> Self {
        let mut outer = Vec::new();
        for _ in 0..h {
            let mut inner = Vec::new();
            for _ in 0..w {
                inner.push(Field::Tile(TileType::Wall))
            }
            outer.push(inner);
        }
        let grid= outer;
        Grid { grid, width: w, height: h }
    }

    fn to_string(&self) -> String {
        self.to_colored_string().to_string()
    }

    fn to_colored_string(&self) -> ColoredString {
        self.to_string_rec(0, &mut Vec::new(), None, 0.0f64.into())
    }

    fn to_string_annotated(&self, pos: Option<&Position>, angle: OrdF) -> colored::ColoredString {
        self.to_string_rec(0, &mut Vec::new(), pos, angle)
    }

    fn to_string_rec(
        &self,
        n: usize,
        v: &mut Vec<(colored::ColoredString, usize)>,
        pos: Option<&Position>,
        angle: OrdF,
    ) -> colored::ColoredString {
        use colored::*;
        let mut count = n;
        let (h_x, h_y, rec_pos) = if let Some(p) = pos {
            let (a, b, c) = p.extract_top_idx();
            (a, b, Some(c))
        } else {
            (usize::MAX, usize::MAX, None)
        };
        let str_v: Vec<String> = self
            .grid
            .iter()
            .enumerate()
            .map(|(y_ix, inner_vec)| {
                inner_vec
                    .iter()
                    .enumerate()
                    .map(|(x_ix, t)| match t {
                        Field::Tile(t) if (x_ix, y_ix) == (h_x, h_y) => {
                            assert!(t == &TileType::Walkable);
                            let mut af = angle.0 % 360.0;
                            while af < 0.0 {
                                af += 360.0;
                            }
                            //dbg!(af);
                            match af {
                                a if a > 355.0 || a < 5.0 => "➜".red(),     //0
                                a if a > 85.0 && a < 95.0 => "↓".red(),     // 90
                                a if a > 175.0 && a < 185.0 => "←".red(),   // 180
                                a if a > 265.0 && a < 275.0 => "↑".red(),   // 270
                                a if a >= 5.0 || a <= 85.0 => "⇘".red(),    // 0-90
                                a if a >= 95.0 || a <= 175.0 => "⇙".red(),  //90-180
                                a if a >= 185.0 || a <= 265.0 => "⇖".red(), //180-270
                                a if a >= 275.0 || a <= 355.0 => "⇗".red(), // 270-0
                                _ => unreachable!(),
                            }
                        }
                        Field::Tile(t) => match t {
                            TileType::Wall => "X".to_string().blue(),
                            TileType::Walkable => "_".to_string().blue(),
                        },
                        Field::Grid(g) => {
                            count += 1;
                            let rec = g.to_string_rec(count, v, rec_pos.as_ref(), angle);
                            v.push((rec, count));
                            let max_rec_count = v.iter().max_by(|a, b| a.1.cmp(&b.1)).unwrap().1;
                            let ret = if (x_ix, y_ix) == (h_x, h_y) {
                                count.to_string().red()
                            } else {
                                count.to_string().white()
                            };

                            count = max_rec_count;
                            ret
                        }
                    })
                    .collect::<Vec<_>>()
                    .into_iter()
                    .fold("".to_string(), |acc, n| format!("{}{}", acc, n))
            })
            .collect();
        let mut res = str_v.join("\n");
        if n == 0 {
            v.iter()
                .for_each(|(s, idx)| res += &format!("\n{}:\n{}\n", idx, s));
            res.normal()
        } else {
            //inner_rec.push((res,n));
            res.normal()
        }
    }
}

impl std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

impl std::fmt::Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.len() == 1 {
            write!(f, "({},{})", self.x[0], self.y[0])
        } else {
            let x = self
                .x
                .iter()
                .map(|i| i.to_string())
                .collect::<Vec<_>>()
                .join(", ");
            let y = self
                .y
                .iter()
                .map(|i| i.to_string())
                .collect::<Vec<_>>()
                .join(", ");
            write!(f, "([{}],[{}])", x, y)
        }
    }
}
