#![allow(dead_code)]
use std::{fs, path::PathBuf, str::FromStr, collections::HashMap};

use hybridautomaton::{ha_parser::HAParser, simulator::Simulator};
use monitor::Monitor;
use ordered_float::OrderedFloat;

mod help_lib;
mod hybridautomaton;
mod monitor;
mod system;
mod world_model;
pub use help_lib::round;

#[cfg(not(feature = "precise"))]
#[macro_use]
extern crate float_cmp;

use clap::{ArgEnum, Parser};
use system::{ha_system::HaSystem, selector::Selector};
use world_model::Position;

use crate::world_model::{
    abstract_planning::{domain_handler::{DummyHandler, DomainHandler}, visitall_handler::VisitAllHandler},
    wm_parser::WMParser,
    Planner,
};

type OrdF = OrderedFloat<f64>;

/// Monitor guided Abstraction Refinement
///
///
#[derive(Parser, Debug, PartialEq)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    /// What mode to execute
    #[clap(arg_enum)]
    mode: Mode,

    /// Path to the System modelling automaton file
    #[clap(short, long, value_name = "FILE")]
    automaton: Option<String>,

    /// Path to the World Model file
    #[clap(short, long, value_name = "FILE")]
    world_model: Option<String>,

     /// Path to the World Model file
    #[clap(long = "ha-wm", value_name = "FILE")]
    ha_world_model: Option<String>,

    /// The RTLola monitor specification
    #[clap(short, long, value_name = "FILE")]
    specification: Option<String>,

    /// Path to the specified System
    #[clap(long = "sys", value_name = "FILE")]
    system_path: Option<String>,

    /// Path to the specified pddl Problem file
    #[clap(short, long, value_name = "FILE")]
    problem: Option<String>,

    ///Number of traces for Ha running
    #[clap(short = 'n', long, value_name = "uint")]
    num_traces: Option<usize>,

    ///Number of traces for Ha running
    #[clap(short, long, value_name = "uint")]
    refinements: Option<usize>,
     
    ///Number of traces for Ha running
    #[clap(short = 'm', value_name = "uint")]
    length_traces: Option<usize>,

}

#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ArgEnum)]
enum Mode {
    Monitor,
    HaParse, //ha-parse
    WmParse, //wm-parse
    SpecParse, //spec-parse
    Path,
    HaRuns, //ha-runs
    Planner,
}

fn main() -> Result<(), std::io::Error> {
    let cli = Cli::parse();
    match &cli.mode {
        Mode::Monitor => {}
        Mode::Planner => {
            /// This Parts mocks the concrete world model and other computations in order to test ONLY the abstract planner
            use std::time::SystemTime;

            let problem_file = cli.problem.expect("no -p problem pddl file argument given");
                if problem_file.contains("visitall") {

                    let time_init = SystemTime::now();

                    let pddl_str = fs::read_to_string(problem_file.clone()).expect("no valid pddl file");
                    let dh = VisitAllHandler::new(pddl_str.clone());
                    let mut awp = world_model::abstract_planning::AbstractWorldPlanner::new(dh, problem_file.clone());

                    let setup_time = SystemTime::now();
                    let time_diff = setup_time
                    .duration_since(time_init)
                    .expect("Time went backwards");
                    println!("Parsing done after {}ms.",time_diff.as_millis());


                    let init_plan = awp.compute_plan();

                
                    let plan_time = SystemTime::now();
                    let time_diff = plan_time
                    .duration_since(setup_time)
                    .expect("Time went backwards");
                    println!("Init plan created after {}ms. Plan length: {}",time_diff.as_millis(), init_plan.len());

                    
                    let mut before_planning = SystemTime::now();
                    let mut dh1_time ;

                    if problem_file.contains("2lane") {
                        let n =  cli.num_traces.unwrap_or(100);
                        for runs in 0..n {
                            
                            dh1_time = SystemTime::now();
                            let time_diff = dh1_time
                            .duration_since(before_planning)
                            .expect("Time went backwards");
                            println!("Iteration: {}, last_plan time diff: {}",runs,time_diff.as_millis());

                            let idx = runs;
                            //5*(idx+1) no errors  --- 5*idx+2 with errors
                            let new_position= Position::new_single((5*idx+2) as isize, 0);
                            let _ = DomainHandler::set_position(&mut awp.dom_handler,new_position.clone()).expect("Should be valid");
                            
                            let cur_start= Position::new_single((5*idx) as isize, 0);
                            let cur_goal= Position::new_single((5*idx+5) as isize, 0);
                            DomainHandler::update_distance(&mut awp.dom_handler, &cur_start, &cur_goal, 20);

                            let passed_positions = (idx..=idx).map(|ix|{
                                let vis_p = Position::new_single((5*ix) as isize, 0);
                                let (x,y,_) = vis_p.extract_top_idx();
                                let s = format!("(visited loc-x{}-y{})", x, y);
                                s
                            }).collect::<Vec<_>>();
                            DomainHandler::update_goal(&mut awp.dom_handler, passed_positions);

                            let bl = Position::new_single((5*idx) as isize, 5);
                            let br = Position::new_single((5*idx+5) as isize, 5);

                            //need for error cases
                            let mut tmp_distances = HashMap::new();
                            tmp_distances.insert((bl.clone(), new_position.clone()),7);
                            tmp_distances.insert((new_position.clone(), bl.clone()),7);
                            tmp_distances.insert((br.clone(), new_position.clone()),12);
                            tmp_distances.insert((new_position.clone(), br.clone()),12);
                            tmp_distances.insert((new_position.clone(), cur_start.clone()),2);
                            tmp_distances.insert((cur_start.clone(), new_position.clone()),2);
                            tmp_distances.insert((cur_goal.clone(), new_position.clone()),17);
                            tmp_distances.insert((new_position.clone(), cur_goal.clone()),17);
                            
                            awp.dom_handler.set_tmp_pos(new_position, tmp_distances);



                            before_planning = SystemTime::now();
                            let time_diff = before_planning
                            .duration_since(dh1_time)
                            .expect("Time went backwards");
                            println!("Planning now! Update time: {}",time_diff.as_millis());

                            let plan = awp.compute_plan();

                            println!("Computed Plan length: {}",plan.len());
                            
                        }
                        let done_time = SystemTime::now();
                        let time_diff = done_time
                        .duration_since(plan_time)
                        .expect("Time went backwards");
                        println!("Init plan created after {}ms. Plan length: {}",time_diff.as_millis(), init_plan.len());
                    }


                } else {
                    todo!("other pddl domains");
                }
            return Ok(());
        }
        Mode::HaParse => {
            let ha_string = cli.automaton.clone().unwrap();
            let ha_string = fs::read_to_string(ha_string).expect("unable to read file");

            let mut ha_parser = HAParser::new();
            let ha = ha_parser.parse(ha_string);
            match ha {
                Ok(ha) => println!("{:?}", ha),
                Err(err) => {
                    println!("{:?}", err);
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        "invalid ha input",
                    ));
                }
            }

            return Ok(());
        },
        Mode::WmParse => {
            let wm_string = cli.world_model.clone().unwrap();
            let wm_string = fs::read_to_string(wm_string).expect("unable to read file");

            let wm_parser = WMParser::new();
            let wm = wm_parser.into_wm(wm_string);
            match wm {
                Ok(wm) => {
                    let mut s = String::new();
                    print!("Want to inspect the result? [y/n]");
                    let _ = std::io::Write::flush(&mut std::io::stdout());
                    std::io::stdin()
                        .read_line(&mut s)
                        .expect("Did not enter a correct string");
                    if let Some('\n') = s.chars().next_back() {
                        s.pop();
                    }
                    if let Some('\r') = s.chars().next_back() {
                        s.pop();
                    }
                    if s == "y" {
                        println!("{}, {}", wm.size_x, wm.size_y);
                        println!("Start: {}", wm.start);
                        if let Some(g) = &wm.goal {
                            println!("Goal: {}", g);
                        }
                        println!("{}", wm.get_grid().to_string());
                    } else if s == "n" {
                        println!("no print requested");
                    } else {
                        println!("unknown input selected, shutting down");
                    }
                    return Ok(());
                }
                Err(pe) => {
                    println!("Model parsing failed");
                    println!("{}", pe);
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        "invalid model input",
                    ));
                }
            }
        }
        Mode::SpecParse => {
            let spec_path = cli.specification.clone().unwrap();

            let cfg =
                rtlola_frontend::ParserConfig::from_path(PathBuf::from_str(&spec_path).unwrap())?;
            let mir = rtlola_frontend::parse(cfg.clone());
            match mir {
                Ok(ir) => {
                    println!("Spec parse successful");
                    let mut s = String::new();
                    print!("Want to inspect the result? [y/n/debug]");
                    let _ = std::io::Write::flush(&mut std::io::stdout());
                    std::io::stdin()
                        .read_line(&mut s)
                        .expect("Did not enter a correct string");
                    if let Some('\n') = s.chars().next_back() {
                        s.pop();
                    }
                    if let Some('\r') = s.chars().next_back() {
                        s.pop();
                    }
                    if s == "y" {
                        println!("{:?}", ir);
                    } else if s == "n" {
                        println!("no print requested");
                    } else if s == "debug" {
                        println!("{:#?}", ir);
                    } else {
                        println!("unknown input selected, shutting down");
                    }

                    return Ok(());
                }
                Err(err) => {
                    println!("Spec parse failed");
                    let handler = rtlola_frontend::Handler::from(cfg);
                    err.iter().for_each(|d| handler.emit(d));
                    println!("{:?}", err);
                    return Err(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        "invalid spec input",
                    ));
                }
            }
        }
        Mode::Path => {
            use std::time::SystemTime;
            let refinements = cli.refinements.unwrap_or(0);
            println!("{} refinements",refinements);
            //const NUM_REFINES: usize = 0;

            let time_init = SystemTime::now();
            let wm_string = cli.world_model.clone().unwrap();
            let wm_string = fs::read_to_string(wm_string).expect("unable to read file");

            let wm_parser = WMParser::new();
            let mut wm = wm_parser.into_wm(wm_string).expect("Could not compute Path on invalid WM");

        
            let goal = wm.goal.clone().unwrap_or(Position::new(&[1], &[1]));
            
            (1..=refinements).for_each(|_| wm.refine_global(vec![]));


            let s_x = vec![0;refinements+1];
            let s_y = vec![0;refinements+1];

            let mut g_x = vec![1 as isize;refinements+1];
            let mut g_y = vec![1 as isize;refinements+1];

            let start = if refinements > 0 {
                    Position::new(&s_x,&s_y)
                } else {
                    wm.start.clone()
                };
            let (x,y,_) = goal.extract_top_idx();
            g_x[0]= x as isize;
            g_y[0]= y as isize;
            let goal = Position::new(&g_x,&g_y);

            //dbg!(&goal,&start);
            

            let parsing_time = SystemTime::now();
            let parsing_time = parsing_time
            .duration_since(time_init)
            .expect("Time went backwards");
            println!("Parsing done after {}ms. Map Size: {}",parsing_time.as_millis(), wm.size_x * wm.size_y);

            let time_start = SystemTime::now();

            let mut pather = world_model::concrete_model::Pather::new(wm, goal, start);

            let mut route= vec![];
            while let Some(p) = pather.next_move() {
                //(&pather_c.l_val[&p],&p); //, &self.pather.goal);
                if route.contains(&p) {
                    panic!("route position duplicate");
                }
                route.push(p.clone());
                pather.perform_move(p);
            }

            let time_after = SystemTime::now();
            let since_the_epoch = time_after
                .duration_since(time_start)
                .expect("Time went backwards");
            println!("Route length: {}", route.len());
            println!("Seconds: {:?}", since_the_epoch.as_secs());
            println!("Milliseconds: {:?}", since_the_epoch.as_millis());

            println!("Map size: {}", pather.g_val.len());
            println!("Pather mem size: {}", std::mem::size_of_val(&pather));
            //println!("{:?}",route);
            return Ok(());

        },
        Mode::HaRuns => {
            use num::Zero;
            use hybridautomaton::timed_trace::TraceBuilder;
            use std::time::SystemTime;

            let time_init = SystemTime::now();
            println!("Starting Setup ...");

            let n = cli.num_traces.unwrap_or(1000); //length traces
            let m= cli.length_traces.unwrap_or(100);  //num traces

            let ha_string = cli.automaton.clone().unwrap();
            let mut sm_aha = HAParser::aha_from_file(ha_string.clone());

            let sys_string = cli.system_path.clone().unwrap();
            let sys_aha = if sys_string == ha_string {sm_aha.clone() } else { HAParser::aha_from_file(sys_string)};

            assert_eq!(sm_aha.ha.dimension(), sys_aha.ha.dimension());


            let parsing_time = SystemTime::now();
            let parsing_time2 = parsing_time
            .duration_since(time_init)
            .expect("Time went backwards");
            println!("Parsing HA time {}",parsing_time2.as_secs());

            let mut simulator = Simulator::new(&sys_aha.ha);
            let init = vec![hybridautomaton::basics::Atom::zero(); sys_aha.ha.dimension()];
            let traces = simulator.random_walks(m, n, init.clone());

            let mut tb =TraceBuilder::new(init, sys_aha.initial_location());
            let event_traces = traces.into_iter().map(|t|t.into_input_events()).collect::<Vec<_>>();


            let tracing_time = SystemTime::now();
            let tracing_time = tracing_time
            .duration_since(parsing_time)
            .expect("Time went backwards");
            println!("Trace Generation done after {}s. HA Size: {} Mode, {} Edges, {} Dimensions",tracing_time.as_secs(), sys_aha.ha.num_modes(), sys_aha.ha.num_jumps() , sys_aha.ha.dimension());
            let time_start = SystemTime::now();
            let mut update_counter = 0;
            for et in event_traces {
                et.into_iter().for_each(|e| {
                    tb.update_event(&sm_aha.ha, e);
                });
                let u = tb.extract_updates();
                update_counter += u.len();
                sm_aha.ha.update_given_events(u).unwrap();
                //updates.append(&mut u);
                tb.hard_reset();
            }

            let time_after = SystemTime::now();
            let since_the_epoch = time_after
                .duration_since(time_start)
                .expect("Time went backwards");
            println!("Traces: {}, Trace Length: {}, updates/errors encountered: {}", n, m, update_counter);
            println!("Seconds: {:?}", since_the_epoch.as_secs());
            println!("Milliseconds: {:?}", since_the_epoch.as_millis());
    
            return Ok(());
        },
    }

    // mode == monitor
    let ha_string = cli
        .automaton
        .clone()
        .map(|s| fs::read_to_string(s).expect("unable to read file"))
        .expect("No correct Automaton file provided, requirers  --automaton / -a <FILE>");
    let ha_parser = HAParser::new();
    let aha = match ha_parser.annotated_ha(ha_string) {
        Ok(aha) => aha,
        Err(err) => panic!("{:?}", err),
    };

    let wm_string = cli
        .world_model
        .clone()
        .map(|s| fs::read_to_string(s).expect("unable to read file"))
        .expect("No correct WorldModel file provided, requirers  --wm-model / -w <FILE>");
    let wm = crate::world_model::wm_parser::WMParser::new().into_wm(wm_string);

    let wm = match wm {
        Ok(wm) => wm,
        Err(err) => panic!("{}", err),
    };

    let ha_wm_string = cli
        .ha_world_model
        .unwrap_or(cli.world_model.clone().expect("Expected ha-wm or wm input"));
    let ha_wm_string = fs::read_to_string(ha_wm_string).expect("unable to read file");
    let ha_wm = crate::world_model::wm_parser::WMParser::new().into_wm(ha_wm_string);

    let ha_wm = match ha_wm {
        Ok(ha_wm) => ha_wm,
        Err(err) => panic!("{}", err),
    };

    let spec = cli
        .specification
        .map(|s| fs::read_to_string(s).expect("unable to read file"))
        .expect("No correct Specification file provided, requirers  --specification / -s <FILE>");

    let sys_string = cli
        .automaton
        .map(|s| fs::read_to_string(s).expect("unable to read file"))
        .expect("No correct Automaton SYSTEM file provided, requirers  --system / <FILE>");

    let ha_parser = HAParser::new();
    let system = match ha_parser.annotated_ha(sys_string) {
        Ok(aha) => aha,
        Err(err) => panic!("{:?}", err),
    };

    let problem_loc = cli.problem;

    let select = crate::system::selector::RandomSelector::new();
    let init = select.select_all(&system.initial_mode().dynamics.0);

    let start = wm.start.clone();
    let goal = wm.goal.clone().unwrap_or(Position::new(&[1], &[1]));

    let init_loc = aha.initial_location();

    fs::write("planning_log.txt", "").unwrap();

    if let Some(pddl_file) = problem_loc {
        if pddl_file.contains("visitall") {
            let pddl_str = fs::read_to_string(pddl_file.clone()).unwrap();
            let dh = VisitAllHandler::new(pddl_str);
            let planner = Planner::new(
                wm,
                goal,
                dh.init_loc.clone(),
                init_loc,
                aha.dim_names.clone(),
                dh,
                pddl_file,
            );
            let ha_backup_plans = world_model::ha_sys_planer::construct_stable_routes(&aha);
            let mut monitor = Monitor::new(spec, aha, init.clone(), ha_backup_plans, planner);

            let plan = monitor.plan();
            //dbg!(&plan);
            //ha_wm.update_wm_pos(&Position::new(&[6],&[5])); // todo remove
            let mut ha_system = HaSystem::new(&system, &ha_wm, &select, init);

            let res = ha_system.run(plan, &mut monitor);
            println!("{:?}", res);
        } else {
            unimplemented!()
        }
    } else {
        let dh = DummyHandler {};
        let planner = Planner::new(
            wm,
            goal,
            start,
            init_loc,
            aha.dim_names.clone(),
            dh,
            "".to_string(),
        );
        let ha_backup_plans = world_model::ha_sys_planer::construct_stable_routes(&aha);
        let mut monitor = Monitor::new(spec, aha, init.clone(), ha_backup_plans, planner);

        let direct_plan = monitor.plan();
        //ha_wm.update_wm_pos(&Position::new(&[8],&[7])); // todo remove
        let mut ha_system = HaSystem::new(&system, &ha_wm, &select, init);

        let res = ha_system.run(direct_plan, &mut monitor);
        println!("{:?}", res);

        /*

        monitor.planner.update_goal(Position::new_single(8i16, 3i16)).expect("update should work?");
        dbg!("main up pos", &monitor.last_known_pos);
        //monitor.planner.update_position(world_model::concrete_model::Coordinate::from((3.5f64,3.5f64)));
        dbg!("main up pos2", &monitor.last_known_pos);
        let new_plan = monitor.plan();
        ha_system.run(new_plan, &mut monitor);

        monitor.planner.update_goal(Position::new_single(1i16, 3i16)).expect("update should work?");
        dbg!("main up pos", &monitor.last_known_pos);
        //monitor.planner.update_position(world_model::concrete_model::Coordinate::from((3.5f64,3.5f64)));
        dbg!("main up pos2", &monitor.last_known_pos);
        let new_plan = monitor.plan();
        ha_system.run(new_plan, &mut monitor);

        */
    };
    Ok(())
}
