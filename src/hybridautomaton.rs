use crate::hybridautomaton::basics::{transform, Atom, Rectangle, Scalar};
use crate::hybridautomaton::timed_trace::{ActionId, BlindStep, Step};
use gcollections::ops::Intersection;
pub use location::{Location, LocationId};
use std::ops::IndexMut;
use std::{collections::HashMap, ops::Index};

use self::timed_trace::{ObservableTrace, TimedTrace, EventUpdate};

pub(crate) mod basics;
//mod builder;
pub mod location;
//pub(crate) mod minimizer;
pub(crate) mod ha_parser;
pub(crate) mod pretty_print;
pub(crate) mod timed_trace;
pub(crate) mod randomwalker;
pub(crate) mod simulator;
pub(crate) mod walker;

macro_rules! map(
    { $($key:expr => $value:expr),+ } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key, $value);
            )+
            m
        }
     };
);

pub(crate) type AdjacencyMatrix<T> = HashMap<(T, ActionId, T), Rectangle>;
pub(crate) type DetAdjacencyMatrix<T> = HashMap<(T, ActionId), (T, Rectangle)>;
pub(crate) type EdgeRef<'a, T> = (&'a (T, ActionId, T), &'a Rectangle);

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Dynamics(pub Vec<Scalar>);
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ConcreteDynamics(pub Vec<Atom>);

impl From<Vec<Scalar>> for Dynamics {
    fn from(d: Vec<Scalar>) -> Self {
        Dynamics(d)
    }
}

impl From<Vec<Atom>> for ConcreteDynamics {
    fn from(d: Vec<Atom>) -> Self {
        ConcreteDynamics(d)
    }
}

impl From<Vec<Atom>> for Dynamics {
    fn from(d: Vec<Atom>) -> Self {
        ConcreteDynamics::from(d).as_abstract()
    }
}

impl ConcreteDynamics {
    pub fn as_abstract(self) -> Dynamics {
        self.0
            .into_iter()
            .map(Scalar::singular)
            .collect::<Vec<Scalar>>()
            .into()
    }
    fn dimension(&self) -> usize {
        self.0.len()
    }
}

impl Dynamics {
    #[allow(dead_code)]
    fn as_concrete<F>(self, concertizer: F) -> ConcreteDynamics
    where
        F: Fn((usize, Scalar)) -> Atom,
    {
        self.0
            .into_iter()
            .enumerate()
            .map(concertizer)
            .collect::<Vec<Atom>>()
            .into()
    }
    fn dimension(&self) -> usize {
        self.0.len()
    }
}

impl Index<usize> for Dynamics {
    type Output = Scalar;
    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl IndexMut<usize> for Dynamics {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

impl Index<usize> for ConcreteDynamics {
    type Output = Atom;
    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl IndexMut<usize> for ConcreteDynamics {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

#[derive(Debug, Clone)]
pub struct HA {
    locations: HashMap<LocationId, Location>,
    adjacency: AdjacencyMatrix<LocationId>,
    pub initial_location: LocationId,
    next_loc_id: usize,
}

#[derive(Debug, Clone)]
pub struct AnnotatedHA {
    pub ha: HA,
    pub(crate) dim_names: HashMap<String, usize>,
    pub(crate) idle_states: Vec<LocationId>,
    pub(crate) state_map: HashMap<String, LocationId>,
}

#[derive(Debug, Clone)]
pub enum HAConstructionError {
    UnknownLocation(LocationId),
    UnknownSourceLocation(ActionId, LocationId),
    UnknownTargetLocation(ActionId, LocationId),
    InvalidLocationPosition(LocationId, usize),
    InconsistentDimension {
        expected: usize,
        was: usize,
        in_loc: LocationId,
    },
    DoubleInitialLocation,
    NonExistentInitialInvariant,
    NonExistentInitialLocation,
    DoubleInitializeInvariant,
}

impl AnnotatedHA {
    pub fn initial_location(&self) -> LocationId {
        self.ha.initial_location
    }

    pub fn get_location(&self, loc: LocationId) -> &Location {
        self.ha.get_location(loc)
    }

    pub fn initial_mode(&self) -> &Location {
        self.ha.initial_mode()
    }

    pub fn get_action_for_pair(&self, from: LocationId, to: LocationId) -> ActionId {
        self.ha
            .successors_actions(from)
            .iter()
            .find(|(_, suc)| *suc == to)
            .unwrap()
            .0
    }
}

impl HA {
    /// Creates a new hybrid automaton out of the given list of locations and jumps.  The creation is _safe_ in that
    /// it checks the consistency of the passed arguments.  More specifically, it checks whether the id of a location
    /// matches its index in the vector, and whether all jumps have valid sources and targets.
    /// The set of state variables are collected based on their occurrence in the locations and jumps.  If further
    /// variables are required, refer to `HA::add_state_variable(&mut self, new: Variable)`.
    pub fn new(
        locations: HashMap<LocationId, Location>,
        adjacency: AdjacencyMatrix<LocationId>,
        initial_location: LocationId,
    ) -> Result<HA, HAConstructionError> {
        for ((src, action, tar), _) in &adjacency {
            if locations.get(&src).is_none() {
                return Err(HAConstructionError::UnknownSourceLocation(*action, *src));
            }
            if locations.get(&tar).is_none() {
                return Err(HAConstructionError::UnknownTargetLocation(*action, *tar));
            }
        }
        if let Some(ini) = locations.get(&initial_location) {
            let dimension = ini.dimension();
            for loc in locations.values() {
                if loc.dimension() != dimension {
                    return Err(HAConstructionError::InconsistentDimension {
                        expected: dimension,
                        was: loc.dimension(),
                        in_loc: loc.id,
                    });
                }
            }
        } else {
            return Err(HAConstructionError::UnknownLocation(initial_location));
        }
        let next_loc_id = locations.keys().max().unwrap().0 + 1;
        Ok(HA {
            locations,
            adjacency,
            initial_location,
            next_loc_id,
        })
    }

    pub(crate) fn with_single_mode(name: &str, dimension: usize) -> Self {
        let initial = Location {
            name: name.to_string(),
            id: 0.into(),
            dynamics: Dynamics(Vec::new()),
            invariant: Rectangle::unbounded(dimension),
        };
        HA {
            locations: map! { initial.id => initial },
            adjacency: HashMap::new(),
            initial_location: 0.into(),
            next_loc_id: 1usize,
        }
    }

    pub(crate) fn dimension(&self) -> usize {
        self.initial_mode().dimension()
    }

    pub(crate) fn initial_mode(&self) -> &Location {
        self.locations.get(&self.initial_location).unwrap()
    }

    pub(crate) fn num_modes(&self) -> usize {
        self.locations.len()
    }

    pub(crate) fn num_jumps(&self) -> usize {
        self.adjacency.len()
    }

    pub(crate) fn add_location(
        &mut self,
        name: String,
        dynamics: Dynamics,
        invariant: Rectangle,
    ) -> LocationId {
        let new_location = Location {
            name,
            id: self.next_location_id(),
            dynamics,
            invariant,
        };
        let new_id = new_location.id;
        self.locations.insert(new_id, new_location);
        new_id
    }

    pub(crate) fn get_edge_guard(
        &self,
        from: LocationId,
        with: ActionId,
        to: LocationId,
    ) -> &Rectangle {
        &self.adjacency[&(from, with, to)]
    }

    pub fn iter_jumps(&self) -> impl Iterator<Item = EdgeRef<LocationId>> {
        self.adjacency.iter()
    }

    pub(crate) fn iter_modes(&self) -> impl Iterator<Item = &Location> {
        self.locations.iter().map(|(_, l)| l)
    }

    pub(crate) fn connect(
        &mut self,
        source: LocationId,
        target: LocationId,
        action: ActionId,
        guard: Rectangle,
    ) -> Result<(), HAConstructionError> {
        if !self.locations.contains_key(&source) {
            return Err(HAConstructionError::UnknownSourceLocation(action, source));
        }
        if !self.locations.contains_key(&target) {
            return Err(HAConstructionError::UnknownTargetLocation(action, target));
        }
        self.adjacency.insert((source, action, target), guard);
        Ok(())
    }

    pub(crate) fn is_sink(&self, loc: LocationId) -> bool {
        self.successors(loc).is_empty()
    }

    pub(crate) fn successors(&self, loc: LocationId) -> Vec<LocationId> {
        let mut res: Vec<LocationId> = self
            .adjacency
            .iter()
            .filter_map(|(k, _)| if k.0 == loc { Some(k.2) } else { None })
            .collect();
        res.sort();
        res.dedup();
        res
    }

    pub(crate) fn predecessor(&self, loc: LocationId) -> Vec<LocationId> {
        let mut res: Vec<LocationId> = self
            .adjacency
            .iter()
            .flat_map(|(k, _)| if k.2 == loc { Some(k.0) } else { None })
            .collect();
        res.sort();
        res.dedup();
        res
    }

    pub(crate) fn successors_actions(&self, loc: LocationId) -> Vec<(ActionId, LocationId)> {
        let mut res: Vec<(ActionId, LocationId)> = self
            .adjacency
            .iter()
            .filter_map(|(k, _)| if k.0 == loc { Some((k.1, k.2)) } else { None })
            .collect();
        res.sort();
        res.dedup();
        res
    }

    pub(crate) fn outbound(&self, source: LocationId) -> impl Iterator<Item = EdgeRef<LocationId>> {
        self.adjacency.iter().filter(move |(k, _)| k.0 == source)
    }

    pub(crate) fn inbound(&self, target: LocationId) -> impl Iterator<Item = EdgeRef<LocationId>> {
        self.adjacency.iter().filter(move |(k, _)| k.2 == target)
    }

    #[allow(dead_code)]
    pub(crate) fn disconnect<'a>(
        &'a mut self,
        source: LocationId,
        action: ActionId,
        target: LocationId,
    ) {
        let _ = self.adjacency.remove(&(source, action, target));
    }

    pub(crate) fn neutral_dynamics(&self) -> Dynamics {
        vec![Scalar::from(0); self.dimension()].into()
    }

    fn next_location_id(&mut self) -> LocationId {
        let res = self.next_loc_id;
        self.next_loc_id += 1;
        LocationId(res)
    }

    fn remove_location(&mut self, loc: LocationId) {
        self.locations.remove(&loc);
        self.adjacency
            .retain(|(src, _, tar), _| *src != loc && *tar != loc);
    }

    pub(crate) fn get_location(&self, loc: LocationId) -> &Location {
        self.locations
            .get(&loc)
            .expect("access of unknown location")
    }

    pub(crate) fn get_location_mut(&mut self, loc: LocationId) -> &mut Location {
        self.locations
            .get_mut(&loc)
            .expect("access of unknown location")
    }

    pub fn include_synchronous_obs_trace(&mut self, start: LocationId, trace: ObservableTrace) {
        assert!(trace
            .steps
            .iter()
            .all(|bs| bs.state.iter().all(|s| s.is_singular())));

        let ObservableTrace {
            initial,
            end_state,
            steps,
            last_delay,
        } = trace;
        let steps = steps
            .into_iter()
            .map(|bs| {
                let BlindStep {
                    delay,
                    action,
                    state,
                } = bs;
                let state = state.into_iter().map(|s| s.lower()).collect();
                Step {
                    delay,
                    action,
                    state,
                }
            })
            .collect();
        let initial = initial.into_iter().map(|s| s.lower()).collect();
        let end_state = end_state.into_iter().map(|s| s.lower()).collect();
        let tt = TimedTrace {
            initial,
            end_state,
            steps,
            last_delay,
        };
        self.include_timed_trace(start, tt);
    }

    /// Ensures that the HA along the trace can justify the observed trace
    ///
    pub fn include_timed_trace(&mut self, start: LocationId, trace: TimedTrace) {
        //let loc = self.get_location(start);
        let mut cur_loc = start;
        // get state trace
        let loc_trace: Vec<(ActionId, LocationId)> = trace
            .steps()
            .map(|step| {
                dbg!(&step, self.successors_actions(cur_loc));
                let acid: ActionId = step.action;
                let (a,target) = self.successors_actions(cur_loc)
                    .iter()
                    .find(|(aid, _lid)| *aid == acid)
                    .expect("expect valid trace - used action not valid")
                    .clone();
                cur_loc = target;
                (a,target)
            })
            .collect();
        // actions are valid along this trace
        assert_eq!(loc_trace.len(), trace.len());

        let init_vars = &trace.initial;
        // transform init vars from Atom to Scalar => 5 -> [5,5]
        let mut cur_vars = init_vars
            .into_iter()
            .cloned()
            .map(Scalar::singular)
            .collect::<Vec<_>>();
        let mut cur_loc = start;

        // Follow the trace and observe the variables along it
        trace
            .steps()
            .enumerate()
            .zip(loc_trace.iter())
            .for_each(|((_ix, step), (aid, loc_id))| {
                let cur_location = self.get_location(cur_loc).clone();
                //Var change according to delay in cur_loc
                let mut new_vars = transform(&cur_vars, &step.delay, &cur_location.dynamics);
                // Check if possible values are within the cur_loc invariant
                let inv_sat = cur_location.invariant.is_partly_contained(&new_vars);
                if !inv_sat {
                    // the current invariant is not satisfiable
                    let (new_bound, new_v) =
                    cur_location.invariant.restrict_values_update_bounds(&new_vars);
                    new_vars = new_v;
                    let cur_loc = self.get_location_mut(cur_loc);
                    cur_loc.set_invariant(Rectangle(new_bound));
                } else if inv_sat && !cur_location.invariant.is_satisfied(&new_vars) {
                    //inv is partly sat but not completely
                    new_vars = new_vars
                        .into_iter()
                        .zip(cur_location.invariant.0.iter())
                        .map(|(v, g)| g.intersection(&v))
                        .collect();
                }

                //test if observed values are within predicted intervals
                if let Some(obs) = Some(&step.state) {
                    //TODO remove Some
                    let valid_obs = Rectangle(new_vars.clone()).is_satisfied_atom(&obs);
                    if !valid_obs {
                        let violated_dims:Vec<usize> = (0..self.dimension()).filter(|&ix|{
                            let s: Scalar = new_vars[ix];
                            !s.contains_atom(&obs[ix])
                        }).collect();

                        for violated_ix in violated_dims {
                            let violated_dyn = cur_location.dynamics[violated_ix];
                            let old_var = cur_vars[violated_ix];
                            let new_measurment = obs[violated_ix];
                            let time_delay = step.delay;

                            
                            let gradient = (new_measurment - old_var.middle())/time_delay; //TODO FIX middle is placeholder

                            let _new_dyn = violated_dyn.expand_to(&gradient);
                            todo!();

                        }



                        todo!("error detected - observed values not inside predicted bounds");
                    }
                }

                //TODO
                // restrict predict intervals to exact observed values if known
                //if let Some(obs) = step.
                new_vars = step.state.iter().cloned().map(Scalar::singular).collect();

                //get guard of next transition
                let e_guard = self.get_edge_guard(cur_loc, *aid, *loc_id).clone();
                let guard_sat = e_guard.is_partly_contained(&new_vars);
                // if guard not sat
                if !guard_sat {
                    let (new_bound, new_v) =
                    e_guard.restrict_values_update_bounds(&new_vars);
                    new_vars = new_v;

                    let new_r = Rectangle(new_bound);

                    assert!(new_r.hull(&e_guard) == new_r);

                    let feedback = self.adjacency.insert((cur_loc, *aid, *loc_id), new_r);
                    assert!(feedback.is_some(), "map value should already exist")
                } else if guard_sat && e_guard.is_satisfied(&new_vars) {
                    //guard is partly sat but not completely - restrict values inside
                    new_vars = new_vars
                        .into_iter()
                        .zip(e_guard.0.iter())
                        .map(|(v, g)| g.intersection(&v))
                        .collect();
                }
                cur_loc = *loc_id;
                cur_vars = e_guard.constrain(&new_vars);
            });

        let last_location = &self.get_location(cur_loc).clone();
        let last_vars = transform(&cur_vars, &trace.last_delay, &last_location.dynamics);

        let violated_dimension = last_vars
            .iter()
            .enumerate()
            .zip(trace.end_state.iter())
            .filter(|((_ix, s), a)| !s.contains(&Scalar::singular(**a)))
            .collect::<Vec<_>>();
        if !violated_dimension.is_empty() {
            // current end value not explainable, need fix

            // get violated dimension
            violated_dimension
                .iter()
                .for_each(|((ix, _), observed_var)| {
                    let lower_gradient: Atom =
                        (**observed_var - cur_vars[*ix].lower()) / trace.last_delay;
                    let upper_gradient: Atom =
                        (**observed_var - cur_vars[*ix].upper()) / trace.last_delay;

                    let original_dyn = &last_location.dynamics[*ix];

                    let upper_violated = original_dyn.upper() < upper_gradient;
                    let lower_violated = original_dyn.lower() > lower_gradient;
                    assert!(upper_violated ^ lower_violated);
                    let new_bound = if upper_violated {
                        original_dyn.expand_to(&upper_gradient)
                    } else {
                        original_dyn.expand_to(&lower_gradient)
                    };

                    let last_loc_mut = self.get_location_mut(cur_loc);
                    last_loc_mut.dynamics[*ix] = new_bound;
                });
        }
    }

    pub(crate) fn update_given_events(&mut self, updates: Vec<EventUpdate>) -> Result<(), ()> { //TODO better return type
        updates.into_iter().for_each(|up|{
            match up.clone() {
                EventUpdate::Dynamic(loc, b) => {
                    let affected_loc = self.get_location_mut(loc);
                    //dbg!(&affected_loc.dynamics.0, &b.0);
                    //assert!(affected_loc.dynamics.0.iter().zip(b.0.iter()).all(|(d,n)| n.contains(d)));
                    affected_loc.dynamics = b;
                },
                EventUpdate::Guard { from, to, with, guard } =>{
                    let gref =self.adjacency.get_mut(&(from, with, to));
                    if let Some(g_ref) = gref {
                        *g_ref = guard;
                    } else {
                        panic!("Edge not found for guard update: {:?}", up);
                    }

                }
                EventUpdate::Invariant(loc , b) => {
                    let affected_loc = self.get_location_mut(loc);
                    //assert!(affected_loc.invariant.0.iter().zip(b.0.iter()).all(|(d,n)| n.contains(d)));
                    affected_loc.invariant = b;
                },
            }


        });

        Ok(())
    }
}

impl HA {
    #[must_use = "Function does not mutate `self`, resulting edges need to be consumed manually."]
    fn bend_edges(
        &self,
        remove: &Vec<LocationId>,
        retain: LocationId,
    ) -> Vec<((LocationId, ActionId, LocationId), Rectangle)> {
        self.adjacency
            .clone()
            .into_iter()
            .map(|((source, action, target), guard)| {
                if remove.contains(&source) {
                    if remove.contains(&target) {
                        ((retain, action, retain), guard)
                    } else {
                        ((retain, action, target), guard)
                    }
                } else if remove.contains(&target) {
                    ((source, action, retain), guard)
                } else {
                    ((source, action, target), guard)
                }
            })
            .collect()
    }

    #[must_use = "Function does not mutate `self`, resulting matrix needs to be consumed manually."]
    fn merge_transitions(
        mut edges: Vec<((LocationId, ActionId, LocationId), Rectangle)>,
    ) -> AdjacencyMatrix<LocationId> {
        edges.sort_by_key(|((s, a, t), _)| (*s, *a, *t));
        if edges.is_empty() {
            return HashMap::new();
        }
        let first = edges.remove(0);
        edges
            .into_iter()
            .fold(vec![first], |mut retain, next| {
                let ((source1, action1, target1), _) = *retain.last().unwrap();
                let ((source2, action2, target2), ref guard2) = next;
                let new = if source1 == source2 && action1 == action2 && target1 == target2 {
                    let ((source1, action1, target1), guard1) = retain.pop().unwrap();
                    ((source1, action1, target1), guard1.hull(guard2))
                } else {
                    next
                };
                retain.push(new);
                retain
            })
            .into_iter()
            .collect()
    }

    pub(crate) fn merge_modes(&mut self, part: Vec<LocationId>, repr: LocationId) {
        // 1) bend each edge targeting an element of part to target repr.
        // 2) bend each edge sourcing from element of part to source in repr.
        // 3) merge guards edges with same label, target, and source, keep only one
        // 4/5) replace inv/dyn of repr with merged inv/dyn of all of part + repr
        // 6) if initial location is not repr of a partition, change initial location
        // 7) remove modes

        let new_edges = self.bend_edges(&part, repr); // 1) and 2)
        self.adjacency = Self::merge_transitions(new_edges); // 3)
        part.iter()
            .for_each(|remove| self.merge_inv_dyn(repr, *remove)); // 4/5)
        if part.contains(&self.initial_location) {
            self.initial_location = repr; // 6)
        }
        part.into_iter()
            .for_each(|remove| self.remove_location(remove)); // 7)
    }

    fn merge_inv_dyn(&mut self, retain_id: LocationId, remove_id: LocationId) {
        let remove = self.get_location(remove_id);
        let retain = self.get_location(retain_id);
        let merged_dyn = Location::merge_dynamics(retain, remove);
        let merged_inv = Location::merge_invariants(retain, remove);
        let retain = self.get_location_mut(retain_id);
        retain.set_dynamics(merged_dyn);
        retain.set_invariant(merged_inv);
    }
}

#[cfg(test)]
mod tests {
    use crate::hybridautomaton::basics::Rectangle;
    use crate::hybridautomaton::basics::{Atom, Scalar, Variable};
    use crate::hybridautomaton::timed_trace::ActionId;
    use crate::hybridautomaton::{Dynamics, Location, LocationId, HA};
    use std::collections::HashMap;

    #[test]
    fn test_merge_dynamics() {
        // x1 ∈ [1.0, 1.0]; x2 ∈ [3.0, 3.0]
        let dyn1 = Dynamics(vec![
            Scalar::singular(Atom::from(1.0)),
            Scalar::singular(Atom::from(3.0)),
        ]);
        let loc1 = Location {
            name: "Loc1".to_string(),
            id: LocationId(0),
            dynamics: dyn1,
            invariant: Rectangle(vec![]),
        };

        // x1 ∈ [2.0, 2.0]; x2 ∈ [2.0, 2.0]
        let dyn2 = Dynamics(vec![Scalar::singular(Atom::from(2.0)); 2]);
        let loc2 = Location {
            name: "Loc2".to_string(),
            id: LocationId(1),
            dynamics: dyn2,
            invariant: Rectangle(vec![]),
        };

        // x1 ∈ [1.0, 2.0]; x2 ∈ [2.0, 3.0]
        let merged = Location::merge_dynamics(&loc1, &loc2);
        assert_eq!(2, merged.dimension());

        // Check x1
        let x1_dyn = &merged[0];
        assert_eq!(1.0, x1_dyn.lower().into_inner());
        assert_eq!(2.0, x1_dyn.upper().into_inner());

        // Check x2
        let x2_dyn = &merged[1];
        assert_eq!(2.0, x2_dyn.lower().into_inner());
        assert_eq!(3.0, x2_dyn.upper().into_inner());
    }

    #[test]
    fn test_create_consistent_automaton() {
        let dyn1 = Dynamics(vec![Scalar::zero(), Scalar::one()]);
        let loc1 = Location {
            name: "Loc1".to_string(),
            id: LocationId(0),
            dynamics: dyn1,
            invariant: Rectangle(vec![]),
        };

        let dyn2 = Dynamics(vec![Scalar::one(), Scalar::zero()]);
        let loc2 = Location {
            name: "Loc2".to_string(),
            id: LocationId(1),
            dynamics: dyn2,
            invariant: Rectangle(vec![]),
        };

        let mut locs = HashMap::new();
        locs.insert(loc1.id, loc1);
        locs.insert(loc2.id, loc2);

        let mut adjacency = HashMap::new();
        adjacency.insert(
            (LocationId(0), ActionId(0), LocationId(1)),
            Rectangle(vec![]),
        );
        adjacency.insert(
            (LocationId(1), ActionId(1), LocationId(0)),
            Rectangle(vec![]),
        );

        HA::new(locs, adjacency, LocationId(0)).unwrap();
    }

    /*    #[test]
    fn test_create_inconsistent_automaton() {
        let a = Variable(0);
        let mut dyn1: HashMap<Variable, Polynomial> = HashMap::new();
        dyn1.insert(a, ConstantPolynomial::zero().into());
        let mut dyn2: HashMap<Variable, Polynomial> = HashMap::new();
        dyn2.insert(a, ConstantPolynomial::from(Scalar::one()).into());
        let loc0 = Location::new("Loc0".to_string(), LocationId(0), dyn1, vec![]);
        let loc1 = Location::new("Loc2".to_string(), LocationId(2), dyn2, vec![]);
        let jump0 = Jump { source: loc0.id, target: loc1.id, action: ActionId(0), guard: Conjunction(vec![]) };
        let jump1 = Jump { source: loc1.id, target: loc0.id, action: ActionId(0), guard: Conjunction(vec![]) };
        let ha = HA::new(vec![loc0, loc1], vec![jump0, jump1], LocationId(0));
        assert!(ha.is_err());
    } */

    #[test]
    fn test_add_locations_and_connect() {
        let dyn1 = Dynamics(vec![Scalar::zero()]);
        let loc1 = Location {
            name: "Loc0".to_string(),
            id: LocationId(0),
            dynamics: dyn1,
            invariant: Rectangle(vec![]),
        };

        let mut locs = HashMap::new();
        locs.insert(loc1.id, loc1);

        let mut ha =
            HA::new(locs, HashMap::new(), LocationId(0)).expect("Construction should succeed.");
        assert_eq!(1, ha.dimension());
        assert_eq!(1, ha.num_modes());
        assert_eq!(0, ha.num_jumps());

        let dyn2 = Dynamics(vec![Scalar::one()]);
        ha.add_location("Loc1".to_string(), dyn2, Rectangle(vec![]));

        let res = ha.connect(LocationId(0), LocationId(1), ActionId(0), Rectangle(vec![]));
        assert!(res.is_ok());
        assert_eq!(1, ha.dimension());
        assert_eq!(2, ha.num_modes());
        assert_eq!(1, ha.num_jumps());
    }

    #[test]
    fn test_connect_unknown_location() {
        let loc1 = Location {
            name: "Loc0".to_string(),
            id: LocationId(0),
            dynamics: Dynamics(vec![]),
            invariant: Rectangle(vec![]),
        };
        let mut locs = HashMap::new();
        locs.insert(loc1.id, loc1);

        let mut ha =
            HA::new(locs, HashMap::new(), LocationId(0)).expect("Construction should succeed.");
        let res = ha.connect(LocationId(0), LocationId(1), ActionId(0), Rectangle(vec![]));
        assert!(res.is_err());
    }

    #[test]
    fn test_get_and_remove_location() {
        let loc0 = Location {
            name: "Loc0".to_string(),
            id: LocationId(0),
            dynamics: Dynamics(vec![]),
            invariant: Rectangle(vec![]),
        };
        let loc1 = Location {
            name: "Loc1".to_string(),
            id: LocationId(1),
            dynamics: Dynamics(vec![]),
            invariant: Rectangle(vec![]),
        };
        let mut locs = HashMap::new();
        locs.insert(loc0.id, loc0);
        locs.insert(loc1.id, loc1);

        let mut ha =
            HA::new(locs, HashMap::new(), LocationId(0)).expect("Construction should succeed.");
        ha.get_location(LocationId(0)); // Should not panic.
        ha.remove_location(LocationId(0)); // Should not panic.
    }

    /*    #[test]
    fn test_get_jump() {
        let present_jump =
            Jump { source: LocationId(0), target: LocationId(1), action: ActionId(0), guard: Conjunction(vec![]) };
        let mut dynamics: HashMap<Variable, Polynomial> = HashMap::new();
        dynamics.insert(Variable(0), Polynomial::zero());
        let loc0 = Location::new("Loc0".to_string(), LocationId(0), dynamics.clone(), vec![]);
        let loc1 = Location::new("Loc1".to_string(), LocationId(1), dynamics, vec![]);
        let ha_res = HA::new(vec![loc0, loc1], vec![present_jump], LocationId(0));
        assert!(ha_res.is_ok());
        let ha = ha_res.unwrap();
        assert_eq!(1, ha.variables.len());
        assert_eq!(2, ha.locations.len());
        assert_eq!(1, ha.jumps.len());
        let mut jump = ha.get_jump(LocationId(0), ActionId(0));
        assert!(jump.is_some());
        jump = ha.get_jump(LocationId(1), ActionId(0));
        assert!(jump.is_none());
        jump = ha.get_jump(LocationId(0), ActionId(1));
        assert!(jump.is_none());
    } */

    #[test]
    fn test_relax_dynamics() {
        let dynamics = Dynamics(vec![Scalar::singular(Atom::from(3.0))]);
        let mut loc = Location {
            name: "Loc".to_string(),
            id: LocationId(0),
            dynamics,
            invariant: Rectangle(vec![]),
        };

        let lambda1 = Atom::from(2.0);
        loc.relax_dynamics_for(Variable(0), lambda1);
        assert_eq!(1, loc.dimension());

        let lambda2 = Atom::from(4.0);
        loc.relax_dynamics_for(Variable(0), lambda2);
        assert_eq!(1, loc.dimension());

        let relaxed_dyn = loc.get_dynamics(Variable(0));
        assert_eq!(2.0, relaxed_dyn.lower().into_inner());
        assert_eq!(4.0, relaxed_dyn.upper().into_inner());
    }

    #[allow(unused_must_use)]
    #[test]
    fn test_get_successor_locations() {
        let loc0 = Location {
            name: "Loc0".to_string(),
            id: LocationId(0),
            dynamics: Dynamics(vec![]),
            invariant: Rectangle(vec![]),
        };
        let loc1 = Location {
            name: "Loc1".to_string(),
            id: LocationId(1),
            dynamics: Dynamics(vec![]),
            invariant: Rectangle(vec![]),
        };
        let loc2 = Location {
            name: "Loc2".to_string(),
            id: LocationId(2),
            dynamics: Dynamics(vec![]),
            invariant: Rectangle(vec![]),
        };
        let loc3 = Location {
            name: "Loc3".to_string(),
            id: LocationId(3),
            dynamics: Dynamics(vec![]),
            invariant: Rectangle(vec![]),
        };

        let mut locs = HashMap::new();
        locs.insert(loc0.id, loc0);
        locs.insert(loc1.id, loc1);
        locs.insert(loc2.id, loc2);
        locs.insert(loc3.id, loc3);

        let mut ha =
            HA::new(locs, HashMap::new(), LocationId(0)).expect("Construction should succeed.");

        ha.connect(LocationId(0), LocationId(1), ActionId(0), Rectangle(vec![]));
        ha.connect(LocationId(0), LocationId(1), ActionId(1), Rectangle(vec![]));
        ha.connect(LocationId(0), LocationId(2), ActionId(2), Rectangle(vec![]));
        ha.connect(LocationId(1), LocationId(3), ActionId(0), Rectangle(vec![]));

        let successors = ha.successors(LocationId(0));
        assert_eq!(2, successors.len());
        assert!(successors.contains(&LocationId(1)));
        assert!(successors.contains(&LocationId(2)));
    }

    /* #[test]
    fn test_minimize() {
        let dynamics = HashMap::new();
        let loc0 = Location::new("Loc0".to_string(), LocationId(0), dynamics.clone(), vec![]);
        let loc1 = Location::new("Loc1".to_string(), LocationId(1), dynamics.clone(), vec![]);
        let loc2 = Location::new("Loc2".to_string(), LocationId(2), dynamics.clone(), vec![]);
        let loc3 = Location::new("Loc3".to_string(), LocationId(3), dynamics, vec![]);
        let ha_res = HA::new(vec![loc0, loc1, loc2, loc3], vec![], None, None);
        assert!(ha_res.is_ok());
        let mut ha = ha_res.unwrap();
        ha.connect(LocationId(0), LocationId(1), ActionId(0), None);
        ha.connect(LocationId(0), LocationId(2), ActionId(1), None);
        ha.connect(LocationId(1), LocationId(3), ActionId(2), None);
        ha.connect(LocationId(2), LocationId(3), ActionId(2), None);
        assert_eq!(4, ha.get_all_present_locations().len());
        assert_eq!(4, ha.get_all_jumps().len());
        let minimizer = Minimizer::new(&mut ha);
        minimizer.start();
        assert_eq!(3, ha.get_all_present_locations().len());
        assert!(ha.get_location(LocationId(2)).is_none());
        assert_eq!(3, ha.get_all_jumps().len());
    } */
}
