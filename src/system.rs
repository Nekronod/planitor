pub(crate) mod ha_system;
pub(crate) mod selector;

trait MonitorSystem {
    fn set_plan(&mut self, plan: ());

    fn notify_monitor(&self, monitor: ()); //Monitor
}

struct PrototypeSystem {
    monitor: (), //Monitor
}

impl PrototypeSystem {
    fn run(&mut self) -> Result<(), String> {
        loop {
            //stuff
            break;
        }
        todo!()
    }

    fn execute_instruction(&mut self) -> Result<(), String> {
        todo!()
    }

    fn report_to_monitor(&self) -> Result<(), String> {
        todo!()
    }
}
