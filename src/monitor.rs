use std::fmt::Display;
use std::{collections::HashMap, fmt::Debug, time::Duration};

use super::OrdF;

use crate::hybridautomaton::basics::{Atom, Scalar};
use crate::hybridautomaton::timed_trace::{ActionId, TraceBuilder};
use crate::hybridautomaton::{basics::VariableAssignment, HA};
use crate::hybridautomaton::{AnnotatedHA, LocationId};
use crate::system::ha_system::HaPlan;
use crate::world_model::abstract_planning::domain_handler::DomainHandler;
use crate::world_model::concrete_model::Coordinate;
use crate::world_model::ha_sys_planer::Maneuver;
use crate::world_model::Planner;
use rtlola_frontend::{self, mir::StreamReference};
use rtlola_interpreter::*;
pub(crate) struct Monitor<D>
where
    D: DomainHandler,
{
    rtl: rtlola_interpreter::Monitor,
    aha: AnnotatedHA,
    idx_map: HashMap<usize, usize>, //Map Stream idx to HA dimension
    last_trace_start: LocationId,
    cur_ha_loc: LocationId,
    trace_builder: TraceBuilder,
    pub(crate) planner: Planner<D>,
    backup_plans: HashMap<LocationId, HaPlan>,
    pub(crate) specification_stream_names: HashMap<String, usize>,
    collision_idx: Option<usize>,
    pub(crate) last_known_pos: Option<Coordinate>,
}

#[derive(Debug, Clone)]
pub(crate) enum MonitoringError {
    BackUpPlan(HaPlan),
    FallBackManeuvers(Maneuver),
    UnexpectedValueType,
    InvalidHaAction,
    AmbiguousAction,
    InternalError(String),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum SystemReading {
    Bool(bool),
    Float(OrdF),
    Int(i64),
    UInt(u64),
    None,
}

pub(crate) type SystemValues = Vec<SystemReading>;

pub(crate) trait Plan {
    fn instruction(&self) -> Vec<PlanStep>;
    fn is_fall_back(&self) -> bool;
}
pub(crate) struct PlanStep {
    pub(crate) id: usize,
    pub(crate) condition: Vec<(usize, Scalar)>,
    pub(crate) name: String,
    pub(crate) time: Option<Duration>,
}

impl<D: DomainHandler> Monitor<D> {
    pub fn new(
        spec: String,
        ha: AnnotatedHA,
        ha_state: VariableAssignment,
        backup_plans: HashMap<LocationId, HaPlan>,
        planner: Planner<D>,
    ) -> Self {
        //crate a new RTLolaIR and transform it to an Monitor
        let tf = rtlola_interpreter::TimeFormat::FloatSecs;
        let tr = rtlola_interpreter::TimeRepresentation::Absolute(tf);
        let ir = rtlola_frontend::parse(rtlola_frontend::ParserConfig::for_string(spec))
            .expect("expect valid input spec");

        // crate a name to index map for the IR Streams
        let ir_name_map: HashMap<String, usize> = ir
            .all_streams()
            .map(|sr| match sr {
                StreamReference::In(i_ix) => (ir.inputs[i_ix].name.clone(), i_ix),
                StreamReference::Out(o_ix) => (ir.outputs[o_ix].name.clone(), o_ix),
            })
            .collect();

        let collision_idx = ir_name_map.get("collision").map(|u| *u);
        //Create a Hashmap mapping IR idx to HA idx
        let ix_map = ha
            .dim_names
            .iter()
            .map(|(n, ha_ix)| ir_name_map.get(n).map(|ir_ix| (*ir_ix, *ha_ix)))
            .filter(|o| o.is_some())
            .map(|o| o.unwrap())
            .collect();
        //dbg!(ix_map);
        //dbg!(ha.dim_names);
        //dbg!(ir_name_map);
        let interpreter_config =
            rtlola_interpreter::Config::new_api(rtlola_interpreter::EvalConfig::api(tr), ir);
        let rtl = interpreter_config.as_api();

        //let ha_state_s: Vec<Scalar> = ha_state.iter().map(|a| Scalar::singular(*a)).collect();
        let trace_builder = TraceBuilder::new(ha_state, ha.initial_location());

        Monitor {
            rtl,
            idx_map: ix_map,
            last_trace_start: ha.initial_location(),
            cur_ha_loc: ha.initial_location(),
            aha: ha,
            trace_builder,
            planner,
            backup_plans,
            specification_stream_names: ir_name_map,
            collision_idx,
            last_known_pos: None,
        }
    }

    /// Accepts a new input batch given by the system
    /// - Forwards input data to the rtlola-monitor
    /// - Compare Monitor Verdict with HA predictions
    /// - Feedback system with Ok() or Err with emergency plans
    pub fn accept_input(
        &mut self,
        time: OrdF,
        values: SystemValues,
    ) -> Result<(), MonitoringError> {
        //dbg!(&values, &self.collision_idx);
        let time = Duration::from_secs_f64(time.0);
        let event: Event = values.clone().into_iter().map(|v| v.into()).collect();
        let verdict = self.rtl.accept_event(event, time);
        //dbg!(&verdict);
        let Verdicts { timed, event } = verdict;
        for (d, v) in timed.into_iter() {
            self.update_verdict(d, v)?;
        }
        self.update_verdict(time, event)?;
        Ok(())
    }

    fn update_verdict(
        &mut self,
        time: Duration,
        verdict: Vec<(usize, Value)>,
    ) -> Result<(), MonitoringError> {
        //dbg!(&verdict);
        let time_f = time.as_secs_f64();
        //dbg!(time_f);
        //let mut error_found = false;
        //time -> update intervals,
        //dbg!(&self.observed_var);
        // let mut time_advanced_state: Vec<Scalar> = self
        //     .observed_var
        //     .iter()
        //     .zip(self.aha.get_location(self.cur_ha_loc).dynamics.0.iter())
        //     .map(|(s, d)| *s + *d * Scalar::singular(Atom::from(time_f)))
        //     .map(|tav| {
        //         let u = tav.upper();
        //         let l = if tav.is_singular() { u } else { tav.lower() };
        //         let u_rounded = Atom::from(round_f(u.into_inner()));
        //         let l_rounded = Atom::from(round_f(l.into_inner()));
        //         Scalar::range(l_rounded, u_rounded)
        //     })
        //     .collect();

        //dbg!(&self.aha.get_location(self.cur_ha_loc).dynamics.0);
        //dbg!(&time_advanced_state);

        let action_idx = self.specification_stream_names["actionID"];

        let mut collision_detected_flag = false;
        if let Some(collision_idx) = self.collision_idx {
            if let Some((_, Value::Bool(b))) = verdict.iter().find(|(ix, _)| *ix == collision_idx) {
                collision_detected_flag = *b;
            }
        }

        //update position if x and y are reported at the same event
        let x_ir_idx = self.specification_stream_names["x"];
        let y_ir_idx = self.specification_stream_names["y"];
        let pos_x_val = verdict
            .iter()
            .find(|(idx, _)| *idx == x_ir_idx)
            .map(|v| &v.1);
        let pos_y_val = verdict
            .iter()
            .find(|(idx, _)| *idx == y_ir_idx)
            .map(|v| &v.1);
        if let (Some(Value::Float(x)), Some(Value::Float(y))) = (pos_x_val, pos_y_val) {
            self.last_known_pos = Some(Coordinate::from((x.into_inner(), y.into_inner())));
        }

        if collision_detected_flag {
            //TODO: update world map
            //dbg!(&self.aha.dim_names["angle"]);
            //dbg!(&self.idx_map);
            let angle_dim: usize = self.idx_map[&self.aha.dim_names["angle"]];
            let angle: Scalar = self.trace_builder.lookup_value(angle_dim);
            let vehicle_size: OrdF = 1.0f64.into();
            // current position
            /*
            let x_ir_idx = self.specification_stream_names["x"];
            let y_ir_idx = self.specification_stream_names["y"];
            let pos_x_val = verdict
                .iter()
                .find(|(idx, _)| *idx == x_ir_idx)
                .map(|v| &v.1);
            let pos_y_val = verdict
                .iter()
                .find(|(idx, _)| *idx == y_ir_idx)
                .map(|v| &v.1);
            if let (Some(Value::Float(x)), Some(Value::Float(y))) = (pos_x_val, pos_y_val) {
                self.last_known_pos = Some(Coordinate::from((x.into_inner(), y.into_inner())));
            }
            */
            /*
            let pos = if let Some(c) = self.last_known_pos {
                self.planner.get_wm().position_from_cords(c)
            } else {
                todo!()
            };
            */
            let angle =
                crate::world_model::concrete_model::Angle::from(angle.middle().into_inner());

            //TODO store target pos: to remove and re-edit for refinement
            let p2 = self.planner.pather.pather.update_wm_cords(
                &self.last_known_pos.unwrap(),
                angle,
                vehicle_size,
            );
            //dbg!(&p2);
            self.planner.get_wm_mut().update_wm_pos(&p2);

            // we may need to update loc_id first too
            if let Some((_, Value::Unsigned(ac_id))) =
                verdict.iter().find(|(ix, _)| *ix == action_idx as usize)
            {
                //dbg!("update loc after collision");
                let action_id = ActionId(*ac_id as usize);
                let successors: Vec<LocationId> = self
                    .aha
                    .ha
                    .successors_actions(self.cur_ha_loc)
                    .into_iter()
                    .filter(|(a, _)| *a == action_id)
                    .map(|(_, l)| l)
                    .collect();
                //dbg!(ac_id, self.cur_ha_loc, &successors);
                self.cur_ha_loc = successors[0];
            }

            {
                use colored::*;
                eprintln!(
                    "{}",
                    "Collision detected! Updating and replanning."
                        .to_string()
                        .red()
                );
            }

            // reset loc: trace not useable after worldmodel error
            self.trace_builder.reset();
            self.trace_builder.cur_loc = self.cur_ha_loc;
            let velo_dim = self.idx_map[&self.aha.dim_names["vel"]];
            self.trace_builder.trace.initial_values[velo_dim] = Scalar::zero();
            self.trace_builder.trace.end_state[velo_dim] = self.trace_builder.trace.initial_values[velo_dim];
            //dbg!("current ha loc known before back up plan:", self.cur_ha_loc);
            let stable_state_plan = self.backup_plans[&self.cur_ha_loc].clone();
            //dbg!(&stable_state_plan);
            return Err(MonitoringError::BackUpPlan(stable_state_plan));
        }


        /*
        // value -> check and collapse interval
        for (idx, v) in &verdict {
            //dbg!(v, idx);
            if !self.idx_map.contains_key(idx) && *idx != action_idx {
                continue;
            }
            //dbg!(v,idx);
            let f_val = if let Value::Float(f) = v {
                f
            } else if let Value::Bool(_b) = v {
                //todo trigger results and bool streams
                continue;
            } else if let Value::Unsigned(_u) = v {
                assert!(*idx == 6);
                continue;
            } else {
                return Err(MonitoringError::UnexpectedValueType);
            };
            let monitor_atom = Atom::from(f_val.into_inner());

            //check if observed value is explainable
            let interval: &Scalar = &time_advanced_state[self.idx_map[idx]];
            if !interval.contains_atom(&monitor_atom) {
                dbg!("observation not explainable");
                //dbg!(&time_advanced_state);
                //dbg!(interval);
                //dbg!(monitor_atom);
                //dbg!(&self.cur_trace);
                error_found = true;
            } else {
                time_advanced_state[self.idx_map[idx]] = Scalar::singular(monitor_atom);
            }
        }
        //todo invariant violation by non overlapping intervals
        let invariant = &self.aha.get_location(self.cur_ha_loc).invariant;
        let (bound, _) = invariant.restrict_values_update_bounds(&time_advanced_state);
        if invariant.0 != bound {
            // violated invariant detected
            error_found = true;
            dbg!("invariant violation");
        }

        // action? -> intersect scalars, check not empty, change mode

        if let Some((_, Value::Unsigned(ac_id))) =
            verdict.iter().find(|(ix, _)| *ix == action_idx as usize)
        {
            //dbg!("action detected", ac_id);
            let action_id = ActionId(*ac_id as usize);
            let successors: Vec<LocationId> = self
                .aha
                .ha
                .successors_actions(self.cur_ha_loc)
                .into_iter()
                .filter(|(a, _)| *a == action_id)
                .map(|(_, l)| l)
                .collect();
            if successors.is_empty() {
                return Err(MonitoringError::InvalidHaAction);
            } else if successors.len() > 1 {
                return Err(MonitoringError::AmbiguousAction);
            } // else len ==1;
            let new_loc = successors[0];
            let guard = self
                .aha
                .ha
                .get_edge_guard(self.cur_ha_loc, action_id, new_loc);
            let (bound, constrained_vars) = guard.restrict_values_update_bounds(&time_advanced_state);
            if guard.0 != bound {
                // violated guard detected
                dbg!("guard violation found");
                error_found = true;
            }
            self.observed_var = constrained_vars;
            self.cur_ha_loc = new_loc;
            //build step
            let bs = BlindStep {
                delay: Atom::from(time_f),
                action: action_id,
                state: self.observed_var.clone(),
            };
            self.cur_trace.steps.push(bs);
            self.cur_trace.end_state = self.observed_var.clone();
        } else {
            self.cur_trace.end_state = time_advanced_state.clone();
            self.observed_var = time_advanced_state;
        }

        if error_found == true {
            let stable_state_plan = self.backup_plans[&self.cur_ha_loc].clone();
            return Err(MonitoringError::BackUpPlan(stable_state_plan));
        }
         */
        let tb_ac = if let Some((_, Value::Unsigned(ac_id))) =
            verdict.iter().find(|(ix, _)| *ix == action_idx as usize)
        {
            Some(ActionId(*ac_id as usize))
        } else {
            None
        };

        let mut input_values = vec![None; self.aha.ha.dimension()];
        for (idx, v) in &verdict {
            //dbg!(v, idx);
            if !self.idx_map.contains_key(idx) && *idx != action_idx {
                continue;
            }
            //dbg!(v,idx);
            let f_val = if let Value::Float(f) = v {
                f
            } else if let Value::Bool(_b) = v {
                //todo trigger results and bool streams
                continue;
            } else if let Value::Unsigned(_u) = v {
                assert!(*idx == 6);
                continue;
            } else {
                return Err(MonitoringError::UnexpectedValueType);
            };
            let monitor_atom = Atom::from(f_val.into_inner());
            let ha_idx = self.idx_map[idx];
            input_values[ha_idx] = Some(monitor_atom);
        }

        let event = crate::hybridautomaton::timed_trace::Event::new(Atom::from(time_f), input_values, tb_ac);
        if self.trace_builder.update_event(&self.aha.ha, event) {
            let stable_state_plan = self.backup_plans[&self.trace_builder.get_cur_loc_id()].clone();
            Err(MonitoringError::BackUpPlan(stable_state_plan))
        } else {
            self.cur_ha_loc = self.trace_builder.get_cur_loc_id();
            Ok(())
        }

    }

    /// To be called after an error occurred and the system reached a stable state.
    /// Computes a new Plan from the current position.
    pub fn update(&mut self) -> Result<HaPlan, MonitoringError> {
        // update HA
        
        if self.trace_builder.has_updates() {
            let ha_updates =  self.trace_builder.extract_updates();
            self.aha
            .ha
            .update_given_events(ha_updates).map_err(|()| MonitoringError::InternalError("HA update Error".to_string()))?;
        }   
       
        self.trace_builder.reset();
        self.next_plan()
    }

    pub fn next_plan(&mut self) -> Result<HaPlan, MonitoringError> {
        //generate new plan
        //dbg!(self.last_known_pos);
        self.planner
            .update_position(self.last_known_pos.unwrap())
            .map_err(|s| MonitoringError::InternalError(s.to_string()))?;
        let new_plan: HaPlan = self.plan();
        if new_plan.is_empty() && !self.planner.is_abstract_empty() {
            todo!("refinement")
        } else {
            Ok(new_plan)
        }
    }

    pub fn update_maneuvers(&mut self) -> Result<Vec<Maneuver>, MonitoringError> {
        todo!()
    }

    pub(crate) fn plan(&mut self) -> HaPlan {
        let angle = self.trace_builder.lookup_value(self.aha.dim_names["angle"]);
        self.planner
            .plan(&self.aha, angle.middle().into_inner().into())
    }

    pub(crate) fn get_ha(&self) -> &HA {
        &self.aha.ha
    }
}

impl Into<Value> for SystemReading {
    fn into(self) -> Value {
        match self {
            SystemReading::Bool(b) => Value::Bool(b),
            SystemReading::Float(f) => {
                Value::Float(ordered_float::NotNan::new(f.0 as f64).unwrap())
            }
            SystemReading::Int(i) => Value::Signed(i),
            SystemReading::None => Value::None,
            SystemReading::UInt(u) => Value::Unsigned(u),
        }
    }
}

impl Display for MonitoringError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let m_str = if let MonitoringError::FallBackManeuvers(m) = self {
            format!("Fall back maneuver:{:?}", m)
        } else {
            "".to_string()
        };
        let str = match self {
            MonitoringError::FallBackManeuvers(_) => &m_str,
            MonitoringError::BackUpPlan(_) => "BackupPlan",
            MonitoringError::UnexpectedValueType => {
                "Obtained a non-matching type from the monitor verdict"
            }
            MonitoringError::InvalidHaAction => "No action found for reported action ID",
            MonitoringError::AmbiguousAction => {
                "Multiple followup states for given Action possible"
            }
            MonitoringError::InternalError(s) => s,
        };
        write!(f, "{}", str)
    }
}
