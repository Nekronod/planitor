use super::*;

pub(crate) struct WMParser {
    start: Position,
    goal: Position,
    dim: (usize, usize),
}

#[derive(Debug, Clone, Copy)]
enum Token {
    Wall,
    Free,
    Number(usize),
}

#[derive(Debug, Clone)]
pub(crate) enum ParseError {
    TokenError(String),
    MissingNumber,
    MismatchFieldNumbers(usize, usize),
}

impl WMParser {
    pub fn new() -> Self {
        Self {
            start: Position::default(),
            goal: Position::default(),
            dim: (0, 0),
        }
    }

    /// Parses the grid and builds a WorldModel
    ///
    /// panics if input is invalid or contains no metadata
    pub fn into_wm(mut self, map: String) -> Result<WorldModel, ParseError> {
        let g = self.parse(map);
        let (w, h) = self.dim;

        g.map(|grid| WorldModel {
            grid,
            pos: self.start.clone(),
            start: self.start,
            goal: Some(self.goal),
            size_x: (w as f64).into(),
            size_y: (h as f64).into(),
            wm_updates: vec![],
        })
    }

    pub fn parse(&mut self, map: String) -> Result<Grid, ParseError> {
        let tokens = self.tokenize(map)?;
        let (rec_grid, t) = self.parse_rec(&tokens)?;
        self.dim = (rec_grid.width, rec_grid.height);
        //eprintln!("{}", rec_grid.to_string());
        //dbg!(t);
        if t.len() == 4 {
            let (t1, t2, t3, t4) = (
                t.get(0).unwrap(),
                t.get(1).unwrap(),
                t.get(2).unwrap(),
                t.get(3).unwrap(),
            );
            let (x, y, xg, yg) = match (t1, t2, t3, t4) {
                (Token::Number(w), Token::Number(h), Token::Number(xg), Token::Number(yg)) => {
                    (*w, *h, *xg, *yg)
                }
                _ => return Err(ParseError::MissingNumber),
            };
            self.start = Position::new(&[x as isize], &[y as isize]);
            self.goal = Position::new(&[xg as isize], &[yg as isize]);
            return Ok(rec_grid);
        } else if t.len() == 2 {
            let (t1, t2) = (t.get(0).unwrap(), t.get(1).unwrap());
            let (x, y) = match (t1, t2) {
                (Token::Number(w), Token::Number(h)) => (*w, *h),
                _ => return Err(ParseError::MissingNumber),
            };
            self.start = Position::new(&[x as isize], &[y as isize]);
            return Ok(rec_grid);
        }
        if !t.is_empty() {
            Err(ParseError::MismatchFieldNumbers(2, t.len()))
        } else {
            Ok(rec_grid)
        }
    }

    fn tokenize(&self, input: String) -> Result<Vec<Token>, ParseError> {
        let mut res = Vec::new();
        for split_str in input.split_whitespace() {
            if split_str.starts_with('X') || split_str.starts_with('_') {
                //split and crate separate field tokens
                for c in split_str.chars() {
                    match c {
                        'X' => {
                            res.push(Token::Wall);
                        }
                        '_' => {
                            res.push(Token::Free);
                        }
                        n if n.is_numeric() => {
                            return Err(ParseError::TokenError(
                                "Separate Tiles nad Numbers with spaces".to_string(),
                            ));
                        }
                        c => {
                            return Err(ParseError::TokenError(format!("unknown character {}", c)));
                        }
                    };
                }
            } else if *split_str
                .chars()
                .next()
                .map(|c| c.is_numeric())
                .get_or_insert(false)
            {
                let n = split_str
                    .parse::<usize>()
                    .map_err(|e| ParseError::TokenError(e.to_string() + ": unknown character"))?;
                res.push(Token::Number(n));
            } else {
                return Err(ParseError::TokenError(format!("unknown character")));
            }
        }

        Ok(res)
    }

    fn parse_rec<'a>(&mut self, tokens: &'a [Token]) -> Result<(Grid, &'a [Token]), ParseError> {
        let (t1, t2) = (tokens.get(0).unwrap(), tokens.get(1).unwrap());
        let (w, h) = match (t1, t2) {
            (Token::Number(w), Token::Number(h)) => (*w, *h),
            _ => return Err(ParseError::MissingNumber),
        };
        let mut tokens = &tokens[2..];
        let mut fields = Vec::new();
        loop {
            let f = match tokens[0] {
                Token::Free => {
                    tokens = &tokens[1..];
                    Field::Tile(TileType::Walkable)
                }
                Token::Wall => {
                    tokens = &tokens[1..];
                    Field::Tile(TileType::Wall)
                }
                Token::Number(_) => {
                    let (g, t) = self.parse_rec(tokens)?;
                    tokens = t;
                    Field::Grid(g)
                }
            };

            fields.push(f);
            if fields.len() == w * h {
                break;
            }
        }

        let res = fields
            .chunks(w)
            .map(|chunk| chunk.to_vec())
            .collect::<Vec<Vec<Field>>>();

        Ok((
            Grid {
                grid: res,
                width: w,
                height: h,
            },
            tokens,
        ))
    }

    fn parse_ascii_grid_simple(&self, map: &str) -> Grid {
        let map = map.to_string();
        let lines = map.lines();
        let mut meta_data: Vec<usize> = Vec::new();
        let mut g: Vec<Vec<TileType>> = Vec::new();
        lines.enumerate().for_each(|(ix, line)| {
            if ix == 0 {
                meta_data = line
                    .split(' ')
                    .map(|s| s.parse::<usize>().ok().unwrap())
                    .collect::<Vec<_>>();
            } else {
                g.push(
                    line.chars()
                        .map(|s| match s {
                            'X' => TileType::Wall,
                            '_' => TileType::Walkable,
                            _ => panic!("invalid map char"),
                        })
                        .collect(),
                )
            }
        });
        assert_eq!(meta_data.len(), 2);
        let width = meta_data[0];
        let height = meta_data[1];
        assert_eq!(width * height, g.iter().map(|inner| inner.len()).sum());

        let grid = g
            .into_iter()
            .map(|v| v.into_iter().map(|t| Field::Tile(t)).collect())
            .collect();

        Grid {
            grid,
            width,
            height,
        }
    }
}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ParseError::TokenError(t) => write!(f, "Token Error: {}", t),
            ParseError::MissingNumber => write!(f, "Expected a Number"),
            ParseError::MismatchFieldNumbers(exp, got) => {
                write!(f, "Incorrect number of fields exp: {}, got: {}", exp, got)
            }
        }
    }
}

/// 6 3
/// 1XX_XX
/// XXX_XX
/// XXX_XX
///
/// 1:
/// _X
/// _X
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[ignore]
    fn simple_map_parse() {
        let map = "6 3\n".to_string() + "XXX_XX\n" + "XXX_XX\n" + "XXX_XX";

        let wmp = WMParser::new();
        let grid = wmp.parse_ascii_grid_simple(&map);
        //colored strings broke tests, compare yourself
        println!("{}", grid.to_colored_string());
        println!("{}", "XXX_XX\nXXX_XX\nXXX_XX");
    }

    #[test]
    #[ignore]
    fn map_parse() {
        let map = "6 3\n".to_string() + "XXX_XX\n" + "XXX_XX\n" + "XXX_XX";

        let mut wmp = WMParser::new();
        let grid = wmp.parse(map).ok().expect("should be valid");
        assert_eq!(grid.to_string(), "XXX_XX\nXXX_XX\nXXX_XX");
    }

    #[test]
    #[ignore]
    fn map_parse_rec() {
        let map = "6 3\n".to_string() + "XXX_XX\n" + "XXX_XX\n" + "XXX_X 2 2 _X_X";

        let mut wmp = WMParser::new();
        let grid = wmp.parse(map).ok().expect("should be valid");
        assert_eq!(grid.to_string(), "XXX_XX\nXXX_XX\nXXX_X1\n1:\n_X\n_X\n");
    }

    #[test]
    fn map_parse_with_meta_pos() {
        let map = "6 3\n".to_string() + "XXX_XX\n" + "XXX_XX\n" + "XXX_XX 3 0 3 2";

        let mut wmp = WMParser::new();
        let res = wmp.parse(map);
        let grid = res.ok().expect("should be valid");
        //assert_eq!(grid.to_string(), "XXX_XX\nXXX_XX\nXXX_XX");

        let start = wmp.start.clone();
        let g = wmp.goal;
        dbg!(&g, &start);

        let wm = WorldModel {
            grid,
            pos: start.clone(),
            start: start.clone(),
            goal: None,
            size_x: (wmp.dim.0 as f64).into(),
            size_y: (wmp.dim.0 as f64).into(),
            wm_updates: vec![],
        };
        assert!(wm.try_get_field(&start).is_some()); // 3 0
        assert!(wm.try_get_field(&g).is_some()); // 3 2

        dbg!(&wmp.dim);
        let exp: OrdF = 2.0.into();
        assert_eq!(wm.distance(&start, &g), exp);
    }

    #[test]
    fn position_test() {
        let map = "5 5\n".to_string() + "XXXXX\n" + "_____\n" + "XX_XX\n" + "_____\n" + "XXXXX\n";

        let mut wmp = WMParser::new();
        let res = wmp.parse(map);
        let grid = res.ok().expect("should be valid");
        //assert_eq!(grid.to_string(), "XXXXX\n_____\nXX_XX\n_____\nXXXXX");

        let p = Position::default();
        let wm = WorldModel {
            grid,
            pos: p.clone(),
            goal: None,
            start: p.clone(),
            size_x: 1.0.into(),
            size_y: 1.0.into(),
            wm_updates: vec![],
        };
        let mid = Position::new(&[3], &[3]);
        assert!(wm.try_get_field(&mid).is_some());

        let exp: OrdF = 0.0.into();
        assert_eq!(wm.distance(&mid, &mid), exp);
    }

    #[test]
    fn angle_test() {
        let map = "5 5\n".to_string() + "XXXXX\n" + "_____\n" + "XX_XX\n" + "_____\n" + "XXXXX\n";
        let wmp = WMParser::new();
        let wm = wmp.into_wm(map).expect("valid input");

        let p1 = Position::new(&[3], &[1]);
        let p2 = Position::new(&[3], &[2]);

        let (d, a) = wm.distance_angle(&p1, &p2);
        let exp_d: OrdF = 1.0.into();
        let exp_a: concrete_model::Angle = 90f64.into();

        assert!((d - exp_d).abs() < 0.00000005f64);
        assert_eq!(a, exp_a);

        let (_, a) = wm.distance_angle(&p2, &p1);
        let exp_a: concrete_model::Angle = 270f64.into();
        assert_eq!(a, exp_a);
    }

    #[test]
    fn neighbor_test() {
        let map = "2 2\n".to_string() + "2 2 ____" + "_\n" + "2 2 _ 2 2 ____ __\n" + "_\n";
        let wmp = WMParser::new();
        let wm = wmp.into_wm(map).expect("valid input");

        let p1 = Position::new(&[0, 1], &[0, 1]);

        let n = wm.neighbors(&p1);
        assert_eq!(n.len(), 5);

        let p2 = Position::new(&[1], &[1]);
        let n = wm.neighbors(&p2);
        assert_eq!(n.len(), 4);

        let p3 = Position::new(&[0, 1, 0], &[1, 0, 1]);
        let n = wm.neighbors(&p3);
        assert_eq!(n.len(), 4);
    }
}

macro_rules! distance_test {
    ($suite:ident, $map:expr, $($name:ident: $input1:expr, $input2:expr, $input3:expr,$input4:expr,$output:expr,)*) => {
        mod $suite {
            #[allow(unused_imports)]
            use super::*;
            $(
                #[test]
                fn $name() {
                    let wmp = WMParser::new();
                    let wm = wmp.into_wm($map).expect("valid");
                    let p1 = Position::new(&$input1, &$input2);
                    let p2 = Position::new(&$input3, &$input4);
                    dbg!(&p1,&p2);
                    let exp: crate::OrdF = $output.into();
                    let res = wm.distance(&p1, &p2);
                    dbg!(res,exp);
                    assert!((res - exp).abs() < 0.00000005f64);
                }
            )*
        }
    };
}

distance_test!(five_by_five,
    "5 5\n".to_string() + "XXXXX\n" + "X___X\n"+ "X___X\n"+ "X___X\n" +"XXXXX\n",
    zero: [1],[1], [1],[1], 0.0,
    one: [1],[1], [1],[2], 1.0,
    diag: [1],[1], [2],[2], 2.0f64.sqrt(),
    two_one: [1],[1], [3],[2], 5.0f64.sqrt(),
);

distance_test!(level_two,
    "2 2\n".to_string() + "2 2 ____\n" + "2 2 ____\n"+ "2 2 ____\n"+ "2 2 ____\n",
    top_zero: [1],[1], [1],[1], 0.0,
    top_one: [0],[0], [0],[1], 1.0,
    across: [0,0], [0,0],[1,1],[1,1], f64::sqrt((1.5*1.5)*2.0),
    small: [0,1], [0,1],[0,1],[0,0], 0.5,
    small_across: [0,0], [0,0],[0,1],[0,1], f64::sqrt(2.0)/2.0,
    top_low: [0], [0],[0,1],[0,1], 0.125f64.sqrt(),
    small_straight_wide: [0,1], [0,0],[0,1],[1,1], 1.5,
    small_wide: [0,1], [0,0],[1,1],[1,0], 2f64.sqrt(),
);

macro_rules! coords_test {
    ($suite:ident, $map:expr, $($name:ident: $input1:expr, $input2:expr,)*) => {
        mod $suite {
            //use super::*;
            $(
                #[test]
                fn $name() {
                    let wmp = crate::world_model::wm_parser::WMParser::new();
                    let wm = wmp.into_wm($map).expect("valid");
                    let p1 = crate::world_model::Position::new(&$input1, &$input2);
                    let c = wm.cords(&p1);
                    let re_p1 = wm.position_from_cords(c);
                    assert_eq!(p1,re_p1);
                }
            )*
        }
    };
}

coords_test!(five_square,
    "5 5\n".to_string() + "XXXXX\n" + "X___X\n"+ "X___X\n"+ "X___X\n" +"XXXXX\n",
    zero: [0],[0],
    one: [1],[1],
    diff: [1],[2],
    two_one: [1],[3],
);

coords_test!(cords_level_two,
    "2 2\n".to_string() + "2 2 ____\n" + "2 2 ____\n"+ "2 2 ____\n"+ "2 2 ____\n",
    rec_zero: [0,0], [0,0],
    rec_one: [0,1],[0,0],
    rec_two: [1,1],[1,0],
);
