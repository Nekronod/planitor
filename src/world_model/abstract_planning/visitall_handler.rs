use std::collections::HashMap;

use crate::world_model::Position;

use super::domain_handler::DomainHandler;

#[derive(Debug, Clone)]
pub struct VisitAllHandler {
    pub(crate) init_file_string: String,
    pub(crate) pre_string: String,
    pub(crate) objects: Vec<String>,
    pub(crate) init_facts: Vec<String>,
    pub(crate) init_loc: Position,
    pub(crate) distances: HashMap<(Position, Position), i64>,
    pub(crate) goals: Vec<String>,
    is_perfect_abstraction: bool,
    cur_tmp_pos: Option<Position>,
    tmp_distances: HashMap<(Position, Position), i64>,
}

impl VisitAllHandler {
    /// Expects the input string
    pub fn new(str: String) -> Self {
        let init_file_string = str.clone();
        if let Some((pre, rest1)) = str.split_once("(:init") {
            let pre_string = pre.to_string();
            //split prefix, get objects
            let (objects,pre) = if let Some((pre, objects)) = pre.split_once("(:objects") {
                let objects: Vec<String> =objects.trim().strip_suffix(")").unwrap().trim().lines().into_iter().map(|s| s.trim().to_string()).
                filter(|s|s.starts_with("loc")).collect();
                (objects,pre.to_string())

            } else {
                panic!()
            };
            let remaining_string = rest1.to_owned();
            //dbg!(&remaining_string);
            if let Some((facts, goals)) = remaining_string.split_once("(:goal\n(and") {
                let facts: Vec<String> = facts
                    .trim()
                    .strip_suffix(")")
                    .unwrap()
                    .trim()
                    .lines()
                    .into_iter()
                    .map(|s| s.trim().to_string())
                    .collect();
                let (facts, distances): (Vec<String>, Vec<String>) =
                    facts.into_iter().partition(|l| !l.starts_with("(="));
                let (pos, facts): (Vec<String>, Vec<String>) =
                    facts.into_iter().partition(|s| s.contains("at-robot"));

                let distances = distances
                    .iter()
                    .map(|l| VisitAllHandler::parse_distance_fact(l))
                    .collect();
                let init_loc = Self::parse_loc_from_str(
                    pos[0]
                        .trim()
                        .trim_start_matches("(at-robot ")
                        .trim_end_matches(")"),
                );
                //dbg!(&goals);
                let goals: Vec<String> = goals
                    .trim()
                    .strip_suffix("\n)\n)\n(:metric minimize (total-cost))\n)")
                    .unwrap()
                    .trim()
                    .lines()
                    .into_iter()
                    .map(|s| s.trim().to_string())
                    .collect();

                return Self {
                    is_perfect_abstraction: init_file_string.contains("perfect"),
                    init_file_string,
                    pre_string: pre,
                    objects,
                    goals,
                    init_facts: facts,
                    distances,
                    init_loc,
                    cur_tmp_pos: None,
                    tmp_distances: HashMap::new(),
                };
            } else {
                panic!("could split at goals in problem.pddl: {}", remaining_string);
            }
        } else {
            panic!("unable to split at init: {}", str);
        }
    }

    /// "loc-x0-y2" -> Position::new_single(0,2)
    fn parse_loc_from_str(input: &str) -> Position {
        //dbg!(&input);
        let input = input.trim().trim_end_matches(")").trim_start_matches("(");
        let loc = input.trim().trim_start_matches("loc-");
        let pos = if let Some((x, y)) = loc.split_once("-") {
            let x = x
                .chars()
                .filter(|c| c.is_numeric())
                .fold(0, |acc, n| acc * 10 + n.to_digit(10).unwrap());
            let y = y
                .chars()
                .filter(|c| c.is_numeric())
                .fold(0, |acc, n| acc * 10 + n.to_digit(10).unwrap());
            Position::new_single(x as i16, y as i16)
        } else {
            panic!("invalid input str from plan {}", loc)
        };
        pos
    }

    fn str_for_loc(pos: &Position) -> String {
        let (x, y) = (pos.x[0], pos.y[0]);
        format!("loc-x{}-y{}", x, y)
    }

    pub fn set_tmp_pos(&mut self, pos: Position, distances: HashMap<(Position, Position), i64>) {
        //assert!(self.pre_string.contains(self.st(&pos)));
        assert!(!self.distances.keys().any(|(p1,p2)| p1 == &pos || p2 == &pos));
        self.tmp_distances = distances;
        self.cur_tmp_pos = Some(pos);
    }

    pub fn clear_tmp(&mut self) ->Result<(),String> {
        if self.cur_tmp_pos == None {
            return Err("TMP pos already none".to_string());
        }
        self.cur_tmp_pos = None;
        self.tmp_distances.clear();
        Ok(())
    }

    /// (= (distance loc-x1-y1 loc-x0-y1) 1)
    fn parse_distance_fact(distance_line: &str) -> ((Position, Position), i64) {
        //loc-x1-y1 loc-x0-y1) 1
        let trimmed_par = distance_line
            .trim()
            .trim_start_matches("(= (distance ")
            .trim_end_matches(")");
        let white_split = trimmed_par.split_once(" ");
        if let Some((loc1, loc2_and_distance)) = white_split {
            let pos1 = VisitAllHandler::parse_loc_from_str(loc1);
            if let Some((loc2, distance)) = loc2_and_distance.split_once(" ") {
                let pos2 = VisitAllHandler::parse_loc_from_str(loc2.trim().trim_end_matches(")"));
                let d = distance.parse().unwrap();
                return ((pos1, pos2), d);
            }
        }
        panic!("invalid input {}", distance_line);
    }

    fn reconstruct_distance(&self) -> String {
        let tmp_iter = if self.cur_tmp_pos.is_some() {
            self.tmp_distances.iter()
        } else {
            self.tmp_distances.iter()
        };
        self.distances
            .iter()
            .chain(tmp_iter)
            .map(|((p1, p2), d)| {
                assert!(p1.is_top_level());
                assert!(p2.is_top_level());
                let loc1 = format!("loc-x{}-y{}", p1.x[0], p1.y[0]);
                let loc2 = format!("loc-x{}-y{}", p2.x[0], p2.y[0]);
                format!("\t(= (distance {} {}) {})", loc1, loc2, d)
            })
            .collect::<Vec<_>>()
            .join("\n")
    }

    fn cur_pos_str(&self) -> String {
        assert!(self.init_loc.is_top_level());
        if let Some(tmp_p) = &self.cur_tmp_pos {
            let loc1 = format!("loc-x{}-y{}", tmp_p.x[0], tmp_p.y[0]);
            format!("\t(at-robot {})\n\t", loc1)
        } else {
            let p = &self.init_loc;
            let loc1 = format!("loc-x{}-y{}", p.x[0], p.y[0]);
            format!("\t(at-robot {})\n\t", loc1)
        }
    }

    fn object_string(&self) -> String {
        let tmp_str = if let Some(p) = &self.cur_tmp_pos { 
            VisitAllHandler::str_for_loc(p) + "\n"
        } else {
            "".to_string()
        };
        let res = "(:objects\n\t".to_string()
            + &self.objects.join("\n\t")
            +"\n\t"
            + &tmp_str
            + "- place\n)\n"
            + "(:init\n";
        res
    }

    fn fact_string(&self) -> String {
        let mut res = self.init_facts.join("\n\t");
        if let Some (_) = &self.cur_tmp_pos {
            self.tmp_distances.keys().for_each(|(p1,p2)|{
                let con_str1 = format!("(connected {} {})",VisitAllHandler::str_for_loc(p1),VisitAllHandler::str_for_loc(p2));
                if !res.contains(&con_str1) {
                    res += "\n\t";
                    res += &con_str1;
                }
                let con_str2 = format!("(connected {} {})",VisitAllHandler::str_for_loc(p2),VisitAllHandler::str_for_loc(p1));
                if !res.contains(&con_str2) {
                    res += "\n\t";
                    res += &con_str2;
                }
            })
        }
        res
    }
}

impl DomainHandler for VisitAllHandler {
    fn update_goal(&mut self, reached_goals: Vec<String>) -> Result<(), String> {
        self.init_facts.extend(reached_goals);
        Ok(())
    }

    fn set_position(&mut self, pos: Position) -> Result<(), String> {
        let pos_str = VisitAllHandler::str_for_loc(&pos);
        if self.pre_string.contains(&pos_str) {
            self.goals.push(format!("(visited {})", pos_str))
        }
        self.init_loc = pos;
        Ok(())
    }

    fn remove_connection_str(&mut self, connection: Vec<String>) -> Result<(), String> {
        for fact in connection {
            let ix = self.init_facts.iter().position(|e| e == &fact);
            if let Some(ix) = ix {
                self.init_facts.remove(ix);
            } else {
                eprintln!(
                    "Tried removing {} from pddl problem , but was not found. Check the mapping!",
                    fact
                );
            }
        }
        Ok(())
    }

    fn remove_connection(
        &mut self,
        x0: usize,
        y0: usize,
        x1: usize,
        y1: usize,
    ) -> Result<(), String> {
        let connection_str = format!("(connected loc-x{}-y{} loc-x{}-y{})", x0, y0, x1, y1);
        self.remove_connection_str(vec![connection_str])
    }

    fn default_file(&self) -> String {
        self.init_file_string.clone()
    }

    fn goals(&self) -> Vec<String> {
        self.goals.clone()
    }

    fn init_facts(&self) -> Vec<String> {
        self.init_facts.clone()
    }

    

    fn construct_call_file_string(&self) -> String {
        self.pre_string.clone()
            + &self.object_string()
            + &self.cur_pos_str()
            + &self.fact_string()
            + "\n"
            + &self.reconstruct_distance()
            + "\n)\n(:goal\n(and\n\t"
            + &self.goals.join("\n\t")
            + "\n)\n)\n(:metric minimize (total-cost))\n)"
    }

    fn extract_move(&self, plan_line: &str) -> super::PlanningInstruction {
        // move loc-x0-y0 loc-x1-y1
        assert!(plan_line.starts_with("move"));
        //  loc-x0-y0 loc-x1-y1
        let trimmed = plan_line.trim().trim_start_matches("move ").trim();
        // (loc-x0-y0,loc-x1-y1)
        if let Some((loc1, loc2)) = trimmed.split_once(" ") {
            // Position(0,0)
            let pos1 = Self::parse_loc_from_str(loc1);
            // Position(1,1)
            let pos2 = Self::parse_loc_from_str(loc2);
            return super::PlanningInstruction::Move(pos1, pos2);
        }
        panic!("Extracting Move failed for {}", plan_line);
    }

    fn performed_action(&mut self, _done_action: &super::PlanningInstruction) {
        // Visitall only has move actions - this function should not be called
        unreachable!()
    }

    fn update_distance(&mut self, from: &Position, to: &Position, cost: i64) {
        if let Some(r) =self.distances.get_mut(&(from.clone(), to.clone())) {
            *r = cost;
        } else {
            self.distances.insert((from.clone(), to.clone()),cost);
        }
    }

    fn distances(&self) -> &HashMap<(Position, Position),i64> {
        &self.distances
    }
}

#[cfg(test)]
mod tests {
    use std::fs;

    use super::*;

    #[test]
    fn test() {
        let test_pddl = fs::read_to_string("dependencies/downward/benchmarks/visitall/small.pddl")
            .expect("unable to read file");
        let dh = VisitAllHandler::new(test_pddl.clone());
        let out_string = dh.construct_call_file_string();
        dbg!(&out_string);
        fs::write("t.txt", out_string).unwrap();
        //assert_eq!(out_string, test_pddl.trim());
        //panic!()
    }

    #[test]
    fn test2() {
        let test_pddl = fs::read_to_string("dependencies/downward/benchmarks/visitall/small.pddl")
            .expect("unable to read file");
        let mut dh = VisitAllHandler::new(test_pddl.clone());
        let mut hm = HashMap::new();
        let p22 = Position::new_single(2 as isize, 2 as isize);
        let p11 =  Position::new_single(1 as isize, 1 as isize);
        hm.insert((p22.clone(),p11.clone()),2);
        dh.set_tmp_pos(p22, hm);
        let out_string = dh.construct_call_file_string();
        dbg!(&out_string);
        eprintln!("{}",&out_string);
        fs::write("t.txt", out_string).unwrap();
        //assert_eq!(out_string, test_pddl.trim());
        //panic!()
    }
}
