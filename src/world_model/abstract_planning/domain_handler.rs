use core::fmt::Debug;
use std::collections::HashMap;

use crate::world_model::Position;

use super::PlanningInstruction;

pub trait DomainHandler: Sized + Clone + Debug {
    fn update_goal(&mut self, reached_goals: Vec<String>) -> Result<(), String>;
    fn performed_action(&mut self, done_action: &PlanningInstruction);
    fn set_position(&mut self, pos: Position) -> Result<(), String>;
    fn remove_connection_str(&mut self, connection: Vec<String>) -> Result<(), String>;
    fn remove_connection(
        &mut self,
        x0: usize,
        y0: usize,
        x1: usize,
        y1: usize,
    ) -> Result<(), String>;
    fn update_distance(&mut self, from: &Position, to: &Position, cost: i64);
    fn distances(&self) -> &HashMap<(Position, Position),i64>;
    fn default_file(&self) -> String;
    fn goals(&self) -> Vec<String>;
    fn init_facts(&self) -> Vec<String>;
    fn construct_call_file_string(&self) -> String;
    fn extract_move(&self, plan_line: &str) -> PlanningInstruction;
    fn is_dummy(&self) -> bool {
        false
    }
}

#[derive(Debug, Copy, Clone)]
pub(crate) struct DummyHandler {}

impl DomainHandler for DummyHandler {
    fn update_goal(&mut self, _reached_goals: Vec<String>) -> Result<(), String> {
        Err("Dummy".to_string())
    }

    fn remove_connection_str(&mut self, _connection: Vec<String>) -> Result<(), String> {
        Err("Dummy".to_string())
    }

    fn remove_connection(
        &mut self,
        _x0: usize,
        _y0: usize,
        _x1: usize,
        _y1: usize,
    ) -> Result<(), String> {
        Err("Dummy".to_string())
    }

    fn default_file(&self) -> String {
        "Dummy".to_string()
    }

    fn goals(&self) -> Vec<String> {
        vec!["Dummy".to_string()]
    }

    fn init_facts(&self) -> Vec<String> {
        vec!["Dummy".to_string()]
    }

    fn construct_call_file_string(&self) -> String {
        "Dummy".to_string()
    }

    fn extract_move(&self, _plan_line: &str) -> PlanningInstruction {
        panic!("Dummy can't extract move");
    }

    fn set_position(&mut self, _pos: Position) -> Result<(), String> {
        //panic!("Dummy pos set");
        Ok(())
    }


    fn performed_action(&mut self, _done_action: &PlanningInstruction) {
        //nothing
    }

    fn is_dummy(&self) -> bool {
        true
    }

    fn update_distance(&mut self, _from: &Position, _to: &Position, _cost: i64) {
        panic!("Dummy distance")
    }

    fn distances(&self) -> &HashMap<(Position, Position),i64> {
        panic!("Dummy distance")
    }
}
