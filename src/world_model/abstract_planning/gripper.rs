use super::{domain_handler::DomainHandler};


#[derive(Debug, Clone)]
pub struct GripperHandler {
    init_file_string: String,
    pre_string: String,
    init_facts: Vec<String>,
    goals: Vec<String>,
}
