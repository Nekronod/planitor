use std::collections::HashMap;
use std::time::Duration;

use num::Zero;

use crate::hybridautomaton::basics::{Atom, Scalar};
use crate::hybridautomaton::{AnnotatedHA, LocationId};
use crate::OrdF;

use crate::system::ha_system::{HaPlan, HaPlanStep};
use crate::world_model::concrete_model::Pather;

use super::abstract_planning::Node;
use super::concrete_model::Angle;
use super::WorldModel;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub(crate) enum Maneuver {
    TurnLeft(OrdF),              // Degree
    TurnRight(OrdF),             // Degree
    Drive(OrdF),                 // Distance
    Stop(OrdF),                  // Distance
    DriveAndStop(OrdF),          // Distance
    Acc { v: OrdF, time: OrdF }, // target_velo, time
}

pub(crate) struct HaRoutePlaner {
    pub(crate) pather: Pather,
    dim_map: HashMap<String, usize>,
    cur_loc: LocationId,
    cur_angle: Angle,
    cur_v: OrdF,
}

impl HaRoutePlaner {
    pub fn new(
        cur_loc: LocationId,
        pather: Pather,
        dim_map: HashMap<String, usize>,
        cur_angle: Angle,
        cur_v: OrdF,
    ) -> Self {
        Self {
            cur_loc,
            pather,
            dim_map,
            cur_angle,
            cur_v,
        }
    }

    pub(crate) fn plan(&mut self, ha: &AnnotatedHA, goal: Option<Node>, angle: OrdF) -> HaPlan {
        //dbg!("extracting route");
        let route = self.extract_route(goal, angle);
        //dbg!(&route);
        let m = self.compute_maneuvers(route);
        //dbg!(&m);
        HaPlan::construct(m, ha, ha.ha.initial_location, angle)
    }

    pub(crate) fn extract_route(
        &mut self,
        _goal: Option<Node>,
        init_angle: OrdF,
    ) -> Vec<(OrdF, Angle)> {
        //dbg!("computing route on: ", self.get_wm().get_grid().to_string_annotated(Some(&self.pather.pos), init_angle));
        if !self.pather.updates.is_empty() {
            //dbg!("path updating");
            print!("{}", self.get_wm().get_grid().to_string());
            match self.pather.update_path() {
                Ok(()) => (),
                Err(_) => unreachable!(),
            };
        }

        let start = self.pather.pos.clone();
        //dbg!(&start);
        //dbg!(&start, &self.pather.goal);
        if start == self.pather.goal {
            return vec![];
        }
        let wm = &self.pather.wm;
        let mut pather_c = self.pather.clone();
        let mut route = vec![start];
        //dbg!(&pather_c.l_val);
        while let Some(p) = pather_c.next_move() {
            //dbg!(&pather_c.l_val[&p],&p, &self.pather.goal);
            if route.contains(&p) {
                panic!("route position duplicate");
            }
            route.push(p.clone());
            pather_c.perform_move(p);
        }
        //dbg!(&route);

        let dis_ang_absolute_v = route
            .iter()
            .zip(route.iter().skip(1))
            .map(|(cur, next)| {
                let (distance, angle) = wm.distance_angle(cur, next);
                (distance, angle)
            })
            .collect::<Vec<_>>();

        let mut prev_angle = init_angle.0.into();
        let mut dis_angle_relative_v = vec![];
        for cur in dis_ang_absolute_v.into_iter() {
            let diff_angle = cur.1 - prev_angle;
            dis_angle_relative_v.push((cur.0, diff_angle));
            prev_angle = cur.1;
        }
        dis_angle_relative_v
    }

    pub(crate) fn compute_maneuvers(&self, route: Vec<(OrdF, Angle)>) -> Vec<Maneuver> {
        //todo optimise
        let mut res = Vec::new();
        route.iter().for_each(|(d, a)| {
            let angle_dif = *a - self.cur_angle;
            if angle_dif.a >= 0.1f64.into() {
                res.append(&mut self.construct_turn(angle_dif));
            }
            let travel_distance = *d;
            res.append(&mut self.construct_drive(travel_distance));
        });
        //dbg!(&res);

        let mut simplified_res = vec![];
        {
            use Maneuver::*;
            let mut last = None;
            let mut step_change;
            for m in res.iter() {
                //dbg!(m);
                match last {
                    None => last = Some(*m),
                    Some(prev) => {
                        step_change = true;
                        let combined = match (prev,*m) {
                            (DriveAndStop(x), DriveAndStop(y)) => DriveAndStop(x+y),
                            (TurnLeft(x), TurnLeft(y)) => TurnLeft(x+y),
                            (TurnRight(x), TurnRight(y)) => TurnRight(x+y),
                            (TurnRight(x), TurnLeft(y)) if x > y=> TurnRight(x-y),
                            (TurnRight(x), TurnLeft(y)) => TurnLeft(y-x),
                            (TurnLeft(x), TurnRight(y)) if x > y=> TurnLeft(x-y),
                            (TurnLeft(x), TurnRight(y)) => TurnRight(y-x),
                            (Drive(x), DriveAndStop(y))/*if x+y < bound*/ => DriveAndStop(x+y),
                            (DriveAndStop(x), Stop(y)) => DriveAndStop(x+y),
                            (_prev,next) =>   {
                                step_change = false;
                                next
                            }
                        };
                        if step_change {
                            //combined maneuvers - set last to combined
                            //dbg!(&combined);
                            last = Some(combined);
                        } else {
                            //cant combine - store prev to result - set last to current(=combined)
                            //dbg!(&prev);
                            simplified_res.push(prev);
                            last = Some(combined);
                        }
                    }
                }
            }
            //dbg!(step_change, &res, &last);
            if !res.is_empty() {
                simplified_res.push(last.unwrap());
            }
        }
        simplified_res
    }

    fn construct_turn(&self, angle: Angle) -> Vec<Maneuver> {
        let a: OrdF = if <Angle as Into<OrdF>>::into(angle) > 180f64.into() {
            angle.inner() - OrdF::from(360.0)
        } else {
            angle.inner()
        };

        let t = if a > OrdF::zero() {
            Maneuver::TurnRight(a)
        } else {
            Maneuver::TurnLeft(a.abs().into())
        };
        vec![Maneuver::Stop(0f64.into()), t]
    }

    fn construct_drive(&self, distance: OrdF) -> Vec<Maneuver> {
        //TODO optimise
        vec![Maneuver::DriveAndStop(distance)]
    }

    pub(crate) fn get_wm(&self) -> &WorldModel {
        self.pather.get_wm()
    }

    pub(crate) fn get_wm_mut(&mut self) -> &mut WorldModel {
        self.pather.get_wm_mut()
    }

    /*
    fn construct_turn(&mut self, angle: Angle) -> Vec<PlanStep>{
        //go idle
        //todo

        //go to turn state
        let a: OrdF = if <Angle as Into<OrdF>>::into(angle) > 180f64.into() {
            angle.inner() - OrdF::from(360.0)
        } else {
            angle.inner()
        };
        let turn_dim = self.dim_map["angel_vel"];
        let l_max = self.ha.iter_modes().map(|l|(l,l.dynamics.0[turn_dim])).max_by(|s1,s2|s1.1.upper().cmp(&s2.1.upper()));
        let l_min = self.ha.iter_modes().map(|l|(l,l.dynamics.0[turn_dim])).min_by(|s1,s2|s1.1.lower().cmp(&s2.1.lower()));

        let (loc,interval,name) = if a < OrdF::from(0.0)  && l_min.map_or(false, |s|s.1.lower() < Atom::zero()) {
            (l_min.unwrap().0, l_min.unwrap().1, "turn left".to_string())
        } else {
            (l_max.unwrap().0, l_max.unwrap().1, "turn left".to_string())
        };

        //go in this state
        let (ac,l) = self.ha.successors_actions(self.cur_loc).iter().find(|(a,l)|*l == loc.id).unwrap().clone();

        let guard = self.ha.get_edge_guard(self.cur_loc, ac, l);
        let inv = &self.ha.get_location(self.cur_loc).invariant;
        let inv_sat_guard = guard.is_satisfied(&inv.0);
        if !inv_sat_guard {
            todo!()
        }
        let ac_step = PlanStep {
            id: ac.0,
            condition: vec![],
            name,
            time: None,
        };


        let (ac,l) = self.ha.successors_actions(l).iter().find(|(a,l_id)|*l_id == self.cur_loc).unwrap().clone();

        let cur_a_f: f64 = self.cur_angle.into();
        let turning_step = PlanStep {
            id: ac.0,
            condition: vec![(turn_dim, Scalar::singular(Atom::from(cur_a_f + a.into_inner())))],
            name:"stop_turn".to_string(),
            time: None,
        };

        self.cur_angle = self.cur_angle + a.into();

        vec![ac_step, turning_step]

    }

    fn construct_drive(&mut self, distance: OrdF) {
        let d = Atom::from(distance.into_inner());
        let acc_dim = self.dim_map["velocity"];
        let l_max = self.ha.iter_modes().map(|l|(l,l.dynamics.0[acc_dim])).max_by(|s1,s2|s1.1.upper().cmp(&s2.1.upper())).unwrap();

        let (ac,next_loc) = self.ha.successors_actions(self.cur_loc).iter().find(|(_,l)| *l == l_max.0.id).unwrap().clone();

        let ac_step = PlanStep {
            id: ac.0,
            condition: vec![],
            name: "acc".to_string(),
            time: None,
        };



        let l_break = self.ha.iter_modes().map(|l|(l,l.dynamics.0[acc_dim])).min_by(|s1,s2|s1.1.upper().cmp(&s2.1.upper())).unwrap();
        let (ac_break,loc_break) = self.ha.successors_actions(self.cur_loc).iter().find(|(_,l)| *l == l_break.0.id).unwrap().clone();


        //full acc distance
        let acc_state = LocationId(4); //TODO
        let acc_state = self.ha.get_location(acc_state);
        let max_speed = acc_state.invariant.0[self.dim_map["vel"]].upper();
        let acc_scalar = acc_state.dynamics.0[self.dim_map["vel"]];
        let break_state = LocationId(5); //TODO
        let break_state = self.ha.get_location(break_state);
        let break_scale = break_state.invariant.0[self.dim_map["vel"]];

        let t1 = max_speed / acc_scalar.upper();
        let t2 = (max_speed /break_scale.lower()) * Atom::from(-1.0);

        let s_prime_l =  Atom::from(0.5)* (acc_scalar.upper() * t1);
        let s_prime_r =  Atom::from(0.5)* (t2 * break_scale.lower());

        let s_prime = s_prime_l + s_prime_r;


        if d < s_prime {
            let p = acc_scalar.upper() /(acc_scalar.upper() + break_scale.lower());
            let s_l = d*p;
            let s_r = d*p;

            let t_l = Atom::from(((Atom::from(2.0) * s_l *acc_scalar.upper()).into_inner().sqrt()));
            let v_l = acc_scalar.upper()* t_l;
            let t_r = Atom::from(((Atom::from(2.0) * s_r *break_scale.lower()).into_inner().sqrt()));

            let acc_step = PlanStep {
                id: ac_break.0,
                condition: vec![(self.dim_map["vel"], Scalar::singular(v_l))],
                name: "break".to_string(),
                time: None,
            };
            let break_step = PlanStep {
                id: ac.0,
                condition: vec![],
                name: "time".to_string(),
                time: Some(Duration::from_secs_f64(t_l.into_inner())),
            };
        }

    }

    fn construct_idle(&self){

    }
    */
}

impl HaPlan {
    pub fn construct(
        maneuvers: Vec<Maneuver>,
        ha: &AnnotatedHA,
        start_loc: LocationId,
        init_angle: OrdF,
    ) -> Self {
        //dbg!(&maneuvers);
        PlanTranslator::new(ha, maneuvers, start_loc, init_angle).construct_ha_plan()
    }

    pub fn empty(is_fall_back: bool) -> Self {
        Self {
            steps: vec![],
            is_fall_back,
        }
    }

    pub fn is_empty(&self) -> bool {
        self.steps.is_empty()
    }
}

struct PlanTranslator<'a> {
    aha: &'a AnnotatedHA,
    start_loc: LocationId,
    maneuvers: Vec<Maneuver>,
    angle: OrdF,
}

impl<'a> PlanTranslator<'a> {
    pub fn new(
        ha: &'a AnnotatedHA,
        maneuvers: Vec<Maneuver>,
        loc: LocationId,
        init_angle: OrdF,
    ) -> Self {
        //dbg!(&init_angle);
        Self {
            aha: ha,
            start_loc: loc,
            maneuvers,
            angle: init_angle,
        }
    }

    fn transform_maneuver(
        &mut self,
        m: Maneuver,
        assumed_loc_id: LocationId,
    ) -> (Vec<HaPlanStep>, LocationId) {
        match m {
            Maneuver::TurnLeft(a) => {
                //turn left => negative angle
                assert!(self.aha.idle_states.contains(&assumed_loc_id));
                let target_loc = self.aha.state_map["turn_left"];
                //we know v=0 from planning
                //check if turn_left is direct neighbor
                let neighbors = self.aha.ha.successors_actions(assumed_loc_id);
                if neighbors.iter().any(|(_, l)| *l == target_loc) {
                    let next_action = neighbors.iter().find(|(_, l)| *l == target_loc).unwrap().0;
                    //idle to turning should sat guard and no other condition needed
                    let v_cond = (self.aha.dim_names["vel"], Scalar::singular(Atom::from(0.0)));
                    let step_1 = HaPlanStep {
                        act_id: next_action,
                        condition: vec![v_cond],
                        name: "turn_left".to_string(),
                        time: None,
                    };
                    let turn_state_neighbors = self.aha.ha.successors_actions(target_loc);
                    let next_action = turn_state_neighbors
                        .iter()
                        .find(|(_, l)| *l == assumed_loc_id)
                        .unwrap()
                        .0;
                    let turn_cond = (
                        self.aha.dim_names["angle"],
                        Scalar::range(
                            Atom::from(self.angle.0 - a.0),
                            Atom::from(self.angle.0 - a.0),
                        ),
                    );
                    //dbg!(a,self.angle);
                    self.angle -= a;
                    let step_2 = HaPlanStep {
                        act_id: next_action,
                        condition: vec![turn_cond],
                        name: "stop_turn_l".to_string(),
                        time: None,
                    };

                    return (vec![step_1, step_2], assumed_loc_id);
                } else {
                    todo!("reach non direct neighbor for turn")
                }
            }
            Maneuver::TurnRight(a) => {
                //turn left => negative angle
                assert!(self.aha.idle_states.contains(&assumed_loc_id));
                let target_loc = self.aha.state_map["turn_right"];
                //we know v=0 from planning
                //check if turn_right is direct neighbor
                let neighbors = self.aha.ha.successors_actions(assumed_loc_id);
                if neighbors.iter().any(|(_, l)| *l == target_loc) {
                    let next_action = neighbors.iter().find(|(_, l)| *l == target_loc).unwrap().0;
                    //idle to turning should sat guard and no other condition needed
                    let v_cond = (self.aha.dim_names["vel"], Scalar::singular(Atom::from(0.0)));
                    let step_1 = HaPlanStep {
                        act_id: next_action,
                        condition: vec![v_cond],
                        name: "turn_right".to_string(),
                        time: None,
                    };
                    let turn_state_neighbors = self.aha.ha.successors_actions(target_loc);
                    let next_action = turn_state_neighbors
                        .iter()
                        .find(|(_, l)| *l == assumed_loc_id)
                        .unwrap()
                        .0;
                    let turn_cond = (
                        self.aha.dim_names["angle"],
                        Scalar::range(
                            Atom::from(self.angle.0 + a.0),
                            Atom::from(self.angle.0 + a.0),
                        ),
                    );
                    self.angle += a;
                    let step_2 = HaPlanStep {
                        act_id: next_action,
                        condition: vec![turn_cond],
                        name: "stop_turn_r".to_string(),
                        time: None,
                    };
                    return (vec![step_1, step_2], assumed_loc_id);
                } else {
                    todo!("reach non direct neighbor for turn")
                }
            }
            Maneuver::Drive(_) => todo!(),
            Maneuver::Stop(d) => {
                let z: OrdF = 0.0f64.into();
                if d == z {
                    return (vec![], assumed_loc_id);
                } else {
                    todo!()
                }
            }
            Maneuver::DriveAndStop(d) => {
                assert!(self.aha.idle_states.contains(&assumed_loc_id));

                //state and action stuff
                let acc_id = self.aha.state_map["acc"];
                let break_id = self.aha.state_map["break"];
                let drive_id = self.aha.state_map["drive"];
                let cur_neighbors = self.aha.ha.successors_actions(assumed_loc_id);
                if !cur_neighbors.iter().any(|(_, l)| l == &acc_id) {
                    todo!("no direct access to acceleration");
                }

                let (acc_action, _) = cur_neighbors.iter().find(|(_, l)| l == &acc_id).unwrap();

                let acc_neighbors = self.aha.ha.successors_actions(acc_id);
                let (direct_break_action, _) =
                    acc_neighbors.iter().find(|(_, l)| l == &break_id).unwrap();
                let (drive_action, _) = acc_neighbors.iter().find(|(_, l)| l == &drive_id).unwrap();

                let drive_neighbors = self.aha.ha.successors_actions(acc_id);
                let (drive_to_break_action, _) = drive_neighbors
                    .iter()
                    .find(|(_, l)| l == &break_id)
                    .unwrap();

                let break_neighbors = self.aha.ha.successors_actions(break_id);
                let (halting_action, _) = break_neighbors
                    .iter()
                    .find(|(_, l)| l == &assumed_loc_id)
                    .unwrap();

                //compute distance and time stuff
                let d = Atom::from(d.0);
                let vel_dim = self.aha.dim_names["vel"];

                let acc_location = self.aha.ha.get_location(acc_id);
                let acc_interval = acc_location.dynamics[vel_dim];

                let break_location = self.aha.ha.get_location(break_id);
                let break_interval = break_location.dynamics[vel_dim];

                let max_speed = acc_location.invariant.0[vel_dim].upper();
                let t_acc_max = max_speed / acc_interval.upper();
                let t_break_max = (max_speed / break_interval.upper()) * Atom::from(-1.0);

                let s_prime_l = Atom::from(0.5) * (acc_interval.upper() * t_acc_max);
                let s_prime_r = Atom::from(0.5)
                    * (t_break_max * Atom::from(break_interval.lower().into_inner().abs()));

                let s_prime = s_prime_l + s_prime_r;
                //dbg!(max_speed, acc_interval, break_interval, s_prime_l, s_prime_r);
                //dbg!(&s_prime,d, d.cmp(&s_prime));
                //dbg!(&s_prime,d);
                let v0_cond = (self.aha.dim_names["vel"], Scalar::singular(Atom::from(0.0)));

                match d.cmp(&s_prime) {
                    std::cmp::Ordering::Less => {
                        /*
                        let a_frag = acc_interval.upper() + acc_interval.upper()*acc_interval.upper()/break_interval.lower();
                        dbg!(a_frag);
                        let right_side = d/(Atom::from(0.5)*a_frag);
                        let t_l = right_side.into_inner().sqrt();
                        dbg!(t_l);
                         */
                        let p = acc_interval.upper()
                            / (acc_interval.upper() + (break_interval.lower() * Atom::from(-1.0)));
                        let s_l = d * (Atom::from(1.0f64) - p);
                        let s_r = d * p;
                        let t_l = Atom::from(
                            (Atom::from(2.0) * s_l / acc_interval.upper())
                                .into_inner()
                                .sqrt(),
                        );
                        // max v reached after acc for t_l secs
                        let _v_l = acc_interval.upper() * t_l;
                        let denom = break_interval.lower() * Atom::from(-1.0);
                        //_t_r time needed to break to v=0; not needed due to cond(v=0) instruction
                        let _t_r = Atom::from((Atom::from(2.0) * s_r / denom).into_inner().sqrt());
                        //dbg!(t_l,t_r);
                        let start_accelerating = HaPlanStep {
                            act_id: *acc_action,
                            condition: vec![v0_cond],
                            name: "acc".to_string(),
                            time: None,
                        };
                        let acc_to_break_step = HaPlanStep {
                            act_id: *direct_break_action,
                            condition: vec![],
                            name: "break".to_string(),
                            time: Some(Duration::from_secs_f64(t_l.into_inner())),
                        };
                        let break_to_halt_step = HaPlanStep {
                            act_id: *halting_action,
                            condition: vec![v0_cond],
                            name: "halt".to_string(),
                            time: None,
                        };
                        let steps = vec![start_accelerating, acc_to_break_step, break_to_halt_step];
                        return (steps, assumed_loc_id);
                    }
                    std::cmp::Ordering::Equal => {
                        let start_accelerating = HaPlanStep {
                            act_id: *acc_action,
                            condition: vec![v0_cond],
                            name: "acc".to_string(),
                            time: None,
                        };
                        let acc_step = HaPlanStep {
                            act_id: *direct_break_action,
                            condition: vec![(
                                self.aha.dim_names["vel"],
                                Scalar::singular(max_speed),
                            )],
                            name: "break".to_string(),
                            time: None,
                        };
                        let break_step = HaPlanStep {
                            act_id: *halting_action,
                            condition: vec![v0_cond],
                            name: "halt".to_string(),
                            time: None,
                        };
                        let steps = vec![start_accelerating, acc_step, break_step];
                        return (steps, assumed_loc_id);
                    }
                    std::cmp::Ordering::Greater => {
                        let driving_distance = d - s_prime;
                        //current speed = max_speed
                        let driving_time = driving_distance / max_speed;

                        let start_accelerating = HaPlanStep {
                            act_id: *acc_action,
                            condition: vec![v0_cond],
                            name: "acc3".to_string(),
                            time: None,
                        };
                        let acc_to_drive_step = HaPlanStep {
                            act_id: *drive_action,
                            condition: vec![(
                                self.aha.dim_names["vel"],
                                Scalar::singular(max_speed),
                            )],
                            name: "drive3".to_string(),
                            time: None,
                        };
                        let drive_to_break_step = HaPlanStep {
                            act_id: *drive_to_break_action,
                            condition: vec![],
                            name: "break3".to_string(),
                            time: Some(Duration::from_secs_f64(driving_time.into_inner())),
                        };
                        let break_step = HaPlanStep {
                            act_id: *halting_action,
                            condition: vec![v0_cond],
                            name: "halt3".to_string(),
                            time: None,
                        };
                        let steps = vec![
                            start_accelerating,
                            acc_to_drive_step,
                            drive_to_break_step,
                            break_step,
                        ];
                        return (steps, assumed_loc_id);
                    }
                }
            }
            Maneuver::Acc { v: _v, time: _time } => todo!(),
        }
    }

    fn unfold_ha(&self) {
        todo!("maybe never")
    }

    pub fn construct_ha_plan(&mut self) -> HaPlan {
        let mut assumed_loc = self.start_loc;
        let mut steps = vec![];
        let maneuvers = self.maneuvers.clone();
        if maneuvers.is_empty() {
            return HaPlan::empty(false);
        }
        for m in maneuvers {
            let (mut step, pred_loc) = self.transform_maneuver(m, assumed_loc);
            assumed_loc = pred_loc;
            steps.append(&mut step);
        }

        HaPlan {
            steps,
            is_fall_back: false,
        }
    }
}

pub(crate) fn construct_stable_routes(aha: &AnnotatedHA) -> HashMap<LocationId, HaPlan> {
    let mut res = HashMap::new();
    let mut done = vec![];
    for loc in aha.ha.iter_modes() {
        let id = loc.id;
        if aha.idle_states.contains(&id) {
            res.insert(id, HaPlan::empty(true));
            done.push(id);
        }
        /*
        let suc = aha.ha.successors(id);
        suc.iter().for_each(|suc_id| {
            // if a suc is done, try to reach that and copy+prepend this action to get a new plan
        });
        */
    }

    let idle = aha.ha.initial_location;
    let acc_state = aha.state_map["acc"];
    let break_state = aha.state_map["break"];
    let drive_state = aha.state_map["drive"];

    let acc_plan = {
        let acc_break_ac = aha.get_action_for_pair(acc_state, break_state);
        let step = HaPlanStep {
            act_id: acc_break_ac,
            condition: vec![],
            name: "acc to emergency breaking".to_string(),
            time: Some(Duration::ZERO),
        };

        let break_to_idle = aha.get_action_for_pair(break_state, idle);
        let step2 = HaPlanStep {
            act_id: break_to_idle,
            condition: vec![(aha.dim_names["vel"], Scalar::singular(Atom::from(0.0)))],
            name: "emergency breaking".to_string(),
            time: None,
        };
        HaPlan {
            steps: vec![step, step2],
            is_fall_back: true,
        }
    };
    res.insert(acc_state, acc_plan);
    done.push(acc_state);

    let drive_plan = {
        let drive_break_ac = aha.get_action_for_pair(drive_state, break_state);
        let step = HaPlanStep {
            act_id: drive_break_ac,
            condition: vec![],
            name: "drive to emergency breaking".to_string(),
            time: Some(Duration::ZERO),
        };

        let break_to_idle = aha.get_action_for_pair(break_state, idle);
        let step2 = HaPlanStep {
            act_id: break_to_idle,
            condition: vec![(aha.dim_names["vel"], Scalar::singular(Atom::from(0.0)))],
            name: "emergency breaking".to_string(),
            time: None,
        };
        HaPlan {
            steps: vec![step, step2],
            is_fall_back: true,
        }
    };
    res.insert(drive_state, drive_plan);
    done.push(drive_state);

    let break_plan = {
        let break_to_idle = aha.get_action_for_pair(break_state, idle);
        let step2 = HaPlanStep {
            act_id: break_to_idle,
            condition: vec![(aha.dim_names["vel"], Scalar::singular(Atom::from(0.0)))],
            name: "emergency breaking".to_string(),
            time: None,
        };
        HaPlan {
            steps: vec![step2],
            is_fall_back: true,
        }
    };
    res.insert(break_state, break_plan);
    done.push(break_state);

    let turn_l_state = aha.state_map["turn_left"];
    let turn_l_plan = {
        let break_to_idle = aha.get_action_for_pair(turn_l_state, idle);
        let step2 = HaPlanStep {
            act_id: break_to_idle,
            condition: vec![],
            name: "stop turning left immediately".to_string(),
            time: Some(Duration::ZERO),
        };
        HaPlan {
            steps: vec![step2],
            is_fall_back: true,
        }
    };
    res.insert(turn_l_state, turn_l_plan);
    done.push(turn_l_state);

    let turn_r_state = aha.state_map["turn_right"];
    let turn_r_plan = {
        let break_to_idle = aha.get_action_for_pair(turn_r_state, idle);
        let step2 = HaPlanStep {
            act_id: break_to_idle,
            condition: vec![],
            name: "stop turning right immediately".to_string(),
            time: Some(Duration::ZERO),
        };
        HaPlan {
            steps: vec![step2],
            is_fall_back: true,
        }
    };
    res.insert(turn_r_state, turn_r_plan);
    done.push(turn_r_state);

    assert_eq!(done.len(), aha.ha.num_modes());
    res
}

#[cfg(test)]
mod tests {
    use crate::world_model::wm_parser::WMParser;

    use super::*;

    #[test]
    fn route_extraction() {
        let map =
            "5 5\n".to_string() + "XXXXX\n" + "_____\n" + "XX_XX\n" + "_____\n" + "XXXXX\n0 1 4 3";

        let wmp = WMParser::new();
        let wm = wmp.into_wm(map).expect("valid wm");
        let ps = wm.pos.clone();
        let pg = wm.goal.clone().unwrap();
        let pather = Pather::new(wm, pg, ps);

        let mut ha_sys_planer = HaRoutePlaner::new(
            LocationId(0),
            pather,
            HashMap::new(),
            0f64.into(),
            0f64.into(),
        );
        let route = ha_sys_planer.extract_route(None, 0.0.into());
        //dbg!(&route);
        assert_eq!(route.len(), 6);
    }

    #[test]
    fn maneuver_test() {
        let map =
            "5 5\n".to_string() + "XXXXX\n" + "_____\n" + "XX_XX\n" + "_____\n" + "XXXXX\n0 1 4 3";

        let wmp = WMParser::new();
        let wm = wmp.into_wm(map).expect("valid wm");
        let ps = wm.pos.clone();
        let pg = wm.goal.clone().unwrap();
        let pather = Pather::new(wm, pg, ps);

        let mut ha_sys_planer = HaRoutePlaner::new(
            LocationId(0),
            pather,
            HashMap::new(),
            0f64.into(),
            0f64.into(),
        );
        let route = ha_sys_planer.extract_route(None, 0.0.into());
        //dbg!(&route);
        let man = ha_sys_planer.compute_maneuvers(route);
        //dbg!(&man);
        assert_eq!(man.len(), 5);
    }
}
