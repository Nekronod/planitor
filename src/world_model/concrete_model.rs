//use num::Float;
use num_traits::Pow;
use priority_queue::PriorityQueue;
use std::{
    collections::HashMap,
    hash::Hash,
    iter,
    ops::{Add, Sub},
};

use super::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Angle {
    pub(crate) a: OrdF,
}

#[derive(Clone, Debug, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Coordinate {
    pub(crate) x: OrdF,
    pub(crate) y: OrdF,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Update {
    Tile(Position, TileType),
    Dummy,
}

const H_WEIGHT: f64 = 1.0; 

#[derive(Debug, Clone)]
pub(crate) struct Pather {
    pub(crate) wm: WorldModel,
    pub(crate) g_val: HashMap<Position, OrdF>,
    pub(crate) l_val: HashMap<Position, OrdF>,
    pub(crate) pos: Position,
    pub(crate) goal: Position,
    old_goal: Position,
    prev: Option<Position>,
    pub(crate) updates: Vec<Update>,
    update_pos_cords: Vec<Coordinate>,
    k_m: OrdF,
    pub(crate) done: bool,
    pq: PriorityQueue<Position, OrdF>,
    super_goal: Position,
    super_goal_distance: HashMap<Position, OrdF>,
}

impl Pather {
    pub fn new(wm: WorldModel, goal: Position, start: Position) -> Self {
        //dbg!(&goal,&start);
        let mut s = Pather {
            super_goal: Position::new_single(wm.grid.width as i16, wm.grid.height as i16),
            wm,
            super_goal_distance: vec![(goal.clone(), 0.0f64.into())].into_iter().collect(),
            old_goal: goal.clone(),
            goal,
            pos: start.clone(),
            prev: Some(start),
            k_m: 0f64.into(),
            g_val: HashMap::new(),
            l_val: HashMap::new(),
            updates: Vec::new(),
            update_pos_cords: Vec::new(),
            done: false,
            pq: PriorityQueue::new(),
        };
        s.super_goal_distance
            .insert(s.super_goal.clone(), 0.0f64.into());
        s.init();
        s.pq.push(s.super_goal.clone(), s.compute_key(&s.super_goal));
        s.compute_path();
        s
    }

    fn init_grid(&mut self, g: &Grid, pre_x: &[isize], pre_y: &[isize]) {
        for x in 0..g.width {
            for y in 0..g.height {
                let field: &Field = &g.grid[y][x];
                match field {
                    Field::Tile(_) => {
                        let pre_x = pre_x
                            .iter()
                            .cloned()
                            .chain(iter::once(x as isize))
                            .collect::<Vec<_>>();
                        let pre_y = pre_y
                            .iter()
                            .cloned()
                            .chain(iter::once(y as isize))
                            .collect::<Vec<_>>();
                        let p = Position::new(&pre_x, &pre_y);
                        self.g_val.insert(p.clone(), f64::INFINITY.into());
                        self.l_val.insert(p, f64::INFINITY.into());
                    }
                    Field::Grid(g) => {
                        let pre_x = pre_x
                            .iter()
                            .cloned()
                            .chain(iter::once(x as isize))
                            .collect::<Vec<_>>();
                        let pre_y = pre_y
                            .iter()
                            .cloned()
                            .chain(iter::once(y as isize))
                            .collect::<Vec<_>>();
                        self.init_grid(g, &pre_x, &pre_y);
                    }
                }
            }
        }
    }

    fn init(&mut self) {
        //self.init_grid(&self.wm.get_grid().to_owned(), &[], &[]);
        self.g_val
            .insert(self.super_goal.clone(), f64::INFINITY.into());
        self.l_val.insert(self.super_goal.clone(), 0f64.into());
        self.g_val.insert(self.pos.clone(), f64::INFINITY.into());
        self.l_val.insert(self.pos.clone(), f64::INFINITY.into());
        //self.l_val.insert(self.goal.clone(), 0f64.into());
    }

    fn heuristic(&self, from: &Position, to: &Position) -> OrdF {
        if from == &self.super_goal || to == &self.super_goal {
            if to == &self.goal || from == &self.goal {
                0.0f64.into()
            } else {
                self.distance(from, &self.goal) * OrdF::from(H_WEIGHT)
            }
        } else {
            self.distance(from, to) * OrdF::from(H_WEIGHT)
        }
        //(((from.x[0] - to.x[0]).abs() + (from.y[0] - to.y[0]).abs()) as f64).into()
    }

    fn compute_key(&self, pos: &Position) -> OrdF {
        //TODO: include g_val(s).min(l_val(s))
        if pos == &self.super_goal {
            //dbg!("supergoal key");
            //dbg!(&self.l_val[pos]);
            //dbg!(self.heuristic(&self.pos, pos));
            -(self.g_val.get(pos).unwrap_or(&f64::INFINITY.into()).min(self.l_val.get(pos).unwrap_or(&f64::INFINITY.into())) + self.heuristic(&self.pos, pos) + self.k_m)
        } else {
            -(self.g_val.get(pos).unwrap_or(&f64::INFINITY.into()).min(self.l_val.get(pos).unwrap_or(&f64::INFINITY.into())) + self.heuristic(&self.pos, pos) + self.k_m)
        }
    }

    fn distance(&self, a: &Position, b: &Position) -> OrdF {
        if a == &self.super_goal {
            return self
                .super_goal_distance
                .get(b)
                .map(|d| *d)
                .unwrap_or(f64::INFINITY.into());
        } else if b == &self.super_goal {
            return self
                .super_goal_distance
                .get(a)
                .map(|d| *d)
                .unwrap_or(f64::INFINITY.into());
        };
        //dbg!("non super goal distance");
        let a_f = self.wm.get_field(a);
        let b_f = self.wm.get_field(b);
        //dbg!(a,&a_f,b,&b_f);
        if let Field::Tile(TileType::Wall) = a_f {
            f64::INFINITY.into()
        } else if let Field::Tile(TileType::Wall) = b_f {
            f64::INFINITY.into()
        } else {
            self.wm.distance(a, b)
        }
    }

    fn distance_through_wall(&self, a: &Position, b: &Position) -> OrdF {
        if a == &self.super_goal && b == &self.old_goal {
            return 0.0f64.into();
        } else if b == &self.super_goal && a == &self.old_goal {
            return 0.0f64.into();
        } else if b == &self.super_goal || a == &self.super_goal {
            return f64::INFINITY.into();
        }
        self.wm.distance(a, b)
    }

    ///TODO
    fn update_distance(&self, _a: &Position, _b: &Position, _ty: &TileType) -> OrdF {
        todo!()
    }

    fn neighbors(&self, origin: &Position) -> Vec<Position> {
        if origin == &self.super_goal {
            let mut res: Vec<Position> = self.super_goal_distance.keys().cloned().collect();
            res.dedup();
            res
        } else if self
            .super_goal_distance
            .get(&origin)
            .unwrap_or(&OrdF::from(f64::INFINITY))
            < &OrdF::from(2_000_000.0f64)
        {
            let mut res = self.wm.neighbors(origin);
            res.push(self.super_goal.clone());
            res
        } else {
            self.wm.neighbors(origin)
        }
    }

    fn compute_path(&mut self) {
        //dbg!("computing path");
        //dbg!(&self.pq, *self.pq.peek().expect("ensured by first condition").1);
        //dbg!(self.compute_key(&self.pos), self.l_val[&self.pos], self.g_val[&self.pos]);
        while !self.pq.is_empty()
            && (*self.pq.peek().expect("ensured by first condition").1
                >= self.compute_key(&self.pos)
                || self.l_val[&self.pos] > self.g_val[&self.pos])
        {
            //dbg!("path_step");
            let (u, k_old) = self.pq.pop().unwrap();
            let k_new = self.compute_key(&u);
            //dbg!(u.to_string(),k_old,k_new, self.g_val[&u], self.l_val[&u]);
            // key improvement, better path found
            if k_old > k_new {
                //dbg!("k_old > k_new (key values got worse): add to pq -recheck later");
                // k_old < k_new TODO may be turned around due to negativity
                self.pq.push(u.clone(), k_new);
            } else if self.g_val.get(&u).unwrap_or(&f64::INFINITY.into())  > self.l_val.get(&u).unwrap_or(&f64::INFINITY.into()) {
                //dbg!("g(u) > l(u): set g = l - remove u from pq - add neighbors: found new best way to u");
                if let Some(mut_g_ref) = self.g_val.get_mut(&u){
                   *mut_g_ref = *self.l_val.get(&u).unwrap_or(&f64::INFINITY.into());
                } else {
                    self.g_val.insert(u.clone(),  *self.l_val.get(&u).unwrap_or(&f64::INFINITY.into()));
                }
                //assert_eq!(self.g_val[&u], self.l_val[&u]);
                self.pq.remove(&u);
                self.neighbors(&u).iter().for_each(|s| {
                    if *s != self.super_goal {
                        let new_key = self.l_val.get(&s).map(|x|*x).unwrap_or(f64::INFINITY.into()).min(self.distance(s, &u) + self.g_val[&u]);
                        //dbg!("inserted: ",s.to_string(), "with", new_key);
                        self.l_val.insert(s.clone(), new_key);
                        self.update_vertex(s);
                    }
                });
            } else {
                //dbg!("else : set g = inf - update neighbors and self");
                let g_old = self.g_val[&u];
                self.g_val.insert(u.clone(), f64::INFINITY.into());
                self.neighbors(&u)
                    .iter()
                    .chain(iter::once(&u))
                    .for_each(|s| {
                        //dbg!(s,&u, g_old, self.l_val[s]);
                        if self.l_val[s] == self.distance(s, &u) + g_old {
                            //dbg!("distance + g(u) = l(s)");
                            if *s != self.goal {
                                //dbg!("for non-goal");
                                let nn = self.neighbors(s);
                                self.l_val.insert(
                                    s.clone(),
                                    nn.iter()
                                        .map(|s_prime| {
                                            self.distance(s, s_prime) + self.g_val.get(&s_prime).unwrap_or(&f64::INFINITY.into())
                                        })
                                        .min()
                                        .unwrap(),
                                );
                                //dbg!(&self.l_val[s]);
                            }
                        }
                        self.update_vertex(s);
                    })
            }
        }
        //dbg!(&self.pq);
        //dbg!(self.compute_key(&self.pos), self.l_val[&self.pos], self.g_val[&self.pos]);

        /*
        self.g_val.iter().for_each(|(p,g)| if p != &self.super_goal && self.wm.get_tile(p) == &TileType::Walkable  {
            //dbg!(p,g);
        });
        */
        //let special_check = Position::new_single(8 as i16,7 as i16);
        //dbg!(self.g_val[&special_check]);
        /*
        self.l_val.iter().for_each(|(p,l)| if p != &self.super_goal && self.wm.get_tile(p) == &TileType::Walkable {
            //dbg!(p,l);
        });
        */
        //dbg!(self.l_val[&special_check]);
    }

    /// Updates an item in the Queue (calls [compute_key])
    fn update_vertex(&mut self, pos: &Position) {
        //dbg!("updating Pos:", pos, self.g_val[pos] , self.l_val[pos] );
        if pos != &self.goal {
            let n = self.neighbors(pos);
            let new_l = n
                .into_iter()
                .map(|n| self.distance(pos, &n) + self.g_val.get(&n).unwrap_or(&f64::INFINITY.into()))
                .min()
                .unwrap_or(f64::INFINITY.into());
            self.l_val.insert(pos.clone(), new_l);
        }
        //dbg!("new L =",  self.l_val[pos]);
        if self.g_val.get(pos).unwrap_or(&f64::INFINITY.into()) != self.l_val.get(pos).unwrap_or(&f64::INFINITY.into()) {
            // combines the \in U and \nin cases  as push updates or adds - depending on the case
            //dbg!("added yo pq");
            self.pq.push(pos.clone(), self.compute_key(pos));
        } else {
            //dbg!("removed from pq");
            self.pq.remove(&pos);
        }
    }

    pub fn d_star(&mut self) -> Vec<Position> {
        let mut res = vec![self.goal.clone()];
        // while (goal!= start)
        while self.pos != self.goal {
            // if start.l = inf -> abort, no path possible
            let inf: OrdF = f64::INFINITY.into();
            if self.l_val[&self.pos] == inf {
                todo!("no path found");
            }
            // next = cur.neighbors.get_min()
            let (_, n) = self
                .neighbors(&self.pos)
                .into_iter()
                .map(|n| {
                    let cost = self.distance(&self.pos, &n) + self.g_val.get(&n).unwrap_or(&f64::INFINITY.into());
                    (cost, n)
                })
                .min_by(|t1, t2| t1.0.cmp(&t2.0))
                .unwrap();
            // move to next
            let last = self.pos.clone();
            res.push(n.clone());
            self.pos = n;

            // scan for edge/cost changes
            //todo!("scan for cost changes");

            //if cost changed
            if !self
                .updates
                .iter()
                .filter(|update| matches!(update, Update::Tile(_, _)))
                .collect::<Vec<_>>()
                .is_empty()
            {
                self.k_m = self.k_m + self.heuristic(&last, &self.pos);
            }

            // last = start
            let mut updates = self.updates.clone();
            updates.dedup();
            let any_updates = !updates.is_empty();
            // if any cost changed:
            updates.into_iter().for_each(|up| {
                let (pos, _ty) = if let Update::Tile(pos, ty) = up {
                    (pos, ty)
                } else {
                    unreachable!()
                };
                let n = self.neighbors(&pos);
                n.iter().for_each(|neighbor| {
                    self.wm.update_wm_pos(&pos);
                    let c_old = self.distance_through_wall(&pos, neighbor);
                    let c_new = self.distance(&pos, neighbor);
                    //   if old_cost > new_cost
                    //      u.l = min(u.l, v.g + new_Cost)
                    if c_old > c_new {
                        self.l_val.insert(
                            neighbor.clone(),
                            self.l_val[&pos].min(c_new + self.g_val[&pos]),
                        );
                        self.l_val.insert(
                            pos.clone(),
                            self.l_val[&neighbor].min(c_new + self.g_val[&neighbor]),
                        );
                    } else if self.l_val[neighbor] == c_old + self.g_val[&pos] {
                        //   else if u.l = old + v.g
                        //      u.l = min over all neighbors n:  c(u,n) + n.g
                        self.l_val.insert(
                            neighbor.clone(),
                            self.wm
                                .neighbors(&neighbor)
                                .iter()
                                .map(|s_p| self.distance(neighbor, s_p) + self.g_val[s_p])
                                .min()
                                .unwrap(),
                        );

                        self.l_val.insert(
                            pos.clone(),
                            self.wm
                                .neighbors(&pos)
                                .iter()
                                .map(|s_p| self.distance(&pos, s_p) + self.g_val[s_p])
                                .min()
                                .unwrap(),
                        );
                    }
                });

                self.update_vertex(&pos);
                n.iter().for_each(|n| self.update_vertex(n));
            });
            if any_updates {
                self.compute_path();
            }
        }
        res
    }

    //interactive variant

    pub fn next_move(&mut self) -> Option<Position> {
        //dbg!("next move", self.done);
        if self.done {
            return None;
        }
        let r = self
            .neighbors(&self.pos)
            .into_iter()
            .map(|n| {
                let cost = self.distance(&self.pos, &n) + self.g_val.get(&n).unwrap_or(&f64::INFINITY.into());
                //dbg!(&self.pos, cost,&n, &self.g_val[&n]);
                (cost, n)
            })
            .filter(|(c, _)| c < &f64::INFINITY.into())
            .min_by(|t1, t2| t1.0.cmp(&t2.0));
        r.map(|(_, pos)| pos)
    }

    pub fn perform_move(&mut self, new_p: Position) {
        assert!(self.neighbors(&self.pos).contains(&new_p));
        self.pos = new_p;
        if self.pos == self.goal {
            self.done = true;
        }
    }

    pub(crate) fn update_goal_hard(&mut self, new_goal: Position) -> Result<(), &str> {
        self.goal = new_goal;
        self.init();
        self.pq
            .push(self.super_goal.clone(), self.compute_key(&self.super_goal));
        self.compute_path();
        Ok(())
    }

    pub(crate) fn update_goal(&mut self, new_goal: Position) -> Result<(), &str> {
        self.done = false;
        let mut old_goal = new_goal; //create local var to overwrite
        std::mem::swap(&mut old_goal, &mut self.goal);
        //dbg!(&old_goal,&self.goal);
        self.old_goal = old_goal;

        self.super_goal_distance
            .insert(self.goal.clone(), 0.0f64.into());
        *self.super_goal_distance.get_mut(&self.old_goal).unwrap() = 1_000_000.0f64.into();

        //self.k_m = 0.0f64.into();

        //dbg!(self.g_val[&self.super_goal], self.l_val[&self.super_goal]);

        //dbg!(&self.compute_key(&self.goal));
        self.updates
            .push(Update::Tile(self.super_goal.clone(), TileType::Walkable));

        //dbg!(&self.pq);
        self.update_path()
        //dbg!(&self.pq);
    }

    pub fn update_path(&mut self) -> Result<(), &str> {
        if self.updates.is_empty() && self.g_val.get(&self.goal).unwrap_or(&f64::INFINITY.into()) == self.l_val.get(&self.goal).unwrap_or(&f64::INFINITY.into()) {
            return Err("Update Failed: no changes registered");
        }

        self.k_m = self.k_m + self.heuristic(&self.prev.as_ref().unwrap(), &self.pos);
        //dbg!("increasing k_m by: ", self.heuristic(&self.prev.as_ref().unwrap(), &self.pos));
        self.prev = Some(self.pos.clone());

        let mut updates = self.updates.clone();
        updates.dedup();
        self.updates.clear();
        let any_updates = !updates.is_empty();
        //dbg!(&updates);
        // if any cost changed:
        updates.into_iter().for_each(|up| {
            let (pos, _ty) = if let Update::Tile(pos, ty) = up {
                (pos, ty)
            } else {
                unreachable!()
            };
            //dbg!("updating neighbors for ", &pos);
            let neighbors_of_pos = self.neighbors(&pos);
            let affect_pair_iter = neighbors_of_pos.iter().map(|n| (n, &pos));
            let affected_pairs: Vec<(&Position, &Position)> = affect_pair_iter
                .clone()
                .map(|(u, v)| (v, u))
                .chain(affect_pair_iter)
                .collect();
            for (u, v) in affected_pairs.into_iter() {
                if u == &self.super_goal {
                    continue;
                }
                //dbg!("updating edge for ", u,v);
                let c_old = if u == &self.super_goal && v == &self.old_goal {
                    0.0f64.into()
                } else {
                    self.distance_through_wall(u, v)
                };
                let c_new = self.distance(u, v);
                //   if old_cost > new_cost
                //      u.l = min(u.l, v.g + new_Cost)
                //dbg!("update case", c_old, c_new);
                if c_old > c_new {
                    //dbg!("case1: l[u] = x", self.l_val[&u]);
                    //todo check direction
                    self.l_val
                        .insert(u.clone(), self.l_val.get(&u).map(|ordf| *ordf).unwrap_or(f64::INFINITY.into()).min(c_new + self.g_val.get(&v).unwrap_or(&f64::INFINITY.into())));
                    //dbg!("l[u] = x ", self.l_val[&u]);
                } else if self.l_val[&u] == c_old + self.g_val.get(&v).unwrap_or(&f64::INFINITY.into()) {
                    //dbg!("case2: l[n] = min(neighbors of n)");
                    //   else if u.l = old + v.g
                    //      u.l = min over all neighbors n:  c(u,n) + n.g
                    //dbg!(&u, self.neighbors(&u), &self.super_goal_distance);
                    self.l_val.insert(
                        u.clone(),
                        self.neighbors(&u)
                            .iter()
                            .map(|s_prime| self.distance(u, s_prime) + self.g_val.get(&s_prime).unwrap_or(&f64::INFINITY.into()))
                            .min()
                            .unwrap(),
                    );
                    //dbg!(&self.l_val[u]);
                }

                self.update_vertex(&u);
            }
        });
        if any_updates {
            //dbg!(self.k_m);
            self.compute_path();
        }

        Ok(())
    }

    pub(crate) fn update_wm(&mut self, pos: Position) {
        self.wm.update_wm_pos(&pos);
        self.updates.push(Update::Tile(pos, TileType::Wall));
    }

    pub(crate) fn update_wm_cords(
        &mut self,
        cur_pos: &Coordinate,
        angle: Angle,
        distance: OrdF,
    ) -> Position {
        let (target_c,target_p) = self.wm.update_wm(cur_pos, angle, distance);
        self.update_pos_cords.push(target_c);
        self.updates
            .push(Update::Tile(target_p.clone(), TileType::Wall));
        target_p
    }

    pub(crate) fn get_wm(&self) -> &WorldModel {
        &self.wm
    }

    pub(crate) fn get_wm_mut(&mut self) -> &mut WorldModel {
        &mut self.wm
    }

    pub(crate) fn change_tile(&mut self, pos: Position, tile: TileType) {
        let tc = Update::Tile(pos, tile);
        self.updates.push(tc);
    }
}

impl WorldModel {
    pub(crate) fn refine_global(&mut self, dynamic_obj_cords: Vec<Coordinate>) {
        let dynamic_obj_pos_vec: Vec<_> = dynamic_obj_cords.iter().map(|c| self.position_from_cords(*c)).collect();
        dynamic_obj_pos_vec.iter().for_each(|p| self.remove_wall(p));

        self.grid = self.grid.clone().g_refine_grid();

        let dynamic_obj_pos_vec_refined: Vec<_> = dynamic_obj_cords.iter().map(|c| self.position_from_cords(*c)).collect();
        dynamic_obj_pos_vec_refined.iter().for_each(|p| self.set_wall(p));


    }

    


    pub(crate) fn refine_for_cords(&mut self, cords_vec: Vec<Coordinate>) {
        println!("{}",self.grid.to_string());
        let pos_vec: Vec<_> = cords_vec.iter().map(|c| self.position_from_cords(*c)).collect();
        pos_vec.iter().for_each(|p| self.remove_wall(p));

        pos_vec.iter().for_each(|p|{
            let f = self.get_field_mut(&p);
            match &f {
                Field::Grid(_) => todo!(),
                Field::Tile(_t) => {}
            }
            let grid = Grid::new_clear(2,2);
            *f = Field::Grid(grid);
        });
    
        cords_vec.iter().for_each(|c|{
            let pos = self.position_from_cords(*c);

            self.update_wm_pos(&pos);
        });
        println!("Refined world Model");
        println!("{}",self.grid.to_string());
    }



    pub(crate) fn update_wm(
        &mut self,
        cur_pos: &Coordinate,
        angle: Angle,
        distance: OrdF,
    ) -> (Coordinate,Position) {
        let absolute_pos: Coordinate = *cur_pos;

        let (x_p, y_p): (f64, f64) = absolute_pos.into();
        let angle_f: f64 = angle.into();
        let (y_percentile, x_percentile): (f64, f64) = angle_f.to_radians().sin_cos();
        //dbg!(x_percentile,y_percentile, angle);

        let estimated_obstacle_cords: Coordinate = (
            x_p + x_percentile * distance.0,
            y_p + y_percentile * distance.0,
        )
            .into();
        self.wm_updates.push(estimated_obstacle_cords);

        let pos = self.position_from_cords(estimated_obstacle_cords);

        self.update_wm_pos(&pos);
        //dbg!("updating: ", &pos, absolute_pos, estimated_obstacle_cords);
        (estimated_obstacle_cords,pos)
    }

    pub(crate) fn update_wm_pos(&mut self, target_pos: &Position) {
        //dbg!(&target_pos);
        let f = self.get_field_mut(target_pos);
        let new_f = match f {
            Field::Grid(_) => todo!(),
            Field::Tile(t) => match t {
                TileType::Walkable => Field::Tile(TileType::Wall),
                TileType::Wall => Field::Tile(TileType::Wall),
            },
        };
        *f = new_f;
    }

    pub(crate) fn remove_wall(&mut self, target_pos: &Position) {
        //dbg!(&target_pos);
        let f = self.get_field_mut(target_pos);
        let new_f = match f {
            Field::Grid(_) => todo!(),
            Field::Tile(t) => match t {
                TileType::Walkable => Field::Tile(TileType::Walkable),
                TileType::Wall => Field::Tile(TileType::Walkable),
            },
        };
        *f = new_f;
    }

    pub(crate) fn set_wall(&mut self, target_pos: &Position) {
        //dbg!(&target_pos);
        let f = self.get_field_mut(target_pos);
        let new_f = match f {
            Field::Grid(_) => todo!(),
            Field::Tile(t) => match t {
                TileType::Walkable => Field::Tile(TileType::Wall),
                TileType::Wall => Field::Tile(TileType::Wall),
            },
        };
        *f = new_f;
    }

    /// returns the distance between two positions
    /// (1,0) (1,3) = 3
    /// (1,0) (0,1) = 1.432...
    /// ([0,1],[0,0]) ([1,1],[1,0]) = 1.432...
    pub fn distance(&self, a: &Position, b: &Position) -> OrdF {
        self.distance_angle(a, b).0
    }

    /// returns the distance and angle between two given positions
    pub fn distance_angle(&self, a: &Position, b: &Position) -> (OrdF, Angle) {
        let (a_c_x, a_c_y): (OrdF, OrdF) = self.cords(a).into();
        let (b_c_x, b_c_y): (OrdF, OrdF) = self.cords(b).into();

        //dbg!(a_c_x,a_c_y, b_c_x,b_c_y);

        let raw_distance: f64 = (a_c_x - b_c_x).pow(2) + (a_c_y - b_c_y).pow(2);
        let rad_angle = (b_c_y - a_c_y).atan2((b_c_x - a_c_x).0);
        let angle: f64 = rad_angle.to_degrees();
        (raw_distance.sqrt().into(), angle.into())
    }

    pub fn position_from_cords(&self, cord: Coordinate) -> Position {
        let mut cur_grid = &self.grid;
        let mut x = vec![];
        let mut y = vec![];
        let Coordinate { x: c_x, y: c_y } = cord;
        let mut c_x = c_x.0;
        let mut c_y = c_y.0;

        loop {
            let ix_x = c_x.trunc() as usize;
            let ix_y = c_y.trunc() as usize;
            let sub_field = &cur_grid.grid.get(ix_y).and_then(|v| v.get(ix_x));
            let sub_field = if let Some(f) = sub_field {
                f
            } else {
                panic!(
                    "Position from cords: Tried to access out of bounds sub_field:  X: {}, Y:{}",
                    c_x, c_y
                );
            };
            match sub_field {
                Field::Grid(g) => {
                    x.push(ix_x as isize);
                    y.push(ix_y as isize);
                    c_x = c_x.fract() * (g.width as f64);
                    c_y = c_y.fract() * (g.height as f64);
                    cur_grid = g;
                }
                Field::Tile(_) => {
                    x.push(ix_x as isize);
                    y.push(ix_y as isize);
                    return Position::new(&x, &y);
                }
            }
        }
    }

    ///Computes the coordinates of a given position as a float
    pub fn cords(&self, a: &Position) -> Coordinate {
        let pos_len = a.len();

        let mut old_p_x = 0.0;
        let mut old_p_y = 0.0;
        let mut cur_grid = &self.grid;
        let mut scale_fac_x = 1.0;
        let mut scale_fac_y = 1.0;

        for ((ix, x), y) in a.x.iter().enumerate().zip(a.y.iter()) {
            //dbg!((x,y, pos_len));
            let next_sub_field = &cur_grid.grid[*y as usize][*x as usize];
            let p_x = *x as f64; // (cur_grid.width as f64);
            let p_y = *y as f64; // (cur_grid.height as f64);

            //dbg!(x,cur_grid.width,p_x,y, cur_grid.height,p_y);

            //dbg!(next_sub_field);
            match next_sub_field {
                Field::Grid(g) if ix < pos_len - 1 => {
                    old_p_x = old_p_x + p_x * scale_fac_x;
                    old_p_y = old_p_y + p_y * scale_fac_y;

                    scale_fac_x /= g.width as f64;
                    scale_fac_y /= g.height as f64;

                    cur_grid = g;
                }
                Field::Grid(_) | Field::Tile(_) => {
                    if matches!(next_sub_field, Field::Grid(_)) {
                        eprintln!("Accessing Cords for Position not matching the Grid: Pos longer than grid depth");
                    }
                    let mid_x_add = 0.5; // (cur_grid.width as f64);
                    let mid_y_add = 0.5; // (cur_grid.height as f64);
                                         //dbg!(old_p_x, scale_fac_x, p_x, mid_x_add);
                    let res_x = old_p_x + (p_x + mid_x_add) * scale_fac_x; // * (self.grid.width as f64);
                    let res_y = old_p_y + (p_y + mid_y_add) * scale_fac_y; // * (self.grid.height as f64);

                    return Coordinate {
                        x: res_x.into(),
                        y: res_y.into(),
                    };
                }
            }
        }
        unreachable!("func will end on a field and return early")

        /*
        //percentile of map position in x ans y direction
        let (a_p_x, a_p_y, _, _, _) = a.x.iter().zip(a.y.iter()).fold(
            (0f64, 0f64, &self.grid, 1f64, 1f64),
            |acc, (pos_ix_x, pos_ix_y)| {
                let (old_p_x, old_p_y, cur_grid, tile_size_x, tile_size_y) = acc;
                let p_x = *pos_ix_x as f64 / (cur_grid.width as f64);
                let p_y = *pos_ix_y as f64 / (cur_grid.height as f64);


                let sub_grid = match sub_field {
                    Field::Tile(_) => {
                        let p_x_last = 0.5 * (cur_grid.width as f64);
                        let p_y_last = 0.5 * (cur_grid.height as f64);

                        let res_x = p_x + p_x_last;
                        let res_y = p_y + p_y_last;

                        return (
                            old_p_x + res_x * tile_size_x,
                            old_p_y + res_y * tile_size_y,
                            cur_grid,
                            tile_size_x,
                            tile_size_y,
                        );
                    }
                    Field::Grid(sub_grid) => sub_grid,
                };

                let res_x = p_x * tile_size_x + old_p_x;
                let res_y = p_y * tile_size_y + old_p_y;

                (
                    res_x,
                    res_y,
                    sub_grid,
                    tile_size_x / (cur_grid.width as f64),
                    tile_size_y / (cur_grid.height as f64),
                )
            },
        );

        Coordinate {
            x: a_p_x.into(),
            y: a_p_y.into(),
        }
        */
    }
}

impl PartialEq for Position {
    fn eq(&self, other: &Self) -> bool {
        self.x.len() == other.y.len()
            && self.x.iter().zip(other.x.iter()).all(|(x, y)| x == y)
            && self.y.iter().zip(other.y.iter()).all(|(x, y)| x == y)
    }
}

impl Eq for Position {}

impl Hash for Position {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        self.y.hash(state);
    }
}

impl From<(f64, f64)> for Coordinate {
    fn from(s: (f64, f64)) -> Self {
        Coordinate {
            x: s.0.into(),
            y: s.1.into(),
        }
    }
}

impl From<(OrdF, OrdF)> for Coordinate {
    fn from(s: (OrdF, OrdF)) -> Self {
        Coordinate { x: s.0, y: s.1 }
    }
}

impl Into<(OrdF, OrdF)> for Coordinate {
    fn into(self) -> (OrdF, OrdF) {
        let Coordinate { x, y } = self;
        (x, y)
    }
}

impl Into<(f64, f64)> for Coordinate {
    fn into(self) -> (f64, f64) {
        let Coordinate { x, y } = self;
        (x.0, y.0)
    }
}

impl From<OrdF> for Angle {
    fn from(s: OrdF) -> Self {
        Self::new(s.0)
    }
}

impl From<f64> for Angle {
    fn from(s: f64) -> Self {
        Self::new(s)
    }
}

impl Into<OrdF> for Angle {
    fn into(self) -> OrdF {
        self.a
    }
}

impl Into<f64> for Angle {
    fn into(self) -> f64 {
        self.a.0
    }
}

impl Add for Angle {
    type Output = Angle;

    fn add(self, rhs: Self) -> Self::Output {
        let max = 360f64.into();
        let a = self.a + rhs.a;
        (if a > max {
            a % max
        } else if a < 0f64.into() {
            a + max
        } else {
            a
        })
        .into()
    }
}

impl Sub for Angle {
    type Output = Angle;

    fn sub(self, rhs: Self) -> Self::Output {
        let max = 360f64.into();
        let a = self.a - rhs.a;
        (if a > max {
            a % max
        } else if a < 0f64.into() {
            let mut a2 = a;
            while a2 < 0f64.into() {
                a2 = a2 + max;
            }
            a2
        } else {
            a
        })
        .into()
    }
}

impl Angle {
    pub fn inner(&self) -> OrdF {
        self.a
    }

    pub fn new(mut a: f64) -> Self {
        while a < 0f64 {
            a += 360.0;
        }
        a = a % 360.0;
        Self { a: a.into() }
    }
}

#[cfg(test)]
mod tests {
    use crate::world_model::wm_parser::WMParser;

    use super::*;

    #[test]
    fn pather_test_full_call() {
        let map =
            "5 5\n".to_string() + "XXXXX\n" + "_____\n" + "XX_XX\n" + "_____\n" + "XXXXX\n0 1 4 3";

        let wmp = WMParser::new();
        let wm = wmp.into_wm(map).expect("valid wm");
        let ps = wm.pos.clone();
        let pg = wm.goal.clone().unwrap();
        dbg!(&ps, &pg);
        let mut pather = Pather::new(wm, pg, ps);

        let route = pather.d_star();
        dbg!(&route);
        assert_eq!(route.len(), 7);
    }

    #[test]
    fn pather_interactive() {
        let map =
            "5 5\n".to_string() + "XXXXX\n" + "_____\n" + "XX_XX\n" + "_____\n" + "XXXXX\n0 1 4 3";

        let wmp = WMParser::new();
        let wm = wmp.into_wm(map).expect("valid wm");
        let ps = wm.pos.clone();
        let pg = wm.goal.clone().unwrap();
        let mut pather = Pather::new(wm, pg, ps);
        //as we don't have updates in this case
        let mut route = vec![];
        while let Some(p) = pather.next_move() {
            dbg!(&p);
            if route.contains(&p) {
                panic!()
            }
            route.push(p.clone());
            pather.perform_move(p);
        }

        dbg!(&route);
        assert_eq!(route.len(), 6);
        assert_eq!(route.first().unwrap(), &Position::new(&[1], &[1]));
        assert_eq!(route.last().unwrap(), &Position::new(&[4], &[3]));
    }

    #[test]
    fn pather_small_grid() {
        let map = "5 5\n".to_string()
            + "XXXXX\n"
            + "_____\n"
            + "XX 2 2 __ X_ XX\n"
            + "_____\n"
            + "XXXXX\n0 1 4 3";

        let wmp = WMParser::new();
        let wm = wmp.into_wm(map).expect("valid wm");
        let ps = wm.pos.clone();
        let pg = wm.goal.clone().unwrap();

        let tp1 = Position::new(&[2, 1], &[2, 1]);
        let tp2 = Position::new(&[2, 1], &[2, 0]);
        assert!((wm.distance(&tp1, &tp2) - OrdF::from(0.5f64)).abs() < 0.00001f64);

        let test_pos = Position::new(&[2, 1], &[2, 1]);
        let n = wm.neighbors(&test_pos);
        dbg!(&n);
        assert_eq!(n.len(), 2);
        let mut pather = Pather::new(wm, pg, ps);
        //as we don't have updates in this case
        let mut route = vec![];
        while let Some(p) = pather.next_move() {
            dbg!(&p);
            if route.contains(&p) {
                panic!()
            }
            route.push(p.clone());
            pather.perform_move(p);
        }

        dbg!(&route);
        assert_eq!(route.len(), 7);
        assert_eq!(route.first().unwrap(), &Position::new(&[1], &[1]));
        assert_eq!(route.last().unwrap(), &Position::new(&[4], &[3]));
    }
}
