use std::io::Write;
use std::process::{Command, Output};
use std::{cmp::Ordering, collections::BinaryHeap, fs};

use self::domain_handler::DomainHandler;

use super::{OrdF, Position};

pub(crate) mod domain_handler;
pub(crate) mod visitall_handler;

const DEFAULT_TMP_FILE: &str = "cur_task_problem.pddl";

#[derive(Debug, Clone)]
pub(crate) struct AbstractWorldPlanner<D>
where
    D: DomainHandler,
{
    pub(crate) dom_handler: D,
    pub(crate) downward: DownwardPlanner,
    pub(crate) pddl_folder: String,
}

impl<D: DomainHandler> AbstractWorldPlanner<D> {
    pub fn new(dh: D, input_pddl: String) -> Self {
        let mut pddl_folder = "".to_string();
        let mut tmp_str = "".to_string();
        for c in input_pddl.chars() {
            if c == '/' {
                tmp_str += "/";
                pddl_folder.push_str(&tmp_str);
                tmp_str.clear();
            } else {
                tmp_str += &c.to_string();
            }
        }
        let tmp_pddl_file = pddl_folder.clone() + DEFAULT_TMP_FILE;
        let dw = DownwardPlanner::new(tmp_pddl_file);

        Self {
            dom_handler: dh,
            downward: dw,
            pddl_folder,
        }
    }

    pub fn compute_plan(&self) -> Plan {
        if self.dom_handler.is_dummy() {
            return vec![];
        }
        let file_str = self.dom_handler.construct_call_file_string();
        let tmp_pddl_file = self.pddl_folder.clone() + DEFAULT_TMP_FILE;
        //dbg!(&tmp_pddl_file, &file_str);
        if let Err(e) = fs::write(tmp_pddl_file, file_str.clone()) {
            panic!("unable to write to default tmp file: {}", e.to_string());
        }
        let mut file = fs::OpenOptions::new()
            .write(true)
            .append(true)
            .create(true)
            .open("planning_log.txt")
            .unwrap();
        write!(file, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n").expect("could not write to log file");
        write!(file, "{}", file_str).expect("could not write file_Str to log file");
        match self.downward.call_write_file() {
            Ok(()) => {}
            Err(msg) => panic!("could not compute plan - downward call error: {}", msg),
        }
        match self.downward.open_read_plan_file(&self.dom_handler) {
            Ok(p) => p,
            Err(msg) => panic!("could not extract plan result - error: {}", msg),
        }
    }

    
}

//pub(crate) struct AbstractMapper {}

/// The Fast Downward planning handler
/// Calls to an external Python/Cpp library
///
/// Example calls:
/// # landmark-cut heuristic
/// ./fast-downward.py domain.pddl task.pddl --search "astar(lmcut())"
///
/// # blind heuristic
///  ./fast-downward.py domain.pddl task.pddl --search "astar(blind())"
#[derive(Debug, Clone)]
pub(crate) struct DownwardPlanner {
    pddl_file: String,
    search_arg: String, //tdo extract enum
}

#[derive(Debug, Clone)]
pub enum PlanningInstruction {
    /// Planning step go from:Pos1 to:Pos2
    Move(Position, Position),
    Other(String),
}
pub(crate) type Plan = Vec<PlanningInstruction>;

impl DownwardPlanner {
    pub(crate) fn new(problem: String) -> Self {
        Self {
            pddl_file: problem,
            search_arg: "astar(lmcut())".to_string(),
        }
    }

    fn call_write_file(&self) -> Result<(), String> {
        //let dp= DownwardPlanner {pddl_file_folder: "benchmarks/gripper".to_string(), search_arg: "astar(lmcut())".to_string()};
        let DownwardPlanner {
            pddl_file: pddl_file_folder,
            search_arg,
        } = self;
        //dbg!(search_arg, &pddl_file_folder);
        let output: Output = Command::new("python3")
            .arg("dependencies/downward/fast-downward.py")
            .arg("--plan-file")
            .arg("output-plan.txt")
            .arg("/home/nekronod//uni/master/Master-Thesis/code/".to_string() + pddl_file_folder)
            .arg("--search")
            .arg(search_arg)
            .output()
            .expect("failed to execute process");
        //dbg!(&output);
        let status = output.status;
        if !status.success() {
            let error_msg = "Error encountered during Fast-Downward call! \n".to_string()
                + &std::str::from_utf8(&output.stderr).unwrap();
            return Err(error_msg);
        }
        Ok(())
    }

    fn open_read_plan_file<D: DomainHandler>(&self, dh: &D) -> Result<Plan, String> {
        let plan_string = fs::read_to_string("output-plan.txt")
            .map_err(|s| format!("unable to read output plan file: {}", s.to_string()))?;
        let lines = plan_string.lines();
        let mut plan = vec![];
        let mut plan_cost = -1;
        let mut unit_cost_flag = false;

        for line in lines {
            if line.starts_with(";") {
                plan_cost = line
                    .split_whitespace()
                    .filter(|s| s.chars().all(|c| c.is_numeric()))
                    .find(|_| true)
                    .unwrap_or("-1")
                    .parse()
                    .unwrap();
                unit_cost_flag = line.contains("unit cost");
                continue;
            }
            let trimmed = line.trim_start_matches("(").trim_end_matches(")");
            if trimmed.starts_with("move") {
                let ins = dh.extract_move(trimmed);
                plan.push(ins);
            } else {
                plan.push(PlanningInstruction::Other(trimmed.to_string()));
            }
        }
        if plan.is_empty() || plan_cost == -1 {
            return Err("No Valid plan found, check Fast-Downward call output!".to_string());
        }
        if unit_cost_flag {
            assert_eq!(plan.len() as isize, plan_cost);
        }
        Ok(plan)
    }
}

struct AbstractWorldModel {
    nodes: Vec<Node>,
    start: usize,
}

#[derive(Debug, Clone)]
pub struct Node {
    edges: Vec<Edge>,
    id: usize,
    cost: OrdF,
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Edge {
    target: usize,
    source: usize,
    distance: OrdF,
}

#[derive(Debug, Clone, Copy)]
struct SearchState {
    node_id: usize,
    parent: usize,
    d_cost: OrdF,
    h_val: OrdF,
}

impl Ord for SearchState {
    fn cmp(&self, other: &Self) -> Ordering {
        let val = self.d_cost + self.h_val;
        let o_val = other.d_cost + other.h_val;
        val.cmp(&o_val)
    }
}

impl PartialOrd for SearchState {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for SearchState {
    fn eq(&self, other: &Self) -> bool {
        let val = self.d_cost + self.h_val;
        let o_val = other.d_cost + other.h_val;
        val.eq(&o_val)
    }
}

impl Eq for SearchState {}

impl AbstractWorldModel {
    pub(crate) fn new(nodes: Vec<Node>, start: usize) -> Self {
        Self { nodes, start }
    }

    pub(crate) fn a_star(&self, goal: &Node) -> Vec<Edge> {
        let inf_const: OrdF = (f64::MAX / 2f64).into();
        let mut search_vec: Vec<_> = self
            .nodes
            .iter()
            .map(|n| SearchState {
                node_id: n.id,
                d_cost: (f64::MAX / 2f64).into(),
                h_val: (f64::MAX).into(),
                parent: usize::MAX,
            })
            .collect();
        let mut heap = BinaryHeap::new();

        let start = search_vec[self.start];
        search_vec[self.start].h_val = self.heuristic_val_a_star(goal, &start).into();
        search_vec[self.start].d_cost = 0f64.into();
        heap.push(start);

        while let Some(SearchState {
            node_id,
            d_cost,
            h_val: _,
            parent: _,
        }) = heap.pop()
        {
            if node_id == goal.id {
                return self.reconstruct_path(search_vec, goal);
            }

            self.nodes[node_id].edges.iter().for_each(|n| {
                let mut target = search_vec[n.target];
                let target_h = {
                    if target.h_val >= inf_const {
                        target.h_val = self.heuristic_val_a_star(goal, &target).into()
                    };
                    target.h_val
                };
                let new_target_score = d_cost + n.distance;
                if new_target_score > target_h + target.d_cost {
                    return;
                }

                target.d_cost = d_cost + n.distance;
                target.parent = node_id;
                heap.push(target);
            })
        }
        return Vec::new();
    }

    fn heuristic_val_a_star(&self, _goal: &Node, _current: &SearchState) -> f64 {
        //TODO
        return 0f64;
    }

    fn reconstruct_path(&self, states: Vec<SearchState>, goal: &Node) -> Vec<Edge> {
        let mut cur = states[goal.id];
        let mut res = Vec::new();
        while cur.parent <= states.len() {
            let prev = states[cur.node_id].parent;
            let e = self.nodes[prev]
                .edges
                .iter()
                .find(|e| e.target == cur.node_id)
                .unwrap();
            res.push(*e);
            cur = states[prev];
        }
        res.reverse();
        res
    }
}

#[cfg(test)]
mod abstract_tests {
    use super::*;

    #[test]
    fn parse_int_test() {
        let s = "0test";
        let i = match s.parse::<i32>() {
            Ok(i) => i,
            Err(_e) => -1,
        };
        assert_eq!(-1, i);
    }

    #[test]
    #[ignore]
    fn direct_call() {
        use std::process::Command;

        let dp = DownwardPlanner {
            pddl_file:
                "/Users/stefan/Uni/Master-Thesis/code/dependencies/downward/benchmarks/gripper"
                    .to_string(),
            search_arg: "astar(lmcut())".to_string(),
        };

        // python3 fast-downward.py misc/tests/benchmarks/gripper/*.pddl --search "astar(lmcut())"
        let DownwardPlanner {
            pddl_file: pddl_file_folder,
            search_arg,
        } = dp;
        let output: Output = Command::new("python3")
            .arg("/Users/stefan/Uni/Master-Thesis/code/dependencies/downward/fast-downward.py")
            .arg("--plan-file")
            .arg("output-plan.txt")
            .arg(pddl_file_folder + "/prob01.pddl")
            .arg("--search")
            .arg(search_arg)
            .output()
            .expect("failed to execute process");
        let res = format!(
            "Out: {}\nErr: {}",
            std::str::from_utf8(&output.stdout).unwrap(),
            std::str::from_utf8(&output.stderr).unwrap()
        );
        let sol_split: Vec<_> = res.split_terminator("Actual search time").collect();
        let after_sol = sol_split[1];
        let pre_time_split: Vec<_> = after_sol.split_terminator("\n[t=").collect();
        let should_be_solution = pre_time_split[0];
        let trimmed = should_be_solution.split_whitespace().collect::<Vec<_>>()[1..].join("\n");
        dbg!(trimmed);
        dbg!(output);
        dbg!(res);
        // test fulfilled its purpose: checked call api and output format
    }

    #[test]
    fn parse_test() {
        let n: i32 = "; cost = 11 test"
            .split_whitespace()
            .filter(|s| s.chars().all(|c| c.is_numeric()))
            .find(|_| true)
            .unwrap_or("-1")
            .parse()
            .unwrap();
        assert_eq!(n, 11);
    }
}
