use std::time::Duration;

use colored::Colorize;
use num_traits::Pow;

use crate::{
    world_model::{concrete_model::Coordinate, Position, TileType},
    OrdF,
};

use super::WorldModel;

pub enum Shape {
    Circle(OrdF), //radius
    Square(OrdF),
    Rectangle(OrdF, OrdF), //w,h
}

pub struct WMSimulator<'a> {
    pub wm: &'a WorldModel,
    pub(crate) pos: Position,
    pub(crate) exact_position: Coordinate,
    pub angle: OrdF,
    pub velo: OrdF,
    sys_size: Shape,
}

impl<'a> WMSimulator<'a> {
    pub fn new(wm: &'a WorldModel) -> Self {
        Self {
            pos: wm.pos.clone(),
            exact_position: wm.cords(&wm.pos),
            wm,
            angle: 0f64.into(),
            velo: 0f64.into(),
            sys_size: Shape::Circle(1f64.into()),
        }
    }

    fn emit(&self) {
        use colored::*;
        let mut res = String::new();
        res += "Position: ";
        res += &self.pos.to_string();
        res += " V:= ";
        res += &format!("{:.5}", self.velo.0.to_string().red());
        res += " Angle:= ";
        res += &format!("{:.5}\n", self.angle.0.to_string().green());
        res += &format!(
            "{}",
            self.wm
                .grid
                .to_string_annotated(Some(&self.pos), self.angle)
        );
        print!("{}\n\n", res);
    }

    pub fn move_time(&mut self, time: Duration) -> bool {
        self.move_by(0f64.into(), time)
    }

    pub fn turn(&mut self, angle_velo: OrdF, time: Duration) -> bool {
        assert!(
            self.velo <= 0.001f64.into(),
            "Turning at speed not yet implemented"
        );

        let time_sec: OrdF = time.as_secs_f64().into();
        let angle_change = angle_velo * time_sec;
        self.turn_by_angle(angle_change)
    }

    pub(crate) fn turn_by_angle(&mut self, a: OrdF) -> bool {
        let old_angle = self.angle;
        self.angle += a;
        /*
        self.angle = if self.angle > 360f64.into() {
            self.angle % max
        } else if self.angle < zero {
            self.angle + max
        } else {
            self.angle
        };
        */
        self.angle = crate::round(self.angle);
        println!(
            "{}",
            format!("Turned by {:.2}, from old Angle := {:.2}", a, old_angle).green()
        );
        self.emit();
        //dbg!(self.angle);
        false
    }

    pub fn acc(&mut self, a: OrdF, time: Duration) -> bool {
        self.move_by(a, time)
    }

    /// Reports if a collision occurred
    fn move_by(&mut self, a: OrdF, time_d: Duration) -> bool {
        let starting_v = self.velo;
        //dbg!(self.velo, a, time_d, assumed_v);
        //EXACT POSITION
        let cur_cords = self.exact_position;
        //TILE BASED POSITION
        //self.wm.cords(&self.pos);
        //let (width, height) = (self.wm.size_x, self.wm.size_y);

        // TODO review
        //0° (x=1,y=0)-> map view: right
        // 90° (x=0,y=1) -> map view: up
        // 180° (x=-1,y=0) -> map view: left
        // 270° (x=0,y=-1) -> map view: down
        let (y_fac, x_fac) = self.angle.to_radians().sin_cos();
        //dbg!(self.angle,x_fac,y_fac);

        // a = 0, v = self.velo
        // s = t*v
        let time_f: OrdF = time_d.as_secs_f64().into();
        let mut end_velo = self.velo + a * time_f;
        let time = if end_velo < OrdF::from(0f64) {
            let halt_time = self.velo / (a * OrdF::from(-1.0f64));
            end_velo = 0f64.into();
            halt_time
        } else {
            time_f
        };
        //let acc_distance: OrdF = std::cmp::max(OrdF::from(0.5f64) * a * time.pow(2),OrdF::from(0.0f64));
        let acc_distance: OrdF = OrdF::from(0.5f64) * a * time.pow(2);
        let distance: OrdF = self.velo * time + acc_distance;
        //dbg!(acc_distance,distance,self.velo, time, a);

        let (x_dis, y_dis) = (distance * x_fac, distance * y_fac);
        let (x_rel_dis, y_rel_dis) = (x_dis, y_dis);

        let (cur_x, cur_y): (OrdF, OrdF) = cur_cords.into();

        //dbg!(cur_x, x_rel_dis, cur_y,y_rel_dis);
        let step_count = 10usize * ((time_d.as_secs_f64().trunc() + 1.0) as usize); //TODO: time* constant
        let crossing_positions: Vec<Position> = (1..step_count)
            .map(|step| {
                let factor = step as f64 / (step_count as f64);
                let testing_cords: Coordinate =
                    (cur_x + x_rel_dis * factor, cur_y + y_rel_dis * factor).into();
                self.wm.position_from_cords(testing_cords)
            })
            .collect();
        //dbg!(&crossing_positions);
        let collisions: Vec<(usize, &Position)> = crossing_positions
            .iter()
            .enumerate()
            .filter(|(_, p)| self.wm.get_tile(p) == &TileType::Wall)
            .collect();
        //dbg!(&collisions);
        let new_pos = if let Some((idx, _)) = collisions.first() {
            let coll_idx = if *idx == 0 {0} else {*idx - 1};
            //dbg!(idx, &self.pos, &self.exact_position);
            let prev = if *idx == 0 {self.pos.clone() } else {crossing_positions[coll_idx].clone()};
            prev
        } else {
            crossing_positions.last().unwrap().clone()
        };

        //dbg!(&new_pos);
        let res_flag = !collisions.is_empty();
        //dbg!(res_flag);
        self.velo = crate::round(end_velo);
        self.velo = std::cmp::max(OrdF::from(0.0f64), self.velo);
        //dbg!(self.velo);
        self.pos = new_pos.to_owned();
        self.exact_position = if collisions.is_empty() {
            (cur_x + x_rel_dis, cur_y + y_rel_dis).into()
        } else {
            self.wm.cords(&self.pos)
        };
        println!(
            "{}",
            format!("Moved by {:.3}, from V := {:.3}", distance, starting_v).green()
        );
        self.emit();
        res_flag
    }

    pub(crate) fn move_by_distance(&mut self, d: OrdF) -> bool {
        let zero: OrdF = 0f64.into();
        assert!(self.velo == zero);
        let cur_cords = self.wm.cords(&self.pos);

        let (width, height) = (self.wm.size_x, self.wm.size_y);
        let (y_fac, x_fac) = self.angle.sin_cos();

        let (x_dis, y_dis) = (d * x_fac, d * y_fac);
        let (x_rel_dis, y_rel_dis) = (x_dis / width, y_dis / height);

        let (cur_x, cur_y): (OrdF, OrdF) = cur_cords.into();

        let testing_cords: Coordinate = (cur_x + x_rel_dis, cur_y + y_rel_dis).into();
        let new_pos = self.wm.position_from_cords(testing_cords);
        self.pos = new_pos;
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn angle_test() {
        let angle: OrdF = 00.0.into();
        let (y_fac, x_fac) = angle.sin_cos();
        assert!((y_fac * y_fac + x_fac * x_fac) - 1.0 < 0.0005);
        dbg!(y_fac, x_fac);
        assert_eq!(x_fac, 1.0f64);
    }

    #[test]
    fn angle_test2() {
        let angle = 180.0f64.to_radians();
        let (y_fac, x_fac) = angle.sin_cos();
        assert!((y_fac * y_fac + x_fac * x_fac) - 1.0 < 0.0005);
        dbg!(y_fac, x_fac);
        assert_eq!(x_fac, -1.0f64);
    }

    #[test]
    fn angle_test3() {
        let angle = std::f64::consts::PI;
        let (y_fac, x_fac) = angle.sin_cos();
        assert!((y_fac * y_fac + x_fac * x_fac) - 1.0 < 0.0005);
        dbg!(y_fac, x_fac);
        assert_eq!(x_fac, -1.0f64);
    }
}
