use std::cell::RefCell;

use rand::{prelude::ThreadRng, Rng};

use crate::hybridautomaton::basics::{Atom, Scalar};

pub trait Selector {
    fn select(&self, i: Scalar) -> Atom;
    fn select_all(&self, i: &[Scalar]) -> Vec<Atom> {
        i.into_iter().map(|s| self.select(*s)).collect()
    }
}

pub(crate) struct RandomSelector {
    rng: RefCell<ThreadRng>,
}

impl RandomSelector {
    pub(crate) fn new() -> Self {
        RandomSelector {
            rng: RefCell::new(rand::thread_rng()),
        }
    }
}

impl Selector for RandomSelector {
    fn select(&self, i: Scalar) -> Atom {
        if i.is_singular() {
            i.lower()
        } else {
            let f = self
                .rng
                .borrow_mut()
                .gen_range(i.lower().into_inner(), i.upper().into_inner());
            Atom::from(f)
        }
    }
}

pub(crate) struct MaxSelector {}

impl Selector for MaxSelector {
    fn select(&self, i: Scalar) -> Atom {
        i.upper()
    }
}

pub(crate) struct MinSelector {}

impl Selector for MinSelector {
    fn select(&self, i: Scalar) -> Atom {
        i.lower()
    }
}

pub(crate) struct ExtremaSelector {
    rng: RefCell<ThreadRng>,
}

impl ExtremaSelector {
    pub(crate) fn new() -> Self {
        ExtremaSelector {
            rng: RefCell::new(rand::thread_rng()),
        }
    }
}

impl Selector for ExtremaSelector {
    fn select(&self, i: Scalar) -> Atom {
        let b = self.rng.borrow_mut().gen_range(0f64, 1000f64) < 50.0;
        if b {
            i.lower()
        } else {
            i.upper()
        }
    }
}
