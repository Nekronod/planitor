use std::time::Duration;

use num::Zero;

use super::selector::Selector;
use crate::hybridautomaton::AnnotatedHA;
use crate::world_model::abstract_planning::domain_handler::DomainHandler;
use crate::world_model::ha_sys_planer::Maneuver;
use crate::world_model::WorldModel;
use crate::{
    hybridautomaton::{
        basics::{Atom, Scalar, VariableAssignment},
        timed_trace::ActionId,
        LocationId,
    },
    monitor::{Monitor, MonitoringError, Plan, PlanStep, SystemReading, SystemValues},
    world_model::wm_simulator::WMSimulator,
    OrdF,
};

#[derive(Debug, Clone)]
pub(crate) struct HaPlan {
    pub steps: Vec<HaPlanStep>,
    pub is_fall_back: bool,
}

#[derive(Debug, Clone)]
pub(crate) struct HaPlanStep {
    pub(crate) act_id: ActionId,
    pub(crate) condition: Vec<(usize, Scalar)>,
    pub(crate) name: String,
    pub(crate) time: Option<Duration>,
}

pub(crate) struct HaSystem<'a, S>
where
    S: Selector,
{
    // Ha System
    aha: &'a AnnotatedHA,
    cur_loc: LocationId,
    state: VariableAssignment,
    selector: &'a S,
    // WM Simulator
    pub(crate) environment: WMSimulator<'a>,
    angle: OrdF,
    velocity: OrdF,
}

impl<'a, S: Selector> HaSystem<'a, S> {
    pub fn new(
        ha: &'a AnnotatedHA,
        wm: &'a WorldModel,
        selector: &'a S,
        initial_state: Vec<Atom>,
    ) -> Self {
        let environment = WMSimulator::new(wm);

        Self {
            cur_loc: ha.initial_location(),
            aha: ha,
            state: initial_state,
            selector,
            environment,
            angle: 0f64.into(),
            velocity: 0f64.into(),
        }
    }

    /*
    fn perform_maneuver(&mut self, m: Maneuver) -> Result<Vec<SystemReading>, String> {
        match m {
            Maneuver::TurnLeft(a) => {
                assert!(self.velocity.is_zero());
                self.environment.turn_by_angle(-a);
            }
            Maneuver::TurnRight(a) => {
                assert!(self.velocity.is_zero());
                self.environment.turn_by_angle(a);
            }
            Maneuver::Stop(_) => todo!(),
            Maneuver::DriveAndStop(d) => {
                self.environment.move_by_distance(d);
                //dbg!(&self.environment.pos);
            }
            Maneuver::Acc { v, time } => todo!(),
            Maneuver::Drive(_) => todo!(),
        }

        let (x, y) = self.environment.wm.cords(&self.environment.pos).into();
        let input_stream_values = vec![
            SystemReading::Float(0f64.into()), //v
            SystemReading::Float(0f64.into()), // omega
            SystemReading::Bool(false),        // collision
            SystemReading::Float(x),           //x
            SystemReading::Float(y),           //y
            SystemReading::Float(1f64.into()), //time
            SystemReading::UInt(0),            //actionID
        ];

        Ok(input_stream_values)
    }
     */

    pub(crate) fn run_maneuvers<D: DomainHandler>(
        &mut self,
        init_plan: Vec<Maneuver>,
        monitor: &mut Monitor<D>,
    ) -> Result<(), String> {
        let transformed_plan =
            HaPlan::construct(init_plan, self.aha, self.aha.initial_location(), self.angle);
        self.run(transformed_plan, monitor)
    }

    pub fn run<D: DomainHandler>(
        &mut self,
        init_plan: HaPlan,
        monitor: &mut Monitor<D>,
    ) -> Result<(), String> {
        let time_dim = monitor.specification_stream_names["time"];
        let mut cur_plan = init_plan;
        let mut cur_plan_complete = false;
        let mut broke = false;
        loop {
            //dbg!(&cur_plan);
            for step in cur_plan.steps.iter() {
                match self.execute_instruction(step.clone()) {
                    Ok(values) => {
                        let time_stamp = match values[time_dim] {
                            SystemReading::Float(t) => t,
                            _ => return Err("invalid time type".to_string()),
                        };
                        let feedback = monitor.accept_input(time_stamp, values);
                        if let Err(error) = feedback {
                            match error {
                                MonitoringError::BackUpPlan(plan) => {
                                    //use the new plan
                                    //dbg!("using new plan");
                                    //dbg!("using new plan");
                                    //dbg!("using new plan");
                                    //dbg!("using new plan");
                                    //dbg!(self.cur_loc, &plan);
                                    cur_plan = plan;
                                    // abort current plan
                                    broke = true;
                                    break;
                                }
                                _ => {
                                    return Err(error.to_string());
                                }
                            };
                        }
                    }
                    Err(msg) => {
                        return Err(format!(
                            "Internal System execution (simulation) Error: {}",
                            msg
                        ))
                    }
                }
            }
            if !broke {
                cur_plan_complete = true;
            }
            if cur_plan_complete && cur_plan.is_fall_back {
                let res = monitor.update();
                match res {
                    Err(err) => {
                        return Err(err.to_string());
                    }
                    Ok(plan) => {
                        cur_plan = plan;
                    }
                }
            } else if cur_plan_complete && !cur_plan.is_fall_back {
                let next_plan = monitor.next_plan();
                match next_plan {
                    Err(err) => {
                        return Err(err.to_string());
                    }
                    Ok(plan) if !plan.is_empty() => {
                        cur_plan = plan;
                    }
                    Ok(_empty_plan) => {
                        break;
                    }
                }
            }
            broke = false;
            cur_plan_complete = false;
        }
        Ok(())
    }

    /// execute time based instruction:
    /// e.g.: take transition X and wait 5sec;
    /*
    fn execute_instruction(&mut self, ins: HaInstruction) -> Result<SystemValues, String> {
        //HA part fo the execution
        // Transform variables according to the delay time
        // Restrict values inside invariant and guard
        let loc = self.ha.get_location(self.cur_loc);
        let next_loc_id = if let Some(next_loc_id) = self
            .ha
            .successors_actions(self.cur_loc)
            .iter()
            .find(|(a_id, _)| *a_id == ins.id)
            .map(|t|t.1)
        {
            next_loc_id
        } else {
            return Err(format!(
                "Invalid action: {} for current state {}",
                ins.id, self.cur_loc
            ));
        };
        let dur = Atom::new(ins.time.as_secs_f64().into());

        // select dynamic value
        let selected_dynamic = self.selector.select_all(&loc.dynamics.0);
        // state change: dynamic*time
        let state_change = selected_dynamic
            .iter()
            .map(|d| *d * dur)
            .collect::<Vec<Atom>>();

        //add state change to current state
        let new_state = self
            .state
            .iter()
            .zip(state_change.into_iter())
            .map(|(&l, r)| Scalar::singular(l + r))
            .collect();
        let (restricted_by_inv, bound_inv) = self
            .ha
            .restrict_values_inside_bounds(&loc.invariant, &new_state);
        if bound_inv != loc.invariant.0{
            return Err(format!(
                "Internal Error: invariant not satisfiable",
            ));
        }
        let (restricted_by_guard, bound_guard) = self
            .ha
            .restrict_values_inside_bounds(
                &self.ha.get_edge_guard(self.cur_loc, ins.id, next_loc_id),
                &restricted_by_inv,
            );
        if bound_guard != self.ha.get_edge_guard(self.cur_loc, ins.id, next_loc_id).0{
            return Err(format!(
                "Internal Error: invariant not satisfiable",
            ));
        }

        let restricted_new_state = restricted_by_guard;
        if restricted_new_state.iter().any(|s| !s.is_singular()) {
            return Err(format!(
                "Internal Error: state contains non-singular values: {:?}",
                restricted_new_state
            ));
        } else {
            self.state = restricted_new_state
                .into_iter()
                .map(|s| s.upper())
                .collect();
        }
        self.cur_loc = next_loc_id;

        // World model simulation and input stream data generation

        let acc = selected_dynamic[self.dim_map["velocity"]];
        let angle_vel = selected_dynamic[self.dim_map["angel_vel"]];

        assert!(
            acc.is_zero() || angle_vel.is_zero(),
            "moving and turning not supported"
        );
        let collision = if !angle_vel.is_zero() {
            self.environment
                .turn(angle_vel.into_inner().into(), ins.time)
        } else {
            self.environment
                .acc((acc.into_inner() as f64).into(), ins.time)
        };
        let pos = &self.environment.wm.pos;
        let cords = self.environment.wm.cords(pos);
        let (x, y) = cords.into();
        let input_stream_values = vec![
            SystemReading::Float(acc.into_inner().into()), //v
            SystemReading::Float(angle_vel.into_inner().into()), // omega
            SystemReading::Bool(collision), // collision
            SystemReading::Float(x), //x
            SystemReading::Float(y), //y
            SystemReading::Int(ins.id.0 as i64), //actionID
        ];

        Ok(input_stream_values)
    }
    */

    fn execute_instruction(&mut self, step: HaPlanStep) -> Result<SystemValues, String> {
        //dbg!(&step);
        let cur_loc = self.aha.get_location(self.cur_loc);
        let selected_dyn = self.selector.select_all(&cur_loc.dynamics.0);

        //sanity check
        assert!(step.time.is_some() ^ !step.condition.is_empty());

        let time_needed = if let Some(dur) = step.time {
            Scalar::singular(Atom::from(dur.as_secs_f64()))
        } else {
            let (idx, target_range) = step.condition.first().expect("cant be empty");
            let start = Scalar::singular(self.state[*idx]);
            //dbg!(target_range, start);

            if (*target_range - start) != Scalar::singular(selected_dyn[*idx]) {
                //dbg!((*target_range - start) / Scalar::singular(selected_dyn[*idx]));
                (*target_range - start) / Scalar::singular(selected_dyn[*idx])
            } else {
                Scalar::singular(Atom::from(0.0))
            }
        };

        //select time: average
        let time = (time_needed.lower() + time_needed.upper()) / Atom::from(2.0);
        let loc = self.aha.get_location(self.cur_loc);
        let new_state: Vec<Atom> = self
            .state
            .iter()
            .zip(selected_dyn.iter().map(|d| *d * time))
            .map(|(s, c)| Atom::from(((*s + c).into_inner() * 10000.0).round() / 10000.0))
            .collect();

        //dbg!(time, &step);
        let time_d = Duration::from_secs_f64(time.into_inner());

        let acc = selected_dyn[self.aha.dim_names["vel"]];
        let angle_vel = selected_dyn[self.aha.dim_names["angle"]];

        let next_loc_id = if let Some(next_loc_id) = self
            .aha
            .ha
            .successors_actions(self.cur_loc)
            .iter()
            .find(|(a_id, _)| *a_id == step.act_id)
            .map(|t| t.1)
        {
            next_loc_id
        } else {
            return Err(format!(
                "Invalid action: {} for current state {}: {}",
                step.act_id,
                self.cur_loc,
                self.aha.get_location(self.cur_loc).name.clone()
            ));
        };

        let edge_guard = self
            .aha
            .ha
            .get_edge_guard(self.cur_loc, step.act_id, next_loc_id);
        //dbg!(edge_guard);
        //dbg!(&self.state);
        //dbg!(&new_state);
        if !edge_guard.is_satisfied_atom(&new_state) {
            return Err(format!("Internal Error: edge guard not satisfied",));
        }
        let invariant = &loc.invariant;
        if !invariant.is_satisfied_atom(&new_state) {
            return Err(format!("Internal Error: Loc-Invariant not satisfied",));
        }
        self.cur_loc = next_loc_id;

        assert!(
            acc.is_zero() || angle_vel.is_zero(),
            "moving and turning not supported - yet"
        );

        /*
        println!(
            "\n\nTarget Step Name: {}, {}",
            step.name,
            if step.time.is_some() {
                "timed condition"
            } else {
                "value condition"
            }
        );
         */

        let collision = if !angle_vel.is_zero() {
            //dbg!(angle_vel, time_d);
            self.environment.turn(angle_vel.into_inner().into(), time_d)
        } else {
            self.environment
                .acc((acc.into_inner() as f64).into(), time_d)
        };

        let pos = &self.environment.pos;
        //dbg!(&pos);
        let cords = self.environment.wm.cords(pos);
        let (x, y) = cords.into();
        self.state = new_state;
        self.angle = self.environment.angle;
        if self.angle < OrdF::from(-5.0) {
            //TODO: set angle to positive number
            //dbg!(self.angle);
            //panic!()
        }
        self.velocity = self.environment.velo;
        let time_f: OrdF = time_d.as_secs_f64().into();
        let input_stream_values = vec![
            SystemReading::Float(self.environment.velo),         //v
            SystemReading::Float(self.environment.angle.into()), //angle
            SystemReading::Float(angle_vel.into_inner().into()), //omega
            SystemReading::Float(x),                             //x
            SystemReading::Float(y),                             //y
            SystemReading::Float(time_f),                        //time
            SystemReading::UInt(step.act_id.0 as u64),           //actionID
            SystemReading::Bool(collision),                      // collision
        ];

        Ok(input_stream_values)
    }

    fn report_to_monitor(&self) -> Result<(), String> {
        todo!()
    }
}

impl Plan for HaPlan {
    fn instruction(&self) -> Vec<PlanStep> {
        self.steps.iter().cloned().map(|hs| hs.into()).collect()
    }

    fn is_fall_back(&self) -> bool {
        self.is_fall_back
    }
}

impl Into<PlanStep> for HaPlanStep {
    fn into(self) -> PlanStep {
        let HaPlanStep {
            act_id,
            condition,
            name,
            time,
        } = self;
        PlanStep {
            id: act_id.0,
            condition,
            name,
            time,
        }
    }
}
