use crate::hybridautomaton::{
    basics::{Atom, Rectangle, Scalar, VariableAssignment},
    {ConcreteDynamics, Dynamics, LocationId},
    ActionId, EdgeRef, simulator::Simulator, TimedTrace,
};
use super::timed_trace::Step;
use gcollections::ops::Intersection;
use num::Zero;
use rand::{prelude::ThreadRng, Rng};
use std::fmt::Debug;

pub(crate) trait Walker: Clone + Debug {
    fn commit(&mut self, step: Step, loc: LocationId, state: VariableAssignment);
    fn finish(self, last_delay: f64, end_state: VariableAssignment) -> TimedTrace;
    fn location(&self) -> LocationId;
    fn state(&self) -> &VariableAssignment;
    fn choose_dynamics(&mut self, dynamics: &Dynamics) -> ConcreteDynamics;
    fn select_transition<'a, 'b>(
        &'a mut self,
        enabled: &[(EdgeRef<'b, LocationId>, Scalar)],
        dynamics: &ConcreteDynamics,
    ) -> (EdgeRef<'b, LocationId>, Atom);
    fn select_final_delay(&mut self) -> Atom;

    fn concretize<F>(dynamics: &Dynamics, mut conc: F) -> ConcreteDynamics
    where
        F: FnMut(usize, &Scalar) -> Atom,
    {
        dynamics.0.iter().enumerate().map(|(dim, slope)| conc(dim, slope)).collect::<Vec<Atom>>().into()
    }
}

#[derive(Debug, Clone)]
pub(crate) struct RandomWalk {
    steps: Vec<Step>,
    initial: VariableAssignment,
    rng: ThreadRng,
    loc: LocationId,
    s: VariableAssignment,
}

impl Walker for RandomWalk {
    fn commit(&mut self, step: Step, loc: LocationId, state: VariableAssignment) {
        self.steps.push(step);
        self.loc = loc;
        self.s = state;
    }

    fn finish(self, last_delay: f64, end_state: VariableAssignment) -> TimedTrace {
        TimedTrace { initial: self.initial, end_state, steps: self.steps, last_delay: Atom::from(last_delay) }
    }

    fn choose_dynamics(&mut self, dynamics: &Dynamics) -> ConcreteDynamics {
        Self::concretize(dynamics, |_, slope| {
            if slope.lower() == slope.upper() {
                slope.lower()
            } else {
                Atom::from(self.rng.gen_range(slope.lower().into_inner(), slope.upper().into_inner()))
            }
        })
    }

    fn location(&self) -> LocationId {
        self.loc
    }

    fn state(&self) -> &VariableAssignment {
        &self.s
    }

    fn select_transition<'a, 'b>(
        &'a mut self,
        enabled: &[(EdgeRef<'b, LocationId>, Scalar)],
        _dynamics: &ConcreteDynamics,
    ) -> (EdgeRef<'b, LocationId>, Atom) {
        let rnd_int = self.rng.gen_range(0, enabled.len());
        let (edge, ref interval) = enabled[rnd_int];
        let upper = if interval.upper().into_inner().is_infinite() {
            1000.0 // TODO: magic number
        } else {
            interval.upper().into_inner()
        };
        let delay = self.rng.gen_range(interval.lower().into_inner(), upper);
        (edge, Atom::from(delay))
    }

    fn select_final_delay(&mut self) -> Atom {
        Atom::from(self.rng.gen::<f64>().abs())
    }
}

impl RandomWalk {
    pub(crate) fn new(initial_state: VariableAssignment, initial_location: LocationId, length: usize) -> Self {
        RandomWalk {
            steps: Vec::with_capacity(length),
            initial: initial_state.clone(),
            rng: rand::thread_rng(),
            loc: initial_location,
            s: initial_state,
        }
    }
}

#[derive(Debug, Clone)]
pub(crate) struct ExtremaWalk {
    steps: Vec<Step>,
    initial: VariableAssignment,
    rng: ThreadRng,
    loc: LocationId,
    s: VariableAssignment,
}


impl Walker for ExtremaWalk {
    fn commit(&mut self, step: Step, loc: LocationId, state: VariableAssignment) {
        self.steps.push(step);
        self.loc = loc;
        self.s = state;
    }

    fn finish(self, last_delay: f64, end_state: VariableAssignment) -> TimedTrace {
        TimedTrace { initial: self.initial, end_state, steps: self.steps, last_delay: Atom::from(last_delay) }
    }

    fn choose_dynamics(&mut self, dynamics: &Dynamics) -> ConcreteDynamics {
        Self::concretize(dynamics, |_, slope| {
            if slope.lower() == slope.upper() {
                slope.lower()
            } else {
                let coin_flip = self.rng.gen_bool(0.5);
                if coin_flip {
                    Atom::from(slope.upper())
                } else {
                    Atom::from(slope.lower())
                }
                
            }
        })
    }

    fn location(&self) -> LocationId {
        self.loc
    }

    fn state(&self) -> &VariableAssignment {
        &self.s
    }

    fn select_transition<'a, 'b>(
        &'a mut self,
        enabled: &[(EdgeRef<'b, LocationId>, Scalar)],
        _dynamics: &ConcreteDynamics,
    ) -> (EdgeRef<'b, LocationId>, Atom) {
        let rnd_int = self.rng.gen_range(0, enabled.len());
        let (edge, ref interval) = enabled[rnd_int];
        let upper = if interval.upper().into_inner().is_infinite() {
            1000.0 // TODO: magic number
        } else {
            interval.upper().into_inner()
        };
        let delay = self.rng.gen_range(interval.lower().into_inner(), upper);
        (edge, Atom::from(delay))
    }

    fn select_final_delay(&mut self) -> Atom {
        Atom::from(self.rng.gen::<f64>().abs())
    }
}

impl ExtremaWalk {
    pub(crate) fn new(initial_state: VariableAssignment, initial_location: LocationId, length: usize) -> Self {
        ExtremaWalk {
            steps: Vec::with_capacity(length),
            initial: initial_state.clone(),
            rng: rand::thread_rng(),
            loc: initial_location,
            s: initial_state,
        }
    }
}


#[derive(Debug, Clone)]
pub(crate) struct AdequateWalk {
    steps: Vec<Step>,
    initial: VariableAssignment,
    loc: LocationId,
    s: VariableAssignment,
    path: Vec<PathStep>,
}

#[derive(Debug, Clone, Copy)]
pub(crate) enum Extreme {
    Max,
    Min,
}

#[derive(Debug, Clone, Copy)]
pub(crate) struct DynamicSelection {
    extreme: Extreme,
    sign: Sign,
}

#[derive(Debug, Clone, Copy)]
pub(crate) enum Sign {
    Positive,
    Negative,
    Zero,
    Global,
}

#[derive(Debug, Clone)]
pub(crate) struct PathStep {
    action: ActionId,
    target: LocationId,
    dynamics: Vec<DynamicSelection>,
    transition_rectangle: Rectangle,
}

impl PathStep {
    pub(crate) fn new(ac: ActionId, target: LocationId, extrema: Vec<Extreme>, intended_value_range: Rectangle ) -> Self {
        PathStep {
            action: ac,
            target,
            dynamics: extrema.into_iter().map(|e| DynamicSelection { extreme: e, sign: Sign::Global}).collect(),
            transition_rectangle: intended_value_range,
        }
    }
}


impl Walker for AdequateWalk {
    fn commit(&mut self, step: Step, loc: LocationId, state: VariableAssignment) {
        //dbg!(&step, loc);
        self.steps.push(step);
        self.loc = loc;
        self.s = state;
        if let Some(ps) = Some(self.path.remove(0)) {
            assert_eq!(ps.target, loc);
            assert!(ps.transition_rectangle.is_member(&self.s));
        } else {
            panic!("Path over but length not reached.")
        }
    }

    fn finish(self, last_delay: f64, end_state: VariableAssignment) -> TimedTrace {
        TimedTrace { initial: self.initial, end_state, steps: self.steps, last_delay: Atom::from(last_delay) }
    }

    fn choose_dynamics(&mut self, dynamics: &Dynamics) -> ConcreteDynamics {
        let step = self.path.first().unwrap();
        Self::concretize(dynamics, |dim: usize, slope: &Scalar| {
            self.dynamic_concretizer(slope, step.dynamics[dim].sign, step.dynamics[dim].extreme)
        })
    }

    fn location(&self) -> LocationId {
        self.loc
    }

    fn state(&self) -> &VariableAssignment {
        &self.s
    }

    fn select_transition<'a, 'b>(
        &'a mut self,
        enabled: &[(EdgeRef<'b, LocationId>, Scalar)],
        dynamics: &ConcreteDynamics,
    ) -> (EdgeRef<'b, LocationId>, Atom) {
        let step = self.path.first().unwrap();
        let edge_ref = enabled
            .iter()
            .find(|(er, _)| {
                let ((source, action, target), _) = er;
                assert_eq!(*source, self.loc);
                *action == step.action && *target == step.target
            })
            .map(|(er, _)| er)
            .expect("Desired path is unrealizable.");
        let rect = &step.transition_rectangle;
        //dbg!(&rect);
        let permitted = (0..self.state().len())
            .map(|dim| {
                let slope = dynamics.0[dim];
                let rect = &rect.0[dim];
                let state = self.state()[dim];
                //dbg!(&slope,&rect,&state);
                let fst_bound = Simulator::compute_delay(rect.lower(), slope, state, true);
                let snd_bound = Simulator::compute_delay(rect.upper(), slope, state, false);
                //dbg!(fst_bound,snd_bound);
                Scalar::range(fst_bound.min(snd_bound), fst_bound.max(snd_bound))
            })
            // .fold_first(Scalar::intersection);
            .fold(Scalar::full(), |s1, s2| s1.intersection(&s2));
        //dbg!(&permitted);
        let delay = if permitted.upper().into_inner().is_infinite() {
            Atom::from(rand::thread_rng().gen_range(0.1f64, 100.0f64))
        } else {
            permitted.upper() // TODO:  Random?
        };
        //dbg!(delay);
        (*edge_ref, delay)
    }

    fn select_final_delay(&mut self) -> Atom {
        <Atom as num::One>::one() // TODO: Random?
    }
}

impl AdequateWalk {
    pub(crate) fn new(initial_state: VariableAssignment, initial_location: LocationId, path: Vec<PathStep>) -> Self {
        AdequateWalk {
            steps: Vec::with_capacity(path.len()),
            initial: initial_state.clone(),
            loc: initial_location,
            s: initial_state,
            path,
        }
    }

    fn dynamic_concretizer(&self, slope: &Scalar, sign: Sign, extreme: Extreme) -> Atom {
        let (lower, upper) = match sign {
            Sign::Global => (slope.lower(), slope.upper()),
            Sign::Positive => {
                assert!(slope.upper().into_inner() > 0f64);
                let lower = f64::max(0f64, slope.lower().into_inner());
                (Atom::from(lower), slope.upper())
            }
            Sign::Negative => {
                assert!(slope.lower().into_inner() < 0f64);
                let upper = f64::min(0f64, slope.upper().into_inner());
                (slope.lower(), Atom::from(upper))
            }
            Sign::Zero => (Atom::zero(), Atom::zero()),
        };
        match extreme {
            Extreme::Max => upper,
            Extreme::Min => lower,
        }
    }
}

#[cfg(test)]
mod tests {
    use std::time::SystemTime;

    use num::Zero;

    use crate::hybridautomaton::{basics::{Atom, VariableAssignment, Scalar, Rectangle}, ha_parser::HAParser, timed_trace::{ActionId, TraceBuilder}, simulator::Simulator, Step, TimedTrace, LocationId};

    use super::{PathStep, Extreme};
    //use crate::learner::HALearner;
    //use crate::specification_automaton::SpecAutomatonBuilder;

    #[test]
    fn test_simulate_thermostat() {
        let hap = HAParser::new();
        let ha_str = "
        1 _

        l \"off\"
        d (-0.6 - -0.6)
        i (u-u)

        l \"on\"
        d (1.5-1.5)
        i (u-u)

        e \"on\" \"off\"
        a 1 \"turn_off\"
        g (22-24)


        e \"off\" \"on\"
        a 0 \"turn_on\"
        g (17-19)
        ".to_string();
        let mut aha = hap.annotated_ha(ha_str).expect("valid input");
        /*
        let mut builder = SpecAutomatonBuilder::empty();
        let off = builder.add_state("off".to_string());
        let on = builder.add_state("on".to_string());

        let turn_on = ActionId(0);
        let turn_off = ActionId(1);

        // Create guards.
        let turn_on_guard = Rectangle(vec![Scalar::range(Atom::from(17.0), Atom::from(19.0))]);
        let turn_off_guard = Rectangle(vec![Scalar::range(Atom::from(22.0), Atom::from(24.0))]);

        builder.connect(on, off, turn_off, turn_off_guard);
        builder.connect(off, on, turn_on, turn_on_guard);

        builder.declare_initial(off).expect("unexpected construction error");
         let spec_automaton = builder.build().expect("failed to create spec automaton");
         */
       
        let turn_on = ActionId(0);
        let turn_off = ActionId(1);

        // Create states of trace1.
        let initial: VariableAssignment = vec![Atom::from(20.0)];
        let state1: VariableAssignment = vec![Atom::from(18.5)];
        let state2: VariableAssignment = vec![Atom::from(23.0)];
        let state3: VariableAssignment = vec![Atom::from(19.0)];
        let state4: VariableAssignment = vec![Atom::from(22.5)];
        let end_state: VariableAssignment = vec![Atom::from(21.0)];

        // Create steps of trace1.
        let step1 = Step { delay: Atom::from(2.5), action: turn_on, state: state1 };
        let step2 = Step { delay: Atom::from(3.0), action: turn_off, state: state2 };
        let step3 = Step { delay: Atom::from(6.0), action: turn_on, state: state3 };
        let step4 = Step { delay: Atom::from(2.0), action: turn_off, state: state4 };
        let steps = vec![step1, step2, step3, step4];

        // Create trace1.
        let last_delay = Atom::from(2.0);
        let trace1 = TimedTrace { initial, end_state, steps, last_delay };

        // Create states of trace2.
        let initial: VariableAssignment = vec![Atom::from(20.0)];
        let state1: VariableAssignment = vec![Atom::from(18.0)];
        let state2: VariableAssignment = vec![Atom::from(22.5)];
        let state3: VariableAssignment = vec![Atom::from(20.0)];
        let state4: VariableAssignment = vec![Atom::from(22.5)];
        let end_state: VariableAssignment = vec![Atom::from(19.0)];

        // Create steps of trace2.
        let step1 = Step { delay: Atom::from(3.0), action: ActionId(0), state: state1 };
        let step2 = Step { delay: Atom::from(3.0), action: ActionId(1), state: state2 };
        let step3 = Step { delay: Atom::from(2.5), action: ActionId(0), state: state3 };
        let step4 = Step { delay: Atom::from(2.0), action: ActionId(1), state: state4 };
        let steps = vec![step1, step2, step3, step4];

        // Create trace2.
        let last_delay = Atom::from(4.0);
        let trace2 = TimedTrace { initial, end_state, steps, last_delay };

        // Create states of trace3.
        let initial: VariableAssignment = vec![Atom::from(20.0)];
        let state1: VariableAssignment = vec![Atom::from(17.0)];
        let state2: VariableAssignment = vec![Atom::from(22.0)];
        let state3: VariableAssignment = vec![Atom::from(19.0)];
        let state4: VariableAssignment = vec![Atom::from(23.0)];
        let end_state: VariableAssignment = vec![Atom::from(20.0)];

        // Create steps of trace3.
        let step1 = Step { delay: Atom::from(4.0), action: ActionId(0), state: state1 };
        let step2 = Step { delay: Atom::from(5.0), action: ActionId(1), state: state2 };
        let step3 = Step { delay: Atom::from(6.0), action: ActionId(0), state: state3 };
        let step4 = Step { delay: Atom::from(3.0), action: ActionId(1), state: state4 };
        let steps = vec![step1, step2, step3, step4];

        // Create trace3.
        let last_delay = Atom::from(5.0);
        let trace3 = TimedTrace { initial, end_state, steps, last_delay };

        // Learn!
        let mut tb = TraceBuilder::new(vec![Atom::from(20.0)],LocationId(0));


        let mut updates = vec![];
        for trace in [trace1,trace2, trace3] {
            trace.into_input_events().into_iter().for_each(|e| {
                tb.update_event(&aha.ha, e);
            });
            let mut u = tb.extract_updates();
            aha.ha.update_given_events(u.clone()).unwrap();
            dbg!(&u);
            updates.append(&mut u);
            tb.hard_reset();
        }

       

        let test_ha_string = HAParser::re_string_ha(&aha);
        //eprintln!("{}",test_ha_string);
        println!("{}",test_ha_string);
        /*
        ha.include_timed_trace(0.into(), trace1);
        dbg!(&ha);
        ha.include_timed_trace(0.into(), trace2);
        dbg!(&ha);
        ha.include_timed_trace(0.into(), trace3);
        dbg!(&ha);
         */

        //let ha_res = HALearner::learn(vec![trace1, trace2, trace3].into_iter(), spec_automaton, 1, 3, 100);
        //assert!(ha_res.is_ok());
        //let ha = ha_res.unwrap();

        let mut simulator = Simulator::new(&aha.ha);
        let init = vec![Atom::from(20.0)];
        let traces = simulator.random_walks(10, 3, init);

        dbg!(traces);
    }

    #[test]
    fn dummy_test() {
        assert!(true);
    }


    #[test]
    fn aircraft_test(){
        let sys_str = HAParser::aha_from_file("eval/aircraft.ha".to_string());
        let mut sample_str = HAParser::aha_from_file("eval/aircraft-sampled.ha".to_string());


        let mut simulator = Simulator::new(&sys_str.ha);
        let init = vec![Atom::zero();3];

        //dbg!(&sys_str.ha);

        //0,0,0 to straight - 10s
        let s1_rec = Rectangle(vec![Scalar::singular(Atom::from(1000.0f64)), Scalar::singular(Atom::from(0.0f64)), Scalar::singular(Atom::from(300.0f64))]);
        let s1 = PathStep::new(ActionId(0), LocationId(1), vec![Extreme::Max;3], s1_rec);


        //straight to left - 1s
        let s2_rec = Rectangle(vec![Scalar::singular(Atom::from(1300.0f64)), Scalar::singular(Atom::from(0.0f64)), Scalar::singular(Atom::from(298.0f64))]);
        let s2 = PathStep::new(ActionId(1), LocationId(2), vec![Extreme::Max,Extreme::Max,Extreme::Min], s2_rec);

        //left tto straight - 1s
        let s3_rec = Rectangle(vec![Scalar::singular(Atom::from(1390.0f64)), Scalar::singular(Atom::from(-150.0f64)), Scalar::singular(Atom::from(300.0f64))]);
        let s3 = PathStep::new(ActionId(3), LocationId(1), vec![Extreme::Min,Extreme::Min,Extreme::Max], s3_rec);

        //straight to right -1s
        let s4_rec = Rectangle(vec![Scalar::singular(Atom::from(1480.0f64)), Scalar::singular(Atom::from(-150.0f64)), Scalar::singular(Atom::from(302.0f64))]);
        let s4 = PathStep::new(ActionId(2), LocationId(3), vec![Extreme::Min,Extreme::Min,Extreme::Max], s4_rec);

        //right to straight - 0.5s
        let s5_rec = Rectangle(vec![Scalar::singular(Atom::from(1525.0f64)), Scalar::singular(Atom::from(-150.0f64)), Scalar::singular(Atom::from(303.0f64))]);
        let s5 = PathStep::new(ActionId(4), LocationId(1), vec![Extreme::Min,Extreme::Min,Extreme::Max], s5_rec);

        //straight to land - 0s
        let s6_rec = Rectangle(vec![Scalar::singular(Atom::from(1525.0f64)), Scalar::singular(Atom::from(-150.0f64)), Scalar::singular(Atom::from(303.0f64))]);
        let s6 = PathStep::new(ActionId(5), LocationId(4), vec![Extreme::Min,Extreme::Min,Extreme::Max], s6_rec);
        
        //land  last delay - 1s
        let s7_rec = Rectangle(vec![Scalar::singular(Atom::from(1725.0f64)), Scalar::singular(Atom::from(-150.0f64)), Scalar::singular(Atom::from(278.0f64))]);
        let s7 = PathStep::new(ActionId(6), LocationId(4), vec![Extreme::Max,Extreme::Max,Extreme::Max], s7_rec);
        

        let steps = vec![s1,s2,s3,s4,s5,s6,s7];


        let mut traces = simulator.adequate_walks(steps, 1, init.clone());


        {   //build trace 2
            //0,0,0 to straight - 300s
        let s1_rec = Rectangle(vec![Scalar::singular(Atom::from(300.0f64)), Scalar::singular(Atom::from(0.0f64)), Scalar::singular(Atom::from(300.0f64))]);
        let s1 = PathStep::new(ActionId(0), LocationId(1), vec![Extreme::Min,Extreme::Max,Extreme::Min], s1_rec);

        //straight to right - 1s
        let s2_rec = Rectangle(vec![Scalar::singular(Atom::from(600.0f64)), Scalar::singular(Atom::from(0.0f64)), Scalar::singular(Atom::from(302.0f64))]);
        let s2 = PathStep::new(ActionId(2), LocationId(3), vec![Extreme::Max,Extreme::Min,Extreme::Max], s2_rec);

        //right to straight - 5s
        let s3_rec = Rectangle(vec![Scalar::singular(Atom::from(2100.0f64)), Scalar::singular(Atom::from(750.0f64)), Scalar::singular(Atom::from(292.0f64))]);
        let s3 = PathStep::new(ActionId(4), LocationId(1), vec![Extreme::Max,Extreme::Max,Extreme::Min], s3_rec);
        
        //straight to left -0s
        let s4_rec = Rectangle(vec![Scalar::singular(Atom::from(2100.0f64)), Scalar::singular(Atom::from(750.0f64)), Scalar::singular(Atom::from(292.0f64))]);
        let s4 = PathStep::new(ActionId(1), LocationId(2), vec![Extreme::Min,Extreme::Min,Extreme::Max], s4_rec);

        //left to straight - 2s
        let s5_rec = Rectangle(vec![Scalar::singular(Atom::from(2700.0f64)), Scalar::singular(Atom::from(750.0f64)), Scalar::singular(Atom::from(288.0f64))]);
        let s5 = PathStep::new(ActionId(3), LocationId(1), vec![Extreme::Max,Extreme::Max,Extreme::Min], s5_rec);

        //straight to land - 0s
        let s6_rec =Rectangle(vec![Scalar::singular(Atom::from(2700.0f64)), Scalar::singular(Atom::from(750.0f64)), Scalar::singular(Atom::from(288.0f64))]);
        let s6 = PathStep::new(ActionId(5), LocationId(4), vec![Extreme::Min,Extreme::Min,Extreme::Max], s6_rec);
        
        //land - land  - 0s
        let s7_rec = Rectangle(vec![Scalar::singular(Atom::from(2700.0f64)), Scalar::singular(Atom::from(750.0f64)), Scalar::singular(Atom::from(288.0f64))]);
        let s7 = PathStep::new(ActionId(6), LocationId(4), vec![Extreme::Min,Extreme::Max,Extreme::Max], s7_rec);
        
        //land  last delay - 1s
        let s8_rec = Rectangle(vec![Scalar::singular(Atom::from(2700.0f64)), Scalar::singular(Atom::from(750.0f64)), Scalar::singular(Atom::from(288.0f64))]);
        let s8 = PathStep::new(ActionId(6), LocationId(4), vec![Extreme::Min,Extreme::Max,Extreme::Max], s8_rec);
        

        let steps = vec![s1,s2,s3,s4,s5,s6,s7,s8];


        let traces2 = simulator.adequate_walks(steps, 1, init);
        assert_eq!(traces2.len(),1);
        let mut trace2 = traces2[0].clone();
        //assert_eq!(trace2.last_delay, Atom::zero());
        trace2.last_delay = <Atom as num::One>::one();

        //traces.clear();
        traces.push(trace2);
        assert_eq!(traces.len(),2);

        }


        //dbg!(&traces);

        let mut tb = TraceBuilder::new(vec![Atom::zero();3],LocationId(0));


        let mut updates = vec![];
        let event_trace= traces.into_iter().map(|t|{t.into_input_events().into_iter().collect::<Vec<_>>()}).collect::<Vec<_>>();
        //dbg!(&event_trace[1]);
        let time_start = SystemTime::now();

        for et in event_trace {
            et.into_iter().for_each(|e| {
                tb.update_event(&sample_str.ha, e);
            });
            let mut u = tb.extract_updates();
            sample_str.ha.update_given_events(u.clone()).unwrap();
            //dbg!(&u);
            updates.append(&mut u);
            tb.hard_reset();
            
        }

        let time_after = SystemTime::now();
        let time_all_traces = time_after
            .duration_since(time_start)
            .expect("Time went backwards");

        eprintln!("Time for all traces: {}s", time_all_traces.as_secs());
        eprintln!("Time for all traces: {}ms", time_all_traces.as_millis());
        let _test_ha_string = HAParser::re_string_ha(&sample_str);
        //eprintln!("{}",test_ha_string);
        //println!("{}",_test_ha_string);
    }
}