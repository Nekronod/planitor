use std::{collections::HashMap, iter::Peekable};

use super::{
    basics::Atom, basics::Rectangle, basics::Scalar, timed_trace::ActionId, AnnotatedHA, Dynamics,
    HAConstructionError, Location, LocationId, HA,
};

// 3
// l "name1"
// d (-1 - 1.6) 1.0 (2.0 - 5.0)
// i (0 - 10) (10 - 100) (20 - u)

// l "name2"
// d (1.4 - 1.6) 1.0 (2.0 - 5.0)
// i (0-10) (10-100) (20-u)

// e "name1" "name2"
// a 0 break
// g 5 50 (40-60)

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum Token {
    Number(f64),
    Name(String), // in quotes, allows numbers, e.q.: "edge3"
    Word(String), // no quotes, no numbers, e.g.: u
    Loc,
    Edge,
    Dyn,
    Action,
    Inv,
    LPar,
    RPar,
    Dash,
    Underscore,
}

#[derive(Debug, Clone)]
pub(crate) enum ParseError {
    TokenError(String),
    MissingDim,
    MissingNumber,
    MissingName,
    MissingToken,
    UnexpectedToken(Token),
    ExpectedAorL,
    ConstructionError(HAConstructionError),
}

type Action = (ActionId, String);
type Edge = ((String, Action, String), Rectangle);

pub struct HAParser {
    next_loc_id: usize,
    dim: usize,
    names: Vec<String>,
}

impl HAParser {
    pub fn new() -> Self {
        HAParser {
            next_loc_id: 0,
            dim: 0,
            names: vec![],
        }
    }

    pub fn extract_names(self) -> Vec<String> {
        self.names
    }

    pub fn names(&self) -> &[String] {
        &self.names
    }

    pub(crate) fn annotated_ha(mut self, input: String) -> Result<AnnotatedHA, ParseError> {
        let ha = self.parse(input)?;
        let dim_names = self
            .extract_names()
            .into_iter()
            .enumerate()
            .map(|(ix, n)| (n, ix))
            .collect();
        let state_names: HashMap<_, _> = ha
            .locations
            .values()
            .map(|loc| (loc.name.clone(), loc.id))
            .collect();

        let mut idle_states = vec![];
        if let Some(id) = state_names.get("idle") {
            idle_states.push(*id);
        }
        if let Some(id) = state_names.get("operating_idle") {
            idle_states.push(*id);
        }
        if let Some(id) = state_names.get("offline") {
            idle_states.push(*id);
        }
        if let Some(id) = state_names.get("hover") {
            idle_states.push(*id);
        }

        Ok(AnnotatedHA {
            ha,
            dim_names,
            idle_states,
            state_map: state_names,
        })
    }

    fn tokenize(&self, input: String) -> Result<Vec<Token>, ParseError> {
        let mut res = vec![];
        let mut it = input.chars().peekable();
        while let Some(&c) = it.peek() {
            match c {
                ' ' | '\n' | '\r' | '\t' => {
                    //skip whitespaces
                    it.next();
                }
                '(' | '{' | '[' => {
                    it.next();
                    res.push(Token::LPar);
                }
                ')' | ']' | '}' => {
                    it.next();
                    res.push(Token::RPar);
                }
                '\"' => {
                    it.next();
                    res.push(self.lex_name(&mut it)?);
                    it.next();
                }
                '-' => {
                    it.next();
                    res.push(Token::Dash);
                }
                '_' => {
                    it.next();
                    res.push(Token::Underscore);
                }
                '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '.' => {
                    it.next();
                    let n = self.lex_number(c, &mut it);
                    res.push(Token::Number(n))
                }
                c if c.is_alphabetic() => {
                    it.next();
                    res.push(self.lex_word(c, &mut it)?);
                }
                c => {
                    return Err(ParseError::TokenError(format!(
                        "Token Error: Found unexpected {}",
                        c
                    )));
                }
            }
        }

        Ok(res)
    }

    fn lex_number<T: Iterator<Item = char>>(&self, c: char, iter: &mut Peekable<T>) -> f64 {
        let mut num_str = c.to_string();
        loop {
            if let Some(&c) = iter.peek() {
                if c.is_numeric() || c == '.' {
                    num_str.extend(&[c]);
                    iter.next();
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        num_str.parse().expect("input")
    }

    fn lex_name<T: Iterator<Item = char>>(
        &self,
        iter: &mut Peekable<T>,
    ) -> Result<Token, ParseError> {
        let mut name_str = "".to_string();
        loop {
            if let Some(&c) = iter.peek() {
                if !(c == '\"' || c.is_whitespace()) {
                    name_str.extend(&[c]);
                    iter.next();
                } else if c.is_whitespace() {
                    return Err(ParseError::TokenError(format!(
                        "Found Whitespace:{} during name lexing",
                        c
                    )));
                } else {
                    // c == )
                    iter.next();
                    break;
                }
            } else {
                return Err(ParseError::TokenError(format!("Unclosed Parenthesis")));
            }
        }
        Ok(Token::Name(name_str))
    }

    fn lex_word<T: Iterator<Item = char>>(
        &self,
        c: char,
        iter: &mut Peekable<T>,
    ) -> Result<Token, ParseError> {
        let mut word_str = c.to_string();
        loop {
            if let Some(&c) = iter.peek() {
                if c.is_alphabetic() {
                    word_str.extend(&[c]);
                    iter.next();
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        let t = if word_str.eq("location") || word_str.eq("loc") || word_str.eq("l") {
            Token::Loc
        } else if word_str.eq("edge") || word_str.eq("e") {
            Token::Edge
        } else if word_str.eq("dynamic") || word_str.eq("dyn") || word_str.eq("d") {
            Token::Dyn
        } else if word_str.eq("action") || word_str.eq("ac") || word_str.eq("a") {
            Token::Action
        } else if word_str.eq("invariant")
            || word_str.eq("inv")
            || word_str.eq("i")
            || word_str.eq("g")
            || word_str.eq("guard")
        {
            Token::Inv
        } else {
            Token::Word(word_str)
        };

        Ok(t)
    }

    pub(crate) fn parse(&mut self, input: String) -> Result<HA, ParseError> {
        let tokens = self.tokenize(input)?;
        let mut it = tokens.into_iter().peekable();

        // get #dim
        self.dim = if let Some(Token::Number(d)) = it.peek() {
            if d.trunc() != *d {
                return Err(ParseError::TokenError(format!(
                    "Got {}, expected non float value for dimension",
                    d
                )));
            } else {
                *d as usize
            }
        } else {
            return Err(ParseError::MissingDim);
        };
        it.next();

        let mut names = Vec::new();
        for x in 0..self.dim {
            if let Some(t) = it.peek() {
                match t {
                    Token::Name(n) => names.push(n.clone()),
                    Token::Underscore => names.push("".to_string()),
                    _ => {
                        return Err(ParseError::TokenError(format!(
                            "Missing Dimension Name, got only {}, expected {}, found {:?} instead",
                            x, self.dim, t
                        )));
                    }
                }
            } else {
                return Err(ParseError::TokenError(format!(
                    "Missing Dimension Name, got only {}, expected {}",
                    x, self.dim
                )));
            }
            it.next();
        }
        self.names = names;

        let mut locs = vec![];
        let mut edges = vec![];
        while let Some(t) = it.peek() {
            match t {
                Token::Loc => {
                    it.next();
                    let loc = self.parse_loc(&mut it)?;
                    locs.push((LocationId(self.next_loc_id), loc));
                    self.next_loc_id += 1;
                }
                Token::Edge => {
                    it.next();
                    let edge = self.parse_edge(&mut it)?;
                    edges.push(edge);
                }
                _ => {
                    return Err(ParseError::ExpectedAorL);
                }
            }
        }
        let mut action_map = vec![];
        let edges = edges
            .into_iter()
            .map(|((from, a, to), inv)| {
                let (from_id, _) = locs.iter().find(|(_, loc)| loc.name == from).unwrap();
                let (to_id, _) = locs.iter().find(|(_, loc)| loc.name == to).unwrap();
                let edge = (*from_id, a.0, *to_id);
                action_map.push(a);
                (edge, inv)
            })
            .collect::<HashMap<_, _>>();

        let loc_map = locs.into_iter().collect::<HashMap<_, _>>();
        Ok(HA::new(loc_map, edges, LocationId(0)).map_err(ParseError::ConstructionError)?)
    }

    /// l "name2"
    /// d 1.4-1.6 1.0 2.0-5.0
    /// i 0-10 10-100 20-u
    fn parse_loc<T: Iterator<Item = Token>>(
        &self,
        iter: &mut Peekable<T>,
    ) -> Result<Location, ParseError> {
        let name = if let Some(Token::Name(n)) = iter.peek() {
            let n = n.clone();
            iter.next();
            n
        } else {
            return Err(ParseError::MissingName);
        };
        let mut d = None;
        let mut i = None;
        for _ in 0..2 {
            let peeked = iter.peek().clone();
            match peeked {
                Some(Token::Dyn) => {
                    iter.next();
                    //parse dyn
                    let vec_scalar = self.parse_rec(iter)?;
                    d = Some(vec_scalar);
                }
                Some(Token::Inv) => {
                    iter.next();
                    //parse invariant
                    let vec_scalar = self.parse_rec(iter)?;
                    i = Some(vec_scalar);
                }
                Some(t) => {
                    return Err(ParseError::UnexpectedToken(t.clone()));
                }
                None => {
                    return Err(ParseError::MissingToken);
                }
            }
        }
        match (d, i) {
            (Some(d), Some(i)) => Ok(Location {
                name,
                id: LocationId(self.next_loc_id),
                dynamics: Dynamics(d),
                invariant: Rectangle(i),
            }),
            _ => {
                return Err(ParseError::MissingToken);
            }
        }
    }

    /// e "name1" "name2"
    /// a 0 break
    /// g 5 50 (40-60)
    fn parse_edge<T: Iterator<Item = Token>>(
        &self,
        iter: &mut Peekable<T>,
    ) -> Result<Edge, ParseError> {
        let from = if let Some(Token::Name(n)) = iter.peek() {
            let n = n.clone();
            iter.next();
            n
        } else {
            return Err(ParseError::MissingName);
        };

        let to = if let Some(Token::Name(n)) = iter.peek() {
            let n = n.clone();
            iter.next();
            n
        } else {
            return Err(ParseError::MissingName);
        };

        if let Some(Token::Action) = iter.peek() {
            iter.next();
        } else {
            return Err(ParseError::UnexpectedToken(iter.peek().unwrap().clone()));
        }

        let actid = if let Some(Token::Number(d)) = iter.peek() {
            if d.trunc() != *d {
                return Err(ParseError::TokenError(format!(
                    "Got {}, expected non float value for ActionID",
                    d
                )));
            } else {
                *d as usize
            }
        } else {
            return Err(ParseError::MissingNumber);
        };
        iter.next();

        let actionname = if let Some(Token::Name(n)) = iter.peek() {
            let n = n.clone();
            iter.next();
            n
        } else {
            return Err(ParseError::MissingName);
        };

        let inv = if let Some(Token::Inv) = iter.peek() {
            iter.next();
            //parse invariant
            let vec_scalar = self.parse_rec(iter)?;
            vec_scalar
        } else {
            return Err(ParseError::UnexpectedToken(iter.peek().unwrap().clone()));
        };

        let action = (ActionId(actid), actionname);
        let edge = ((from, action, to), Rectangle(inv));

        Ok(edge)
    }

    fn parse_rec<T: Iterator<Item = Token>>(
        &self,
        iter: &mut Peekable<T>,
    ) -> Result<Vec<Scalar>, ParseError> {
        let mut v = vec![];
        for _ in 0..self.dim {
            let s = match iter.peek() {
                Some(Token::Dash) => {
                    iter.next();
                    //parse invariant
                    let n = self.parse_num(iter)? * Atom::from(-1.0);
                    Scalar::singular(n)
                }
                Some(Token::Number(_)) => {
                    let n = self.parse_num(iter)?;
                    Scalar::singular(n)
                }
                Some(Token::LPar) => {
                    iter.next();
                    let f1 = self.parse_num(iter)?;
                    self.expect(iter, Token::Dash)?;
                    let f2 = self.parse_num(iter)?;
                    self.expect(iter, Token::RPar)?;
                    if f1 == Atom::from(std::f64::INFINITY) {
                        Scalar::range(Atom::from(std::f64::NEG_INFINITY), f2)
                    } else {
                        let r = Scalar::range(f1, f2);
                        assert!(!r.is_empty());
                        r
                    }
                }
                _ => {
                    return Err(ParseError::MissingNumber);
                }
            };
            v.push(s);
        }

        Ok(v)
    }

    fn parse_num<T: Iterator<Item = Token>>(
        &self,
        iter: &mut Peekable<T>,
    ) -> Result<Atom, ParseError> {
        let neg = if let Some(Token::Dash) = iter.peek() {
            iter.next();
            true
        } else {
            false
        };

        //let peek = .clone();
        let n = if let Some(Token::Number(n)) = iter.peek() {
            let a = Atom::from(*n);
            iter.next();
            a
        } else if let Some(Token::Word(s)) = iter.peek() {
            match (s == "u", neg) {
                (false, _) => {
                    return Err(ParseError::UnexpectedToken(Token::Word(s.clone())));
                }
                (true, true) => {
                    iter.next();
                    return Ok(Atom::from(std::f64::NEG_INFINITY));
                }
                (true, false) => {
                    iter.next();
                    return Ok(Atom::from(std::f64::INFINITY));
                }
            }
        } else {
            return Err(ParseError::MissingNumber);
        };
        Ok(if neg { n * Atom::from(-1.0) } else { n })
    }

    fn expect<T: Iterator<Item = Token>>(
        &self,
        iter: &mut Peekable<T>,
        expecting: Token,
    ) -> Result<(), ParseError> {
        if let Some(t) = iter.peek() {
            if t == &expecting {
                iter.next();
                Ok(())
            } else {
                Err(ParseError::UnexpectedToken(t.clone()))
            }
        } else {
            Err(ParseError::MissingToken)
        }
    }

    pub fn re_string_ha(aha: &AnnotatedHA) -> String{
        let mut name_str =  aha.dim_names.iter().collect::<Vec<_>>();
        name_str.sort_by_key(|(_,ix)| **ix);
        let mut res = format!("\n{} \"{}\"\n", aha.ha.dimension(), name_str.iter().map(|(_,v)| format!("{}",v)).collect::<Vec<_>>().join("\" \""));

        let mut mode = aha.ha.locations.iter().collect::<Vec<_>>();
        mode.sort_by_key(|(id,_)|**id);    
        let all_modes = mode.iter().map(|(_,loc)| {
           loc.to_file_string()
        }).collect::<Vec<_>>().join("\n");

        let all_edges = aha.ha.iter_jumps().map(|((ix,ac,tix),g)|{
            /* 
            e \"name1\" \"name2\" 
            a 0 \"break\"
            g 5 50 (40-60)"; 
            */
            let mut res = format!("e \"{}\" \"{}\"\n", &aha.get_location(*ix).name, &aha.get_location(*tix).name);
            res += &format!("a {} {}\n", ac.0, "\"action_unknown_name_\"");
            let g_str: Vec<_> = g.0.iter().map(|s| -> String {
                format!("( {} - {} )", s.lower().to_string(), s.upper().to_string())
            }).collect();
            res += &format!("g {}\n", g_str.join(" "));

            res
        }).collect::<Vec<_>>().join("\n");

        res+= &all_modes;
        res+= "\n\n";
        res+= &all_edges;
        res+= "\n\n";
        res
    }

    pub fn aha_from_file(file_str: String) -> AnnotatedHA {
        let ha_string = std::fs::read_to_string(file_str).expect("unable to read file");

        let ha_parser = HAParser::new();
        let aha = ha_parser.annotated_ha(ha_string);
        match aha {
            Ok(aha) =>aha,
            Err(err) => {
                eprintln!("{:?}", err);
                panic!("Invalid Input");
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple_parse() {
        let str = "
3 \"vel\" \"angle_vel\" _
l \"name1\"
d (-1 - 1.6) 1.0 (2.0 - 5.0)
i (0 - 10) (10 - 100) (20 - u)

l \"name2\"
d (1.4 - 1.6) 1.0 (2.0 - 5.0)
i (0-10) (10-100) (20-u)

e \"name1\" \"name2\" 
a 0 \"break\"
g 5 50 (40-60)";
        let mut parser = HAParser::new();

        let res = parser.parse(str.to_string());
        assert!(res.is_ok());
        let ha = res.unwrap();
        assert_eq!(ha.dimension(), 3);
        assert_eq!(ha.num_jumps(), 1);
        assert_eq!(ha.num_modes(), 2);

        let l1 = ha.get_location(LocationId(0));
        let i_2 = l1.invariant.0[2];
        assert_eq!(Scalar::right_inf(Atom::from(20f64)), i_2);

        assert_eq!(parser.names.len(), 3);
    }
}
