use gcollections::ops::{Bounded as GBounded, Empty, Intersection};
use interval::ops::Hull;
use interval::ops::{Range, Width};
use interval::Interval;
use num::{Num, One, Zero};
use ordered_float::OrderedFloat;
//#[cfg(feature = "precise")]
//use rug::{float::OrdFloat, float::Special, Float};
use std::convert::TryFrom;
use std::hash::Hash;
use std::hash::Hasher;
use std::ops::AddAssign;
use std::ops::{Add, Div, Mul, Rem, Sub};

use super::Dynamics;

pub type VariableAssignment = Vec<Atom>;
pub type VariableGuess = Vec<Scalar>;

pub fn transform(vars: &VariableGuess, time: &Atom, dynamic: &Dynamics) -> VariableGuess {
    vars.iter()
        .enumerate()
        .map(|(ix, v)| *v + Scalar::singular(*time) * dynamic[ix])
        .collect()
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Rectangle(pub Vec<Scalar>);

impl Rectangle {
    pub fn unbounded(dim: usize) -> Self {
        Rectangle(vec![Scalar::full(); dim])
    }
    pub fn is_member(&self, state: &VariableAssignment) -> bool {
        assert_eq!(self.dimension(), state.len());
        self.0
            .iter()
            .zip(state.iter())
            .all(|(scalar, atom)| &scalar.lower() <= atom && atom <= &scalar.upper())
    }

    pub fn is_satisfied(&self, values: &[Scalar]) -> bool {
        assert_eq!(self.dimension(), values.len());
        self.0
            .iter()
            .zip(values)
            .all(|(bounding, value)| bounding.contains(value))
    }

    /// See [is_satisfied].
    pub fn is_satisfied_atom(&self, values: &[Atom]) -> bool {
        self.is_satisfied(
            &values
                .iter()
                .map(|a| Scalar::singular(*a))
                .collect::<Vec<_>>(),
        )
    }

    /// Tests if given scalar values are at least partly contained in this Rectangle
    /// [1,10].is_partly_contained([2,5]) == true
    /// [1,10].is_partly_contained([5,15]) == true
    /// [1,10].is_partly_contained([11,12]) == false
    pub fn is_partly_contained(&self, values: &[Scalar]) -> bool {
        assert_eq!(self.dimension(), values.len());
        self.0
            .iter()
            .zip(values)
            .all(|(bound, v)| !bound.intersection(v).is_empty())
    }

    /// Constrains values to be inside the rectangle
    /// [1,10].constrain([2,5]) -> [2,5]
    /// [1,10].constrain([5,15]) -> [5,10]
    /// [1,10].constrain([20,22]) -> [] //empty
    pub fn constrain(&self, values: &Vec<Scalar>) -> Vec<Scalar> {
        self.0
            .iter()
            .zip(values.iter())
            .map(|(b, v)| b.intersection(v))
            .collect()
    }

    pub fn dimension(&self) -> usize {
        self.0.len()
    }

    pub fn get_inner(&self) -> &Vec<Scalar> {
        &self.0
    }

    pub fn hull(&self, other: &Self) -> Self {
        assert_eq!(self.dimension(), other.dimension());
        Rectangle(
            self.0
                .iter()
                .zip(other.0.iter())
                .map(|(left, right)| left.hull(right))
                .collect::<Vec<Scalar>>(),
        )
    }


    /// Ensures that values are inside bounds
    /// If Values are not inside the bound: expand bound and reduce value to singular at bound edges
    /// Returns a Vec<Scalar> that describes the bounding Rectangle (.0) and the new Values(.1) constricted to the bounds
    /// If a rectangle was changed in dim X => the values[X] are singular
    pub fn restrict_values_update_bounds(
        &self,
        values: &Vec<Scalar>,
    ) -> (Vec<Scalar>, Vec<Scalar>) {
        let res: (Vec<Scalar>, Vec<Scalar>) = self
            .get_inner()
            .iter()
            .zip(values.iter())
            .map(|(b, v)| {
                let upper_violated = b.upper() < v.lower();
                let lower_violated = b.lower() > v.upper();
                assert!(
                    !(lower_violated && upper_violated),
                    "Violating both bounds is not handled here"
                );

                //extend bound to include found value
                let new_b = if upper_violated {
                    b.expand_to(&v.upper())
                } else if lower_violated {
                    b.expand_to(&v.lower())
                } else {
                    //we are inside, nothing to do
                    b.clone()
                };

                let new_v = new_b.intersection(v);
                assert!(!new_v.is_empty(), "we expanded it earlier can't be empty");

                (new_b, new_v)
            })
            .unzip();
        // new bound , new values
        res
    }

    pub fn restrict_values(
        &self,
        values: &Vec<Scalar>,
    ) -> Vec<Scalar> {
        debug_assert!(self.is_partly_contained(values));
        self.0.iter().zip(values.iter()).map(|(b,v)|{
            b.intersection(v)
        }).collect()
    }
}

#[derive(PartialEq, Eq, Debug, Hash, Clone, Copy, PartialOrd, Ord)]
pub struct Variable(pub(crate) usize);

impl Default for Variable {
    fn default() -> Variable {
        Variable(0)
    }
}

impl From<usize> for Variable {
    fn from(id: usize) -> Variable {
        Variable(id)
    }
}

impl Into<usize> for Variable {
    fn into(self) -> usize {
        self.0
    }
}

#[derive(Clone, PartialEq, Eq, Hash, Copy, Debug)]
#[allow(dead_code)]
pub enum Comparison {
    Equal,
}

pub(crate) trait Invert {
    type Output;
    fn invert(self) -> Self::Output;
}

// TODO: Implement float_cmp::ApproxEq conditionally in imprecise for Atom and Scalar, also keep track of error.
#[derive(Clone, Copy, Debug, PartialOrd, Ord, Hash, PartialEq, Eq)]
pub struct Atom {
    #[cfg(not(feature = "precise"))]
    inner_of: OrderedFloat<f64>,
    #[cfg(feature = "precise")]
    inner_ap: OrdFloat,
}

impl Atom {
    #[cfg(not(feature = "precise"))]
    pub(crate) fn into_inner(self) -> f64 {
        self.inner_of.into_inner()
    }

    #[cfg(feature = "precise")]
    pub(crate) fn into_inner(self) -> f64 {
        self.inner_ap.as_float().to_f64()
    }

    #[cfg(not(feature = "precise"))]
    pub(crate) fn new(v: OrderedFloat<f64>) -> Self {
        Atom { inner_of: v }
    }

    #[cfg(feature = "precise")]
    pub(crate) fn new(v: OrdFloat) -> Self {
        Atom { inner_ap: v }
    }

    pub(crate) fn to_string(&self) -> String {
        format!("{}", self.into_inner())
    }
}

impl Width for Atom {
    type Output = Atom;

    #[cfg(not(feature = "precise"))]
    fn max_value() -> Self {
        Atom::from(std::f64::INFINITY)
    }

    #[cfg(feature = "precise")]
    fn max_value() -> Self {
        Atom::new(OrdFloat::from(Float::with_val(53, Special::Infinity)))
    }

    #[cfg(not(feature = "precise"))]
    fn min_value() -> Self {
        Atom::from(std::f64::NEG_INFINITY)
    }

    #[cfg(feature = "precise")]
    fn min_value() -> Self {
        Atom::new(OrdFloat::from(Float::with_val(53, Special::NegInfinity)))
    }

    #[cfg(not(feature = "precise"))]
    fn width(lower: &Self, upper: &Self) -> Self::Output {
        if lower > upper {
            // TODO: This comparison can go awfully wrong.
            Self::Output::zero()
        } else if approx_eq!(
            f64,
            lower.clone().into_inner(),
            upper.clone().into_inner(),
            ulps = 5
        ) {
            Self::Output::one()
        } else {
            Atom::from(std::f64::INFINITY)
        }
    }

    #[cfg(feature = "precise")]
    fn width(lower: &Self, upper: &Self) -> Self::Output {
        if lower > upper {
            Self::Output::zero()
        } else if lower == upper {
            Self::Output::one()
        } else {
            Atom::from(std::f64::INFINITY)
        }
    }
}

impl From<f64> for Atom {
    #[cfg(feature = "precise")]
    fn from(f: f64) -> Atom {
        Atom::new(OrdFloat::from(Float::with_val(53, f)))
    }
    #[cfg(not(feature = "precise"))]
    fn from(f: f64) -> Atom {
        Atom::new(OrderedFloat::from(f))
    }
}

impl TryFrom<Scalar> for Atom {
    type Error = &'static str;

    fn try_from(value: Scalar) -> Result<Self, Self::Error> {
        if value.is_singular() {
            Ok(value.lower())
        } else {
            Err("Only singletons can be converted to Atoms!")
        }
    }
}

impl Num for Atom {
    type FromStrRadixErr = ();

    fn from_str_radix(str: &str, radix: u32) -> Result<Self, Self::FromStrRadixErr> {
        f64::from_str_radix(str, radix)
            .map(Atom::from)
            .map_err(|_| ())
    }
}

impl Add for Atom {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Atom::from(self.into_inner() + other.into_inner())
    }
}

impl AddAssign for Atom {
    fn add_assign(&mut self, rhs: Self) {
        *self = self.clone() + rhs.clone();
    }
}

impl Sub for Atom {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Atom::from(self.into_inner() - other.into_inner())
    }
}

impl Mul for Atom {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Atom::from(self.into_inner() * other.into_inner())
    }
}

impl Div for Atom {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        Atom::from(self.into_inner() / other.into_inner())
    }
}

impl Rem for Atom {
    type Output = Self;

    fn rem(self, other: Self) -> Self {
        Atom::from(self.into_inner() % other.into_inner())
    }
}

impl Invert for Atom {
    type Output = Self;

    fn invert(self) -> Self {
        Atom::from(1.0 / self.into_inner())
    }
}

impl One for Atom {
    fn one() -> Self {
        Atom::from(1f64)
    }
}

impl Zero for Atom {
    fn zero() -> Self {
        Atom::from(0f64)
    }

    fn is_zero(&self) -> bool {
        *self == Atom::zero()
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub struct Scalar(Interval<Atom>);

#[derive(Debug, Clone, PartialEq, Eq, Copy)]
pub enum Signum {
    POSITIVE,
    NEGATIVE,
    ZERO,
    BOTH,
}

impl Hash for Scalar {
    fn hash<H>(&self, h: &mut H)
    where
        H: Hasher,
    {
        self.0.upper().hash(h);
        self.0.lower().hash(h);
    }
}

pub(crate) trait ScalarT: One + Zero + Num + From<f64> {}

impl One for Scalar {
    fn one() -> Self {
        Self::from(1)
    }
}

impl Zero for Scalar {
    fn zero() -> Self {
        Self::from(0)
    }

    fn is_zero(&self) -> bool {
        self.lower().is_zero() && self.upper().is_zero()
    }
}

impl Num for Scalar {
    type FromStrRadixErr = ();

    fn from_str_radix(str: &str, radix: u32) -> Result<Self, Self::FromStrRadixErr> {
        Atom::from_str_radix(str, radix).map(Scalar::singular)
    }
}

impl Scalar {
    pub(crate) fn singular(v: Atom) -> Scalar {
        Scalar::range(v.clone(), v)
    }

    #[allow(dead_code)]
    pub(crate) fn range(lower: Atom, upper: Atom) -> Scalar {
        Scalar(Interval::new(lower, upper))
    }

    #[allow(dead_code)]
    pub(crate) fn left_inf(upper: Atom) -> Scalar {
        Scalar(Interval::new(Atom::from(std::f64::NEG_INFINITY), upper))
    }

    #[allow(dead_code)]
    pub(crate) fn right_inf(lower: Atom) -> Scalar {
        Scalar(Interval::new(lower, Atom::from(std::f64::INFINITY)))
    }

    #[allow(dead_code)]
    pub(crate) fn full() -> Scalar {
        Scalar(Interval::new(
            Atom::from(std::f64::NEG_INFINITY),
            Atom::from(std::f64::INFINITY),
        ))
    }

    #[allow(dead_code)]
    pub(crate) fn one() -> Self {
        <Self as One>::one()
    }

    #[allow(dead_code)]
    pub(crate) fn zero() -> Self {
        <Self as Zero>::zero()
    }

    #[allow(dead_code)]
    pub(crate) fn is_zero(&self) -> bool {
        <Self as Zero>::is_zero(self)
    }

    #[allow(dead_code)]
    pub(crate) fn is_empty(&self) -> bool {
        self.0.size().is_zero()
    }

    #[allow(dead_code)]
    pub(crate) fn is_singular(&self) -> bool {
        self.0.size().is_one()
    }

    #[allow(dead_code)]
    pub(crate) fn lower(&self) -> Atom {
        self.0.lower()
    }

    #[allow(dead_code)]
    pub(crate) fn upper(&self) -> Atom {
        self.0.upper()
    }

    #[allow(dead_code)]
    pub(crate) fn middle(&self) -> Atom {
        (self.0.upper() + self.0.lower()) / Atom::from(2.0)
    }

    #[allow(dead_code)]
    pub(crate) fn bounds(&self) -> Option<(Atom, Atom)> {
        if self.is_empty() {
            None
        } else {
            Some((self.lower(), self.upper()))
        }
    }

    pub(crate) fn contains(&self, other: &Self) -> bool {
        //dbg!(self);
        //dbg!(other);
        self.lower() <= other.lower() && self.upper() >= other.upper()
    }

    /// See [contains].
    pub(crate) fn contains_atom(&self, other: &Atom) -> bool {
        //dbg!(self);
        //dbg!(other);
        (self.lower() < *other
            || crate::help_lib::approx_f_eq(self.lower().into_inner(), other.into_inner()))
            && (self.upper() > *other
                || crate::help_lib::approx_f_eq(self.upper().into_inner(), other.into_inner()))
    }

    /// Expands the Scalar to include the atom as a bound if it was ot included, else return identity
    /// [1.5].expand_to(7) -> [1,7]
    /// [1.5].expand_to(0) -> [0,5]
    /// [1.5].expand_to(3) -> [1,5]
    pub(crate) fn expand_to(&self, point: &Atom) -> Self {
        let upper_violated = self.upper() < *point;
        let lower_violated = self.lower() > *point;
        assert!(
            !upper_violated || !lower_violated,
            "at most 1 has to be violated"
        );

        self.hull(&Scalar::singular(*point))
    }

    /// Gets the sign of an Interval
    /// [1,2] -> +
    /// [-5,-1] -> -
    /// [-1,1] -> BOTH
    /// [0,0] -> 0
    pub(crate) fn get_signum(&self) -> Signum {
        if self.is_empty() {
            return Signum::ZERO;
        }
        match (self.lower() < Atom::zero(), self.upper() > Atom::zero()) {
            (true, true) => Signum::BOTH,      //e.g.: [-1,1]
            (true, false) => Signum::NEGATIVE, //e.g.: [-3,-1]
            (false, true) => Signum::POSITIVE, //e.g.: [1,4]
            (false, false) => unreachable!("previous is_zero() case ensures this"), //e.g. [0,0], extracted as this is not obvious
        }
    }
}

impl Hull for Scalar {
    type Output = Self;
    fn hull(&self, rhs: &Self) -> Self::Output {
        Scalar(self.0.hull(&rhs.0))
    }
}

impl Empty for Scalar {
    fn empty() -> Self {
        Scalar(Interval::empty())
    }
}

impl Intersection for Scalar {
    type Output = Self;

    fn intersection(&self, rhs: &Self) -> Self::Output {
        Scalar(self.0.intersection(&rhs.0))
    }
}

impl Invert for Scalar {
    type Output = Self;

    fn invert(self) -> Self {
        let l = self.lower();
        let u = self.upper();
        let zero = <Atom as Zero>::zero();
        if !(l < zero && zero < u) {
            Scalar::range(u.invert(), l.invert())
        } else if l == zero {
            Scalar::right_inf(u.invert())
        } else if u == zero {
            Scalar::left_inf(l.invert())
        } else {
            // TODO: More conservative, please!  Use IntervalSet.
            Scalar::full()
        }
    }
}

impl From<f64> for Scalar {
    fn from(v: f64) -> Self {
        Scalar::singular(Atom::from(v))
    }
}

impl From<f32> for Scalar {
    fn from(v: f32) -> Self {
        Scalar::singular(Atom::from(f64::from(v)))
    }
}

impl From<u32> for Scalar {
    fn from(v: u32) -> Self {
        Scalar::singular(Atom::from(f64::from(v)))
    }
}

impl From<i32> for Scalar {
    fn from(v: i32) -> Self {
        Scalar::singular(Atom::from(f64::from(v)))
    }
}

impl Add for Scalar {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Scalar(self.0 + other.0)
    }
}

impl AddAssign for Scalar {
    fn add_assign(&mut self, rhs: Self) {
        *self = self.clone() + rhs.clone();
    }
}

impl Sub for Scalar {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Scalar(self.0 - other.0)
    }
}

impl Mul for Scalar {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Scalar(self.0 * other.0)
    }
}

impl Div for Scalar {
    type Output = Self;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn div(self, other: Self) -> Self {
        Scalar(self.0 * other.invert().0)
    }
}

impl Rem for Scalar {
    type Output = Self;

    fn rem(self, _other: Self) -> Self {
        panic!("Dude, what the hell? Modulo on intervals? For computing polynomials? I don't know what you're doing but working it ain't.")
    }
}

impl Mul<Atom> for Vec<Scalar> {
    type Output = Self;

    fn mul(self, rhs: Atom) -> Self::Output {
        let rhs_s = Scalar::singular(rhs);
        self.into_iter().map(|s| s* rhs_s).collect()
    }
}

/*
impl Add<Vec<Scalar>> for Vec<Scalar> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        assert_eq!(self.len(),rhs.len());
        self.into_iter().zip(rhs.into_iter()).map(|(l,r)| l+r).collect()
    }
}
 */

#[cfg(test)]
mod tests {
    use crate::hybridautomaton::basics::{Atom, Rectangle, Scalar};
    use gcollections::ops::Intersection;
    use num::{One, Zero};

    #[test]
    fn test_add() {
        let one = Scalar::one();
        let two = Scalar::from(2);
        let mut res = one + two; // [1,1] + [2,2] = [3,3]
        assert_eq!(Scalar::from(3), res);
        let int = Scalar::range(Atom::from(1f64), Atom::from(2f64));
        res = res + int.clone(); // [3,3] + [1,2] = [4,5]
        let mut expected = Scalar::range(Atom::from(4f64), Atom::from(5f64));
        assert_eq!(expected, res);
        res = res + int; // [4,5] + [1,2] = [5,7]
        expected = Scalar::range(Atom::from(5f64), Atom::from(7f64));
        assert_eq!(expected, res);
    }

    #[test]
    fn test_sub() {
        let one = Scalar::one();
        let two = Scalar::from(2);
        let mut res = two - one; // [2,2] - [1,1] = [1,1]
        assert_eq!(Scalar::one(), res);
        let int = Scalar::range(Atom::from(1f64), Atom::from(2f64));
        res = res - int.clone(); // [1,1] - [1,2] = [-1,0]
        let mut expected = Scalar::range(Atom::from(-1f64), Atom::zero());
        assert_eq!(expected, res);
        res = res - int; // [-1,0] - [1,2] = [-3,-1]
        expected = Scalar::range(Atom::from(-3f64), Atom::from(-1f64));
        assert_eq!(expected, res);
    }

    #[test]
    fn test_mul() {
        let two = Scalar::from(2);
        let three = Scalar::from(3);
        let mut res = two * three; // [2,2] * [3,3] = [6,6]
        assert_eq!(Scalar::from(6), res);
        let int = Scalar::range(Atom::from(1f64), Atom::from(2f64));
        res = res * int.clone(); // [6,6] * [1,2] = [6,12]
        let mut expected = Scalar::range(Atom::from(6f64), Atom::from(12f64));
        assert_eq!(expected, res);
        res = res * int; // [6,12] * [1,2] = [6,24]
        expected = Scalar::range(Atom::from(6f64), Atom::from(24f64));
        assert_eq!(expected, res);
    }

    #[test]
    fn test_div() {
        let two = Scalar::from(2);
        let four = Scalar::from(4);
        let mut res = four / two; // [4,4] / [2,2] = [2,2]
        assert_eq!(Scalar::from(2), res);
        let int = Scalar::range(Atom::from(2f64), Atom::from(4f64));
        res = res / int.clone(); // [2,2] / [2,4] = [0.5,1]
        let mut expected = Scalar::range(Atom::from(0.5), Atom::one());
        assert_eq!(expected, res);
        res = res / int; // [0.5,1] / [2,4] = [0.125,0.5]
        expected = Scalar::range(Atom::from(0.125), Atom::from(0.5));
        assert_eq!(expected, res);
    }

    #[test]
    fn partly_contained() {
        let two = Scalar::range(Atom::from(1f64), Atom::from(2f64));
        let four = Scalar::from(4);
        let prod = two * four; // [4,4] * [1,2] = [4,8]
        let expected = Scalar::range(Atom::from(4f64), Atom::from(8f64));
        assert_eq!(prod, expected);
        let in_full: Scalar = Scalar::range(Atom::from(5f64), Atom::from(6f64));
        let in_partly = Scalar::range(Atom::from(5f64), Atom::from(9f64));
        let not_in = Scalar::range(Atom::from(10f64), Atom::from(11f64));
        let bound: Rectangle = Rectangle(vec![prod]);
        assert!(bound.is_partly_contained(&[in_full]));
        assert!(bound.is_partly_contained(&[in_partly]));
        assert!(!bound.is_partly_contained(&[not_in]));

        let exact: Scalar = Scalar::range(Atom::from(4f64), Atom::from(8f64));
        let over_approx = Scalar::range(Atom::from(2f64), Atom::from(9f64));
        let include_upper = Scalar::range(Atom::from(8f64), Atom::from(11f64));

        assert!(bound.is_partly_contained(&[exact]));
        assert!(bound.is_partly_contained(&[over_approx]));
        assert!(bound.is_partly_contained(&[include_upper]));
    }

    #[test]
    fn constrain() {
        let bound = Scalar::range(Atom::from(1f64), Atom::from(10f64));
        let bound: Rectangle = Rectangle(vec![bound]);
        let s1 = Scalar::range(Atom::from(2f64), Atom::from(11f64));
        let e1 = Scalar::range(Atom::from(2f64), Atom::from(10f64));

        let s2 = Scalar::range(Atom::from(2f64), Atom::from(5f64));
        let e2 = Scalar::range(Atom::from(2f64), Atom::from(5f64));

        let s3 = Scalar::range(Atom::from(10f64), Atom::from(11f64));
        let e3 = Scalar::range(Atom::from(10f64), Atom::from(10f64));

        let s4 = Scalar::range(Atom::from(20f64), Atom::from(22f64));
        let e4 = Scalar::range(Atom::from(20f64), Atom::from(22f64))
            .intersection(&Scalar::singular(Atom::from(0f64)));

        assert_eq!(bound.constrain(&vec![s1]), vec![e1]);
        assert_eq!(bound.constrain(&vec![s2]), vec![e2]);
        assert_eq!(bound.constrain(&vec![s3]), vec![e3]);
        assert_eq!(bound.constrain(&vec![s4]), vec![e4]);
    }

    #[test]
    fn expand() {
        let bound = Scalar::range(Atom::from(1f64), Atom::from(10f64));
        let a1 = Atom::from(15f64);
        let e1 = Scalar::range(Atom::from(1f64), Atom::from(15f64));

        let a2 = Atom::from(0f64);
        let e2 = Scalar::range(Atom::from(0f64), Atom::from(10f64));

        let a3 = Atom::from(5f64);
        let e3 = Scalar::range(Atom::from(1f64), Atom::from(10f64));

        assert_eq!(bound.expand_to(&a1), e1);
        assert_eq!(bound.expand_to(&a2), e2);
        assert_eq!(bound.expand_to(&a3), e3);
    }
}
