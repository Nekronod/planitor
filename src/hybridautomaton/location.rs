use super::{ConcreteDynamics, Dynamics};
use crate::hybridautomaton::basics::{Atom, Scalar, Variable};
use crate::hybridautomaton::Rectangle;
use interval::ops::Hull;

#[derive(PartialEq, Eq, Debug, Hash, Clone, Copy, PartialOrd, Ord)]
pub struct LocationId(pub(crate) usize);

impl From<usize> for LocationId {
    fn from(id: usize) -> LocationId {
        LocationId(id)
    }
}

impl Into<usize> for LocationId {
    fn into(self) -> usize {
        self.0
    }
}

#[derive(PartialEq, Eq, Debug, Clone)]
pub struct Location {
    pub name: String,
    pub id: LocationId,
    pub dynamics: Dynamics,
    pub invariant: Rectangle,
}

impl Location {
    #[cfg(test)]
    pub(crate) fn new(
        name: String,
        id: LocationId,
        dynamics: Dynamics,
        invariant: Vec<Scalar>,
    ) -> Location {
        Location {
            name,
            id,
            dynamics,
            invariant: Rectangle(invariant),
        }
    }

    pub(crate) fn dimension(&self) -> usize {
        self.dynamics.dimension()
    }

    #[allow(dead_code)]
    pub(crate) fn get_dynamics(&self, var: Variable) -> Scalar {
        self.dynamics[var.0].clone()
    }

    #[allow(dead_code)]
    pub(crate) fn get_all_dynamics(&self) -> &Vec<Scalar> {
        &self.dynamics.0
    }

    #[allow(dead_code)]
    pub(crate) fn init_dynamics(&mut self, lambdas: Dynamics) {
        self.dynamics = lambdas;
    }

    #[allow(dead_code)]
    pub(crate) fn set_dynamics_for(&mut self, var: Variable, lambda: Atom) {
        self.dynamics[var.0] = Scalar::singular(lambda);
    }

    #[allow(dead_code)]
    pub(crate) fn set_dynamics(&mut self, lambdas: Dynamics) {
        assert_eq!(self.dimension(), lambdas.dimension());
        self.dynamics = lambdas;
    }

    #[allow(dead_code)]
    pub(crate) fn relax_dynamics_for(&mut self, var: Variable, lambda: Atom) {
        let dyns = &self.dynamics[var.0];
        self.dynamics[var.0] = dyns.hull(&Scalar::singular(lambda));
    }

    #[allow(dead_code)]
    pub(crate) fn relax_dynamics(&mut self, lambdas: ConcreteDynamics) {
        assert_eq!(self.dimension(), lambdas.dimension());
        for (ix, lambda) in lambdas.0.iter().enumerate() {
            self.relax_dynamics_for(Variable(ix), lambda.clone());
        }
    }

    #[allow(dead_code)]
    pub(crate) fn merge_dynamics(a: &Self, b: &Self) -> Dynamics {
        assert_eq!(
            a.dynamics.dimension(),
            b.dynamics.dimension(),
            "Both polynomials must have dynamics for the same variables."
        );
        Self::merge_scalars(&a.dynamics.0, &b.dynamics.0).into()
    }

    #[allow(dead_code)]
    pub(crate) fn merge_invariants(a: &Self, b: &Self) -> Rectangle {
        let inner = Self::merge_scalars(&a.invariant.0, &b.invariant.0);
        Rectangle(inner)
    }

    pub(crate) fn merge_scalars(a: &[Scalar], b: &[Scalar]) -> Vec<Scalar> {
        a.iter().zip(b.iter()).map(|(l, r)| l.hull(r)).collect()
    }

    pub(crate) fn set_invariant(&mut self, inv: Rectangle) {
        self.invariant = inv;
    }

    pub(crate) fn to_file_string(&self) -> String {
         let mut mode_res = format!("l \"{}\"\n", self.name.clone());
        let dyn_str: Vec<_> = self.dynamics.0.iter().map(|s| -> String {
            format!("( {} - {} )", s.lower().to_string(), s.upper().to_string())
        }).collect();
        mode_res += &format!("d {}\n", dyn_str.join(" "));
        mode_res
    }
   
}
