use interval::ops::Hull;
use num::Zero;

use crate::hybridautomaton::{basics::{Atom, VariableAssignment}};

use super::{basics::{Scalar, VariableGuess, Rectangle}, LocationId, HA, Dynamics};

/// An omniscient trace, variable information at any transition
#[derive(Debug, Clone)]
pub struct TimedTrace {
    pub initial: VariableAssignment,
    pub end_state: VariableAssignment,
    pub steps: Vec<Step>,
    pub last_delay: Atom,
}

#[derive(Debug, Clone)]
pub struct ObservableTrace {
    pub initial: VariableGuess,
    pub end_state: VariableGuess,
    pub steps: Vec<BlindStep>,
    pub last_delay: Atom,
}

impl TimedTrace {
    pub fn steps(&self) -> impl Iterator<Item = &Step> {
        self.steps.iter()
    }
    pub fn len(&self) -> usize {
        self.steps.len()
    }
    pub fn is_empty(&self) -> bool {
        self.steps.is_empty()
    }

    pub fn append(&mut self, step: Step) {
        self.steps.push(step);
    }

    pub(crate) fn into_input_events(self) -> Vec<Event> {
        let Self { initial: _, end_state, steps, last_delay } = self;
        let mut res_v:  Vec<Event> = steps.into_iter().clone().map(|s|{
            let Step { delay, action, state } = s;
            Event::new(delay, state.into_iter().map(Option::Some).collect(), Some(action))
        }).collect();

        let last_event = Event::only_val(last_delay, end_state.into_iter().map(Option::Some).collect());
        res_v.push(last_event);
        res_v

    }
}

#[derive(PartialEq, Eq, Debug, Hash, Clone, Copy, PartialOrd, Ord)]
pub struct ActionId(pub(crate) usize);

#[derive(Debug, Clone)]
pub struct Step {
    pub delay: Atom,
    pub action: ActionId,
    pub state: VariableAssignment,
}

#[derive(Debug, Clone)]
pub struct BlindStep {
    pub delay: Atom,
    pub action: ActionId,
    pub state: VariableGuess,
}

impl From<Step> for BlindStep {
    fn from(step: Step) -> Self {
        let Step {
            delay,
            action,
            state,
        } = step;
        BlindStep {
            delay,
            action,
            state: state.iter().map(|a| Scalar::singular(*a)).collect(),
        }
    }
}

impl BlindStep {
    pub fn new(delay: Atom, action: ActionId, state: VariableGuess) -> Self {
        BlindStep {
            delay: Atom::from(delay),
            action,
            state,
        }
    }
}

impl Step {
    pub fn new(delay: f64, action: ActionId, state: VariableAssignment) -> Step {
        Step {
            delay: Atom::from(delay),
            action,
            state,
        }
    }
}

#[derive(Debug,Clone)]
pub(crate)  struct MeasurableTrace {
    pub initial_values: VariableGuess,
    pub end_state: VariableGuess,
    pub steps: Vec<BlindStep>,
    pub last_delay: Atom,

}
#[derive(Debug,Clone,PartialEq, Eq, PartialOrd, Ord)]
pub(crate)  struct Event {
    time_delay: Atom,
    observed_action: Option<ActionId>,
    measured_values: Vec<Option<Atom>>,
}

impl Event {
    pub fn only_action(time_delay: Atom, ac: ActionId) -> Self {
        Event {
            time_delay,
            observed_action: Some(ac),
            measured_values: vec![],
        }
    }

    pub fn only_val(time_delay: Atom, values: Vec<Option<Atom>>) -> Self {
        Event {
            time_delay,
            observed_action: None,
            measured_values: values,
        }
    }

    pub fn new(time_delay: Atom, values: Vec<Option<Atom>>, ac: Option<ActionId>) -> Self {
        Event {
            time_delay,
            observed_action: ac,
            measured_values: values,
        }
    }
}

#[derive(Debug,Clone)]
pub(crate)  enum EventUpdate {
    Dynamic(LocationId, Dynamics),
    Guard{from: LocationId, to: LocationId, with: ActionId, guard: Rectangle},
    Invariant(LocationId, Rectangle),
}

#[derive(Debug,Clone)]
pub(crate) struct TraceBuilder {
    pub(crate) cur_loc: LocationId,
    pub(crate) trace: MeasurableTrace,
    pub(crate) loc_list: Vec<LocationId>,
    updates: Vec<EventUpdate>,
}

impl TraceBuilder{
    pub fn new(initial_values: VariableAssignment, cur_loc: LocationId) -> Self {
        let v: Vec<Scalar> = initial_values.into_iter().map(|a|Scalar::singular(a)).collect();
        Self::new_guess(v, cur_loc)
    }

    pub fn new_guess(initial_values: VariableGuess, cur_loc: LocationId) -> Self {
        TraceBuilder {
            updates: vec![],
            cur_loc,
            loc_list: Vec::new(),
            trace: MeasurableTrace {
                end_state: initial_values.clone(),
                steps: vec![],
                last_delay: <Atom as num::Zero>::zero(),
                initial_values,
            }
        }
    }

    pub fn get_cur_loc_id(&self) -> LocationId {
        self.cur_loc
    }

    pub fn lookup_value(&self, ix: usize) -> Scalar {
        self.trace.end_state[ix]
    }

    pub fn has_updates(&self) -> bool {
        !self.updates.is_empty()
    }

    pub fn extract_updates(&mut self) -> Vec<EventUpdate> {
        let mut empty = vec![];
        std::mem::swap(&mut empty, &mut self.updates);
        empty
    }

    pub fn reset(&mut self) {
        eprintln!("Resetting current trace!\n");
        self.trace.steps.clear();
        self.trace.initial_values = self.trace.end_state.clone();
        self.trace.last_delay = Atom::zero();
    }

    pub fn hard_reset(&mut self) {
        //use colored::*;
        //eprintln!("{}","HARD Resetting current trace!\n".red());
        self.trace.steps.clear();
        self.trace.end_state = self.trace.initial_values.clone();
        self.trace.last_delay = Atom::zero();
        self.cur_loc = LocationId(0);
        self.updates.clear();
    }

    ///returns if new updates have been pushed to the storage
    pub fn update_event(&mut self, ha: &HA, input: Event) -> bool {
        //dbg!(&input);
        let updates_num = self.updates.len();
        let Event {
            time_delay,
            observed_action,
            measured_values,
        } = input;

        assert!(time_delay >= Atom::zero());
        if time_delay == Atom::zero() && observed_action.is_none() {
            return false;
        }

        let starting_values = self.trace.end_state.clone();


        let cur_loc  = ha.get_location(self.cur_loc);
        let cur_dyn = cur_loc.dynamics.0.clone();
        let time_times_dyn = cur_dyn * time_delay;
        let time_advanced_state = starting_values.clone().into_iter().zip(time_times_dyn.into_iter()).map(|(l,r)|l+r).collect::<Vec<_>>();
        let mut restricted_values = time_advanced_state;


        //dbg!(&restricted_values,&starting_values,&cur_dyn,self.cur_loc);
        //update according the invariant
        let inv: &Rectangle = &cur_loc.invariant;
        if inv.is_partly_contained(&restricted_values) {
            //refine and we good
            if !inv.is_satisfied(&restricted_values) {
                let new_val = inv.restrict_values(&restricted_values);
                restricted_values = new_val
            }
        } else {
            //we not good - expand bound and restrict values
            //todo!("todo expand inv and restrict values");
            let (new_b,new_v) = inv.restrict_values_update_bounds(&restricted_values);
            restricted_values = new_v;
            self.updates.push(EventUpdate::Invariant(self.cur_loc, Rectangle(new_b)));
        }


        //handle input event containing measurements
        if measured_values.iter().any(Option::is_some) {
            let mut mut_dyanmic = cur_loc.dynamics.0.clone();
            let mut change = false;
            //at least 1 new value
            restricted_values = measured_values.iter().enumerate().zip(restricted_values.into_iter()).map(|((ix,m_v),tav)|{
                //dbg!(mut_dyanmic[ix], m_v,tav, ix);
                if let Some(m_v) = m_v {
                    //dbg!(&tav,m_v);
                    if tav.contains_atom(m_v) {
                        //do nothing
                    } else {
                        //dbg!(mut_dyanmic[ix], m_v,tav, ix,self.cur_loc);

                        if mut_dyanmic[ix] == Scalar::zero() {
                            unreachable!("you shouldnt be here");
                            /*
                            assert!(mut_dyanmic[ix] != Scalar::zero(), "Zero dyn error not implemented");//TODO
                            let loc_history: Vec<&Location> = self.loc_list.iter().map(|lid| ha.get_location(*lid)).rev().collect();
                            if let Some(l) = loc_history.into_iter().find(|l|l.dynamics.0[ix] != Scalar::zero()) {
                                let observed_dyn =  (Scalar::singular(*m_v) - starting_values[ix]) / Scalar::singular(time_delay); 
                                let new_dyn = l.dynamics[ix].hull(&observed_dyn);
                                if new_dyn != l.dynamics[ix] {
                                    let mut updated_dyn = l.dynamics.0.clone();
                                    updated_dyn[ix] = new_dyn;
                                    self.updates.push(EventUpdate::Dynamic(l.id, Dynamics::from(updated_dyn)));
                                }
                            } else {
                                let observed_dyn =  (Scalar::singular(*m_v) - starting_values[ix]) / Scalar::singular(time_delay); 
                                let new_dyn = mut_dyanmic[ix].hull(&observed_dyn);
                                mut_dyanmic[ix] =  new_dyn;
                                change = true;
                            }
                             */

                            
                        }  else {
                            // update dyn
                            let observed_dyn =  (Scalar::singular(*m_v) - starting_values[ix]) / Scalar::singular(time_delay); 
                            let new_dyn = mut_dyanmic[ix].hull(&observed_dyn);
                            if new_dyn != mut_dyanmic[ix] {
                                mut_dyanmic[ix] =  new_dyn;
                                change = true;
                            }
                        }
                    };
                    Scalar::singular(*m_v)
                } else {
                    tav
                }
            }).collect();

            if change {
                self.updates.push(EventUpdate::Dynamic(self.cur_loc, Dynamics::from(mut_dyanmic)));
            }
        }


        if let Some(ac) = observed_action {
            let target_vec: Vec<LocationId> = ha.successors_actions(self.cur_loc).into_iter().filter(|(with,_)| *with == ac).map(|v|v.1).collect();
            if target_vec.len() != 1 {
                dbg!(&target_vec);
                todo!("ambiguous actions");
            }
            let target = target_vec.first().unwrap();

            let guard = ha.get_edge_guard(self.cur_loc, ac, *target);
            if guard.is_partly_contained(&restricted_values) {
                //refine and we good
                if !guard.is_satisfied(&restricted_values) {
                    let new_val = guard.restrict_values(&restricted_values);
                    restricted_values = new_val
                }
            } else {
                //we not good - expand bound and restrict values
                dbg!(&guard, &restricted_values);
                //todo!("todo expand guard and restrict values");
                let (new_g,new_v) = guard.restrict_values_update_bounds(&restricted_values);
                restricted_values = new_v;
                self.updates.push(EventUpdate::Guard { from: self.cur_loc, to: *target, with: ac, guard: Rectangle(new_g) });
            }

            //update trace
            self.trace.end_state = restricted_values.clone();
            let bs = BlindStep::new(time_delay + self.trace.last_delay, ac, restricted_values);
            self.trace.steps.push(bs);
            self.trace.last_delay = Atom::zero();
            self.loc_list.push(self.cur_loc);
            self.cur_loc = *target;



        } else {
            self.trace.end_state = restricted_values;
            self.trace.last_delay += time_delay;
        }
        self.updates.len() > updates_num
    }
}