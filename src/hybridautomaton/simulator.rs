use super::ConcreteDynamics;
use super::basics::{Atom, Scalar, VariableAssignment};
use super::LocationId;
use super::{EdgeRef, Rectangle, Step, TimedTrace, HA};
use gcollections::ops::{Empty, Intersection};
use num::Zero;

use super::walker::{RandomWalk, Walker, AdequateWalk, PathStep};

pub struct Simulator<'a> {
    ha: &'a HA,
}

impl<'a> Simulator<'a> {
    pub fn new(ha: &'a HA) -> Self {
        Simulator { ha }
    }

    pub fn random_walks(&mut self, length: usize, number: usize, initial: VariableAssignment) -> Vec<TimedTrace> {
        // e.g. let initial: VariableAssignment = vec![Atom::zero(); self.ha.dimension()];
        let walker = RandomWalk::new(initial, self.ha.initial_location, length);
        (0..number).map(|_| self.walk(length, walker.clone())).collect()
    }

    #[allow(dead_code)]
    pub(crate) fn adequate_walks(&mut self, steps: Vec<PathStep>, number: usize, initial: VariableAssignment) -> Vec<TimedTrace> {
         // e.g. let initial: VariableAssignment = vec![Atom::zero(); self.ha.dimension()];
         let num = steps.len() - 1;
         let walker = AdequateWalk::new(initial, self.ha.initial_location, steps);
         (0..number).map(|_| self.walk(num, walker.clone())).collect()
    }

    fn walk<W: Walker>(&mut self, length: usize, mut walker: W) -> TimedTrace {
        for _ in 0..length {
            let dynamics = walker.choose_dynamics(&self.ha.get_location(walker.location()).dynamics);
            let jumps = self.find_possibly_enabled_outgoing_jumps(walker.location(), walker.state(), &dynamics); // TODO: len = 0?
            let (edge, delay) = walker.select_transition(&jumps, &dynamics);
            let next_state = self.delay_transition(&walker.state(), &dynamics, delay.clone().into_inner());
            let ((source, action, target), guard) = edge;
            assert_eq!(*source, walker.location());
            assert!(guard.is_member(&next_state));
            let step = Step::new(delay.into_inner(), *action, next_state.clone());
            walker.commit(step, *target, next_state);
        }
        let dynamics = walker.choose_dynamics(&self.ha.get_location(walker.location()).dynamics);
        let last_delay = walker.select_final_delay().into_inner();
        let end_state = self.delay_transition(&walker.state(), &dynamics, last_delay);
        walker.finish(last_delay, end_state)
    }

    fn find_possibly_enabled_outgoing_jumps(
        &self,
        lid: LocationId,
        assgn: &VariableAssignment,
        dynamics: &ConcreteDynamics,
    ) -> Vec<(EdgeRef<LocationId>, Scalar)> {
        self.ha
            .outbound(lid)
            .filter_map(|(src_act_dst, grd)| {
                let interval = Self::get_enabled_time_interval(grd, assgn, dynamics);
                if interval.is_empty() {
                    None
                } else {
                    Some(((src_act_dst, grd), interval))
                }
            })
            .collect()
    }

    fn get_enabled_time_interval(guard: &Rectangle, assgn: &VariableAssignment, dynamics: &ConcreteDynamics) -> Scalar {
        let mut time_interval = Scalar::right_inf(Atom::zero());

        // Compute enabled interval for every conjunct and intersect them.
        for (var, interval) in guard.0.iter().enumerate() {
            let val_assgn = assgn[var];
            let curr_dyn = dynamics[var];

            // Guard currently enabled?
            let enabled = val_assgn >= interval.lower() && val_assgn <= interval.upper();

            let (lower, upper) = if enabled {
                // Dynamics are 0, i.e., assignment won't leave interval.
                if curr_dyn.is_zero() {
                    continue;
                }

                // Depending on sign of dynamics, guard enabled until lower (-dyn) or upper (+dyn) bound of guard is reached.
                let u = if curr_dyn < Atom::zero() {
                    Self::compute_delay(interval.lower(), curr_dyn, val_assgn, true)
                } else {
                    Self::compute_delay(interval.upper(), curr_dyn, val_assgn, false)
                };
                (Atom::zero(), u)
            } else {
                // Dynamics are 0, i.e., guard won't ever be satisfied.
                if curr_dyn.is_zero() {
                    return Scalar::empty();
                }

                // How long do we need to reach lower and upper bound of guard?
                let int_l = Self::compute_delay(interval.lower(), curr_dyn, val_assgn, true);
                let int_u = Self::compute_delay(interval.upper(), curr_dyn, val_assgn, false);

                if val_assgn < interval.lower() {
                    // Assignment needs to move in positive direction...
                    if curr_dyn < Atom::zero() {
                        return Scalar::empty();
                    }
                    // ... and will reach lower bound of guard first.
                    (int_l, int_u)
                } else {
                    // Assignment needs to move in negative direction...
                    if curr_dyn > Atom::zero() {
                        return Scalar::empty();
                    }
                    // ... and will reach upper bound of guard first.
                    (int_u, int_l)
                }
            };
            time_interval = time_interval.intersection(&Scalar::range(lower, upper));
        }
        time_interval
    }

    fn delay_transition(
        &self,
        current: &VariableAssignment,
        dynamics: &ConcreteDynamics,
        delay: f64,
    ) -> VariableAssignment {
        //dbg!(&dynamics, delay, current);
        current.iter().enumerate().map(|(dim, val)| Atom::from(delay) * dynamics[dim].clone() + val.clone()).collect()
    }

    pub(crate) fn compute_delay(guard: Atom, dynamics: Atom, assgn: Atom, is_lower: bool) -> Atom {
        let value_diff = guard.clone() - assgn.clone();
        if dynamics.is_zero() && value_diff.is_zero() {
            if is_lower {
                Atom::from(f64::NEG_INFINITY)
            } else {
                Atom::from(f64::INFINITY)
            }
        } else {
            value_diff / dynamics.clone()
        }
    }
}