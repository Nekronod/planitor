use crate::hybridautomaton::ActionId;
use crate::hybridautomaton::{HAConstructionError, Location, LocationId, HA};
use std::fmt;
use std::fmt::Display;

impl Display for HA {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Hybrid Automaton:")?;
        writeln!(f, "\tInitial Location: {:?}", self.initial_location)?;
        writeln!(f, "\tDimension: {:?}", self.dimension())?;
        writeln!(f, "\tLocations:")?;
        for loc in self.locations.values() {
            // Merging two locations deletes one of them, i.e., Nones as entries are possible.
            writeln!(f, "\t\t{}", &loc)?;
        }
        Ok(())
    }
}

impl Display for LocationId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

// impl Display for Conjunction {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         if self.is_empty() {
//             write!(f, "true")
//         } else {
//             write!(f, "{}", self[0])?;
//             for eq in self.iter().skip(1) {
//                 write!(f, " ∧ {}", eq)?;
//             }
//             Ok(())
//         }
//     }
// }

// impl Display for Disjunction {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         if self.is_empty() {
//             write!(f, "true")
//         } else {
//             write!(f, "{}", self[0])?;
//             for eq in self.iter().skip(1) {
//                 write!(f, " v {}", eq)?;
//             }
//             Ok(())
//         }
//     }
// }

impl Display for HAConstructionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            HAConstructionError::UnknownLocation(lid) => {
                write!(f, "Location with id {} does not exist", lid)
            }
            HAConstructionError::InvalidLocationPosition(lid, pos) => {
                write!(
                    f,
                    "Location {} has id {}. Id needs to match position in vector.",
                    pos, lid
                )
            }
            HAConstructionError::UnknownSourceLocation(name, sid) => {
                write!(f, "Source of jump {} is not a valid location. Location with id {} does not exist.", name, sid)
            }
            HAConstructionError::UnknownTargetLocation(name, tid) => {
                write!(f, "Target of jump {} is not a valid location. Location with id {} does not exist.", name, tid)
            }
            HAConstructionError::DoubleInitialLocation => {
                write!(f, "Initial location can only be initialized once.")
            }
            HAConstructionError::DoubleInitializeInvariant => {
                write!(f, "Initial invariant can only be initialized once.")
            }
            HAConstructionError::NonExistentInitialInvariant => {
                write!(
                    f,
                    "Initial invariant can only be replaced if it is already set."
                )
            }
            HAConstructionError::NonExistentInitialLocation => {
                write!(f, "Initial location must be set exactly once.")
            }
            HAConstructionError::InconsistentDimension {
                expected,
                was,
                in_loc,
            } => {
                write!(
                    f,
                    "Inconsistent dimensions detected.  Expected {} and got {} in {:?}",
                    expected, was, in_loc
                )
            }
        }
    }
}

impl Display for ActionId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Display for Location {
    fn fmt(&self, _f: &mut fmt::Formatter) -> fmt::Result {
        todo!()
        //     write!(f, "{}: ", self.name)?;
        //     for (var, diff_eq) in &self.dynamics {
        //         write!(f, "\u{0307}{} := {};", var, diff_eq)?;
        //     }
        //     write!(f, "{}", self.invariant)
    }
}
