use super::*;
use num::Zero;
use rand;
use rand::Rng;

struct HaWalker {
    ha: HA,
    cur_loc: LocationId,
    cur_state: Vec<Atom>,
}



impl HaWalker {
    pub fn new(ha: HA) -> Self {
        let s = (0..ha.dimension()).map(|_| Atom::from(0.0f64)).collect();
        Self {
            cur_loc: ha.initial_location,
            cur_state: s,
            ha
        }
    }


    pub fn step(&mut self) {
        let mut rng = rand::thread_rng();
        let loc = self.ha.get_location(self.cur_loc);
        let chosen_dyn: Vec<Atom> = loc.dynamics.0.iter().map(|dim_dyn|{
           let random_zer_one_v: f64 = rng.gen_range(0.0,1.0);
           let selected = dim_dyn.bounds().map(|(l,u)| (u-l)*Atom::from(random_zer_one_v) + l).unwrap_or(Atom::from(0.0));
            selected
        }).collect();
        let times: Vec<Scalar> = loc.invariant.0.iter().zip(chosen_dyn.iter()).map(|(i,d)|{
            let upper = i.upper();
            let max_time = upper/ *d;
            Scalar::range(Atom::zero(), max_time)
        }).collect();

        //[0,t_max] für die maximale Zeit die man warten kann bis eine invariante nichtmehr erfüllt ist.
        let chosen_time_interval = times.iter().fold(Scalar::full(), |acc,t| acc.intersection(t));

        let random_zer_one_v: f64 = rng.gen_range(0.0,1.0);
        let t = chosen_time_interval.bounds().map(|(l,u)| (u-l)*Atom::from(random_zer_one_v) + l).unwrap_or(Atom::from(0.0));

        let time_advanced_state = self.cur_state.iter().zip(chosen_dyn.iter()).map(|(state,d)| *state + *d*t).collect::<Vec<_>>();

        let action_choice = rng.gen();
        if action_choice {
            let neighbors = self.ha.successors_actions(self.cur_loc);
            //filter only valid edges
            let _filtered_neighbors = neighbors.into_iter()
            .map(|(a,l)| (a,l,self.ha.get_edge_guard(self.cur_loc, a, l)))
            .filter(|(_,_,g)| g.is_satisfied_atom(&time_advanced_state))
            .map(|(a,l,_)|(a,l))
            .collect::<Vec<_>>();

            


        } else {

        }
    }
}