# Planitor

This repository shows the result of my master's thesis.
The Planitor system is build to combine runtime monitoring based on [RTLola](https://pages.cispa.de/rtlola/) with model refinement for linear hybrid automaton system model and a grid-based environmental model.


## Introduction Example

The a most simple call to test the system can be seen here:

```
carog run -- monitor -w input/circle/10x10.wm -a input/up_down.ha -s input/simple.lola
```

## Installation 

Install [Rust](https://www.rust-lang.org/tools/install) and you are almost good to go!

Check you installed the required dependencies [Intervallum](https://github.com/Schwenger/intervallum) and [FastDownward](https://github.com/aibasel/downward) and build FastDownward [here](dependencies/downward/build.py).


##Input

To provide a hybrid automaton or world model input, you can checkout the provided example `.ha` and `.wm` [input](./input).
While the corresponding documentation is work in progress, you can check the custom parser for both files: [wm](src/world_model/wm_parser.rs) and [ha](src/hybridautomaton/ha_parser.rs).


## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.


## Visuals
While an interactive GUI is planned for the project, the current visualization are console prints.

![GUI Example](misc/gui1.png =150x)
![GUI Example](misc/gui2.png =150x)
![GUI Example](misc/gui3.png =150x)


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.




## Authors and acknowledgment

The automaton construction in Project is based on the work of [Maximillian Schwenger](https://github.com/Schwenger). 

## License
Open source, exact license to come.
TODO

## Project status
This project is currently in the improvement phase. The base code in general is done, cleanup and small improvements are to follow.
